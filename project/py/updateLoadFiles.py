import re
import os, os.path, sys, shutil, time, subprocess
from subprocess import call
from os import listdir

###
# Checks all the .htmls and images files and generates paths in InitialLoadController.coffee
###

def main():

    url = "../src/coffee/app/controller/InitialLoadController.coffee"
    startPartialsStr = "# Partials start"
    endPartialsStr = "# Partials end"

    with open(url) as f:
        file = f.read()

        top = file[:file.find(startPartialsStr)+len(startPartialsStr)]
        bottom = file[file.find(endPartialsStr):]
        middle = ""

        # adding all global partials
        for root, dirs, files in os.walk("../www/partial"):
            if "page" not in root:
                for file in files:
                    if file.endswith(".html"):
                        path = root+"/"+file
                        path = path.replace("../www/", "")
                        id = path.split("/").pop().replace(".html", "") + "-partial"
                        middle += "                "
                        middle += "{ id: " + '"' + id + '", ' + "src: " + '"' + path + '"' + " }"
                        middle += "\n"

        # adding all global images
        for root, dirs, files in os.walk("../www/image"):
            if "page" not in root:
                for file in files:
                    if file.endswith(".png") or file.endswith(".jpeg") or file.endswith(".jpg") or file.endswith(".gif"):
                        dir = root.replace("../www/", "")
                        n = file.split(".")
                        id = n[0]
                        ext = n[1]
                        path = dir + "/" + id + "." + ext
                        middle += "                "
                        middle += "{ id: " + '"' + id + "-image" + '", ' + "src: " + '"' + path + '"' + " }"
                        middle += "\n"

        # adding all svgs
        for root, dirs, files in os.walk("../www/svg"):
            for file in files:
                if file.endswith(".svg"):
                    dir = root.replace("../www/", "")
                    n = file.split(".")
                    id = n[0]
                    ext = n[1]
                    path = dir + "/" + id + "." + ext
                    middle += "                "
                    
                    middle += "{ id: " + '"' + id + "-svg" + '", ' + "src: " + '"' + path + '"' + ', ' + "type:createjs.LoadQueue.TEXT }"
                    middle += "\n"



        # adding all shaders
        for file in listdir("../www/shader"):
            path = "shader/"+file
            type = file.split(".")[1]
            if type == "frag" or type == "vert":
                id = file.split(".")[0]
                if type == "frag":
                    id += "-fragment"
                else:
                    id += "-vertex"
                middle += "\t\t\t\t"
                middle += "{ id: " + '"' + id + '", ' + "src: " + '"' + path + '"' + " }"
                middle += "\n"

        currentFileName = os.path.basename(__file__)
        content = top + "\n\t\t\t\t" + "# Generated from " + currentFileName + "\n" + middle + "\t\t\t\t" + bottom
        content = content.replace("\t", "    ")

        
        # # adding all 3d objects
        # startPartialsStr = "# Objects start"
        # endPartialsStr = "# Objects end"
        # file = content
        # top = file[:file.find(startPartialsStr)+len(startPartialsStr)]
        # bottom = file[file.find(endPartialsStr):]
        # middle = ""

        # folderdir = "../www/model"
        # currentId = 0
        # for file in listdir(folderdir):
        #     path = "js/app/shader/"+file
        #     type = file.split(".")[1]
        #     id = file.split(".")[0]
        #     if "@" in id: continue
        #     if currentId != id:
        #         currentId = id
        #         hasBin = False
        #         hasJs = False
        #         for fileInside in listdir(folderdir):
        #             if id+".js" == fileInside:
        #                 hasJs = True
        #             if id+".bin" == fileInside:
        #                 hasBin = True
        #         if hasBin == True or hasJs == True:
        #             middle += "\t\t\t\t"
        #             middle += "{ id: " + '"' + id + '"' + "}"
        #             middle += "\n"

        # currentFileName = os.path.basename(__file__)
        # content = top + "\n\t\t\t\t" + "# Generated from " + currentFileName + "\n" + middle + "\t\t\t\t" + bottom
        # content = content.replace("\t", "    ")

        newFile = open(url, "w+")
        newFile.write(content)

if __name__ == '__main__':
    main()
