import re
import os, os.path, sys, shutil, time, subprocess
from subprocess import call
from slugify import slugify
from json import JSONDecoder
import copy

def main():
    
    allStyles = []

    styleFolder = "../src/sass/layout"
    for root, dirs, files in os.walk(styleFolder):
        for file in files:
            if file.endswith(".scss") and "app.scss" not in file:
                allStyles.append(root+"/"+file)
    
    styleFolder = "../src/sass/page"
    for root, dirs, files in os.walk(styleFolder):
        for file in files:
            if file.endswith(".scss") and "app.scss" not in file:
                allStyles.append(root+"/"+file)

    url = "../src/sass/app.scss"
    startStr = "// Define start - generated content"
    endStr = "// Define end - generated content"
    with open(url) as f:
        file = f.read()

        top = file[:file.find(startStr)+len(startStr)]
        bottom = file[file.find(endStr):]
        middle = ""

        for file in allStyles:
            # parts = file.split("/")
            styleUrl = file.replace("../src/sass/", "")
            middle += '@import "' + styleUrl + '";\n'


        currentFileName = os.path.basename(__file__)
        content = top + "\n" + "// Generated from " + currentFileName + "\n" + middle + bottom
        
        newFile = open(url, "w+")
        newFile.write(content)

if __name__ == '__main__':
    main()
