# Require Config
require.config(
    waitSeconds : 30
    paths:
        # Components
        jquery: "../component/jquery/dist/jquery.min"
        signals: "../component/js-signals/dist/signals.min"
        hasher: "../component/hasher/dist/js/hasher.min"
        preloadjs: "../component/createjs-preloadjs/lib/preloadjs-0.6.0.min"
        Mustache: "../component/mustache.js/mustache"
        TweenMax: "../component/greensock/src/minified/TweenMax.min"
        TweenLite: "../component/greensock/src/minified/TweenLite.min"
        SplitText: "../component/greensock/SplitText"
        TimelineMax: "../component/greensock/src/minified/TimelineMax.min"
        TimelineLite: "../component/greensock/src/minified/TimelineLite.min"
        MouseWheel: "../component/jquery-mousewheel/jquery.mousewheel.min"
        
        Modernizr: "../lib/modernizr.custom"
        
        # Classes start
        # Generated from updateMain.py
        Main: "Main"
        App: "app/App"
        Context: "app/context/Context"
        SignalContext: "app/context/SignalContext"
        GlobalController: "app/controller/GlobalController"
        InitialLoadController: "app/controller/InitialLoadController"
        GlobalEvents: "app/event/GlobalEvents"
        GlobalModel: "app/model/GlobalModel"
        LoaderService: "app/service/LoaderService"
        Signal: "app/signal/Signal"
        Browser: "app/util/Browser"
        Util: "app/util/Util"
        View: "app/view/View"
        Footer: "app/view/layout/Footer"
        Header: "app/view/layout/Header"

        # Classes end

    shim:
        TimelineMax:
            deps: ["TweenMax"]
            exports: "TimelineMax"
        TweenMax:
            exports: "TweenMax"
        SplitText:
            deps: ["TweenMax"]
            exports: "SplitText"
        preloadjs:
            exports: "preloadjs"
        signals:
            exports: "signals"
        hasher:
            exports: "hasher"
        mustache:
            exports: "mustache"
)

require([

    "jquery"
    "App"
    "LoaderService"
    "GlobalModel"
    "GlobalController"
    "Signal"
    "Util"
    "GlobalEvents"
    "TweenMax"
    "Modernizr"

], (jquery, App, LoaderService, GlobalModel, GlobalController, Signal, Util, GlobalEvents, TweenMax, Modernizr)=>

    # Jquery Namespace
    window.jQuery = window.$ = jquery

    # RequestAnimationFrame Polyfoam
    do ->
        w = window
        for vendor in ['ms', 'moz', 'webkit', 'o']
            break if w.requestAnimationFrame
            w.requestAnimationFrame = w["#{vendor}RequestAnimationFrame"]
            w.cancelAnimationFrame = (w["#{vendor}CancelAnimationFrame"] or
                                  w["#{vendor}CancelRequestAnimationFrame"])

        # deal with the case where rAF is built in but cAF is not.
        if w.requestAnimationFrame
            return if w.cancelAnimationFrame
            browserRaf = w.requestAnimationFrame
            canceled = {}
            w.requestAnimationFrame = (callback) ->
                id = browserRaf (time) ->
                    if id of canceled then delete canceled[id]
                    else callback time
            w.cancelAnimationFrame = (id) -> canceled[id] = true

        # handle legacy browsers which don’t implement rAF
        else
            targetTime = 0
            w.requestAnimationFrame = (callback) ->
                targetTime = Math.max targetTime + 16, currentTime = +new Date
                w.setTimeout (-> callback +new Date), targetTime - currentTime

            w.cancelAnimationFrame = (id) -> clearTimeout id

    # Global Constants
    window.G = {
        
    }

    # Construct Services and Globals
    # Are 'Singletons' with access from everywhere
    window.Loader = new LoaderService()
    window.Model = new GlobalModel()
    window.Controller = new GlobalController()
    window.Signal = new Signal()
    window.Util = new Util()
    events = new GlobalEvents().init()

    # Starts Application
    $(=>
        new App()
    )

)
