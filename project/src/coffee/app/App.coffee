define ["require", "Context", "Browser", "InitialLoadController"], (require, Context, Browser, InitialLoadController) ->

    "use strict"
    
    class App

        mainLoader: undefined

        constructor: () ->
            # Browser configs
            b = new Browser().init()
            Model.isDesktop = b.isDesktop
            Model.browser = b.browser
            Model.browserVersion = parseInt(b.version, 10)
            Model.isOldBrowser = if Model.browser is "Explorer" and Model.browserVersion <= 9 then true else false

            # default tween
            TweenMax.defaultEase = Expo.easeInOut

            TweenMax.delayedCall 0, @init

        init: =>
            @loadInitialData()

        loadInitialData: =>
            manifest = [
                { id: "data", src: "data/data.json", type:createjs.LoadQueue.JSON }
            ]
            Loader.load(manifest, @dataCompleted )

        dataCompleted: (e) =>
            # Global Content
            data = Loader.queue.getResult("data")
            Model.content = data.content
            Model.parentEl = $(data.initialData.app_parent)
            Model.routing = data.routing
            Model.award = data.award

            @loadAppData()

        loadAppData: =>
            loadController = new InitialLoadController(@appDataCompleted)

        appDataCompleted: =>
            context = new Context()

    return App
