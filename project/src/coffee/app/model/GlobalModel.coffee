define [], () ->

    "use strict"
    
    class GlobalModel

        routing: undefined
        newHash: undefined
        isDesktop: undefined
        browser: undefined
        browserVersion: undefined
        isOldBrowser: false
        content: undefined
        parentEl: undefined
        env: undefined
        debugMode: undefined
        windowW: 0
        windowH: 0

        constructor: () ->

    return GlobalModel
