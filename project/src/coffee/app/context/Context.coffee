define ["signals", "SignalContext"], (signals, SignalContext) ->
    
    "use strict"

    class Context extends SignalContext

        constructor: () ->
            super()
            @mapSignals()

        mapSignals: =>

            # Map Signals with Commands
            @mapSignalCommand Signal.initViews, Controller.setupViews, true

            # Init DOM
            Signal.initViews.dispatch()

    return Context
