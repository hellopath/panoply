define ["signals"], (signals) ->

    "use strict"
    
    class SignalContext

        constructor: () ->

        mapSignalCommand: (signal, command, addOnce)=>
            if addOnce?
                signal.addOnce(command)
            else
                signal.add(command)

    return SignalContext
