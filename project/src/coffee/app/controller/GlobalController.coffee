define ["Header", "Footer"], (Header, Footer) ->

    "use strict"
    
    class GlobalController

        constructor: () ->

        setupViews: () =>
            # Header
            header = new Header("header")
            Model.parentEl.append header.element
            header.init()

            # Footer
            footer = new Footer("footer")
            Model.parentEl.append footer.element
            footer.init()

            return

    return GlobalController
