define [], () ->

    "use strict"

    class InitialLoadController

        callback: undefined

        constructor: (cb) ->
            @callback = cb
            @loadImagesAndTexts()

        loadImagesAndTexts: =>

            manifest = [
                
                # Partials start
                # Generated from updateLoadFiles.py
                { id: "footer-partial", src: "partial/layout/footer.html" }
                { id: "header-partial", src: "partial/layout/header.html" }
                { id: "background-image", src: "image/global/background.jpg" }
                { id: "email-image", src: "image/global/email.png" }
                { id: "logo-image", src: "image/global/logo.png" }
                { id: "weare-image", src: "image/global/weare.png" }
                { id: "behance-icon-svg", src: "svg/behance-icon.svg", type:createjs.LoadQueue.TEXT }
                { id: "facebook-icon-svg", src: "svg/facebook-icon.svg", type:createjs.LoadQueue.TEXT }
                { id: "instagram-icon-svg", src: "svg/instagram-icon.svg", type:createjs.LoadQueue.TEXT }
                { id: "linkedin-icon-svg", src: "svg/linkedin-icon.svg", type:createjs.LoadQueue.TEXT }
                { id: "tumblr-icon-svg", src: "svg/tumblr-icon.svg", type:createjs.LoadQueue.TEXT }
                { id: "twitter-icon-svg", src: "svg/twitter-icon.svg", type:createjs.LoadQueue.TEXT }
                { id: "vimeo-icon-svg", src: "svg/vimeo-icon.svg", type:createjs.LoadQueue.TEXT }
                # Partials end

            ]

            Loader.load manifest, @callback

    return InitialLoadController
