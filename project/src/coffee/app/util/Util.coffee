define [], () ->

    "use strict"
    
    class Util

        constructor: () ->

        CapitalizeFirstLetter: (string) =>
            return string.charAt(0).toUpperCase() + string.slice(1)

        JqueryObjToString: (jobj) =>
            return $('<div>').append($(jobj).clone()).html()

        DegreesToRadians: (degrees) =>
            return degrees * (Math.PI / 180)

        RadiansToDegrees: (radians) =>
            return radians * (180 / Math.PI)

        Limit: (min, max, value) =>
            return Math.max(min, Math.min(max, value))

        ConvertToSlug: (text, separator) =>
            separator = if separator? then separator else "-"
            return text.toLowerCase().replace(/[^\w ]+/g, "").replace RegExp(" +", "g"), separator

        ConvertToClassName: (text) =>
            tArray = text.split("-")
            txt = ""
            if tArray.length > 1
                for t in tArray
                    txt += @CapitalizeFirstLetter(t)
            else
                txt = @CapitalizeFirstLetter(tArray[0])    
            return txt

        RandomInt: (multiplier=10000000) =>
            if multiplier? then multiplier else 10000000
            return Math.round(Math.random()*multiplier) + 1

        IsInt: (n) =>
            return n % 1 is 0

        # Not safe always (be careful with that)
        ToFixed: (number, factor) =>
            return Math.round(number * factor)/factor

        GetImgNativeSize: (url) =>
            img = new Image()
            img.src = url
            return { width:img.width, height:img.height }

        Rand: (max, min, decimals) =>
            if min > max
                undefined
            randomNum = Math.random() * (max - min) + min
            d = Math.pow 10, decimals
            ~~((d * randomNum) + 0.5) / d

        IsEven: (value) =>
            if (value%2) is 0
                return true
            else
                return false

        GetUrlVars:(key) =>
            vars = {}
            parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/g, (m, key, value) ->
                cleanVal = value
                if value.indexOf("#") > 0
                    indexPos = value.indexOf("#")
                    cleanVal = value.slice(0, indexPos)
                vars[key] = cleanVal
                return
            )
            vars

        GetAdjustmentContainerSize: (viewportWidth, viewportHeight, originalW, originalH) =>
            viewportWidth = viewportWidth
            viewportHeight = viewportHeight
            viewportAspectRatio = viewportWidth / viewportHeight
            originalAspectRatio = originalW / originalH

            scaleX = (viewportWidth / originalW) * 1
            scaleY = (viewportHeight / originalH) * 1

            if viewportAspectRatio > originalAspectRatio
                type = "landscape"
                holderW = (originalW * scaleX)
                holderH = viewportHeight
            else
                type = "portrait"
                holderW = viewportWidth
                holderH = (originalH * scaleY)

            return { type:type, holderW: holderW, holderH: holderH, originalW: originalW, originalH: originalH, scaleX: scaleX, scaleY: scaleY }

        SetCookie: (cname, cvalue) =>
            document.cookie = cname + "=" + cvalue

        GetCookie: (cname) =>
            name = cname + "="
            ca = document.cookie.split(';')
            i = 0
            cookie = ""
            while i < ca.length
                c = ca[i].trim()
                if c.indexOf(name) is 0
                    cookie = c.substring name.length, c.length
                i++
            return cookie

        ReplaceAll: (find, replace, str) =>
            return str.replace(new RegExp(find, 'g'), replace)

        CommaSeparateNumber: (val) =>
            while (/(\d+)(\d{3})/.test(val.toString()))
                val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2')
            return val

        SetupCanvas: (params) =>
            params.element.css(
                width: params.width + "px"
                height: params.height + "px"
            )

            stage = new createjs.Stage(params.element[0])
            stage.canvas.width = params.width
            stage.canvas.height = params.height
            return stage

        GetRoutingIndexById: (id) =>
            for route, i in Model.routing
                if route.id is id
                    return i

        SetupAnimatedSprite: (params) =>
            params.framerate = params.framerate || 30
            params.regX = params.regX || 0
            params.regY = params.regY || 0

            stage = @SetupCanvas(params)

            data =
                images: [params.image]
                frames: 
                    width: params.width
                    height: params.height
                    regX: params.regX
                    regY: params.regX
                    count: params.count
                animations: 
                    run: [0, params.count-1, "run"]

            spriteSheet = new createjs.SpriteSheet(data)
            sprite = new createjs.Sprite(spriteSheet, "run")
            sprite.framerate = params.framerate
            if params.paused then sprite.gotoAndStop(0) else sprite.gotoAndPlay(0)

            stage.addChild sprite

            createjs.Ticker.timingMode = createjs.Ticker.RAF

            return { stage: stage, sprite: sprite, tick: params.tick }

        RemoveHTMLTags: (str)=>
            return str.replace(/<\/?[^>]+(>|$)/g, "")

        GetRect: (top, right, bottom, left) =>
            return "rect(" + top + "px " + right + "px " + bottom + "px " + left + "px" + ")"

        GetChapterUniqueClass: ($chapter)=>
            splitter = $chapter.attr("class").split(" ")
            uniqueClass = "." + splitter[splitter.length-1]
            return uniqueClass

        GetSupportedVideoExtension: =>
            support = Modernizr.video
            return if support["h264"] != "" then "mp4" else if support["ogg"] != "" then "ogg" else "webm"

    return Util
