define ["View"], (View) ->

    class Footer extends View

        constructor: (id) ->
            scope = {}

            scope.behance = Loader.getSvg "behance-icon"
            scope.facebook = Loader.getSvg "facebook-icon"
            scope.linkedin = Loader.getSvg "linkedin-icon"
            scope.twitter = Loader.getSvg "twitter-icon"
            scope.vimeo = Loader.getSvg "vimeo-icon"
            scope.tumblr = Loader.getSvg "tumblr-icon"
            scope.instagram = Loader.getSvg "instagram-icon"

            super(id, scope)

        init: =>

    return Footer
