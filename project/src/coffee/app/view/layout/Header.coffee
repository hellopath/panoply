define ["View"], (View) ->

    "use strict"
    
    class Header extends View

        constructor: (id) ->
            scope = {}

            scope.logo = Loader.getImageURL "logo-image"
            scope.email = Loader.getImageURL "email-image"
            scope.weare = Loader.getImageURL "weare-image"

            super(id, scope)
            return

        init: =>

            CSSPlugin.defaultTransformPerspective = 1000

            $logo = @element.find(".logo")
            $weAre = @element.find(".weare")

            @tl = new TimelineMax({repeat:-1, repeatDelay:4})
            @tl.to $logo, 2, {directionalRotation:{ rotationX:-180+"_ccw" }, force3D:true, ease:Expo.easeInOut, transformOrigin:"50% 50% -100"}, 0
            @tl.from $weAre, 2, {directionalRotation:{ rotationX:180+"_cw" }, force3D:true, ease:Expo.easeInOut, transformOrigin:"50% 50% -100"}, 0
            @tl.to $logo, 2, {directionalRotation:{ rotationX:0+"_ccw" }, force3D:true, ease:Expo.easeInOut, transformOrigin:"50% 50% -100"}, 4
            @tl.to $weAre, 2, {directionalRotation:{ rotationX:180+"_ccw" }, force3D:true, ease:Expo.easeInOut, transformOrigin:"50% 50% -100"}, 4
            @tl.play(0)

            @tl.timeScale(0.8)

            return

    return Header
