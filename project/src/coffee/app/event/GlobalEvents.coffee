define [], () ->
    class GlobalEvents

        "use strict"

        resizeTimeout: undefined
        
        constructor: () ->

        init: () =>
            $(window).resize @onResizeHandler
            @onResizeHandler()

        onResizeHandler: =>
            Model.windowW = window.innerWidth
            Model.windowH = window.innerHeight
            @resizeTimeout = setTimeout(Signal.onResize.dispatch, 200)

    return GlobalEvents
