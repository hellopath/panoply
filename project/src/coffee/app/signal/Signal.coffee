define ["signals"], (signals) ->

    "use strict"
    
    class Signal

        # Notifications

        onResize: new signals.Signal()
        onUpdate: new signals.Signal()
        onWheel: new signals.Signal()
        onScroll: new signals.Signal()
        onRouteChanged: new signals.Signal()
        onKeyPressed: new signals.Signal()

        # Actions

        initViews: new signals.Signal()
        startRouting: new signals.Signal()

        constructor: () ->

    return Signal
