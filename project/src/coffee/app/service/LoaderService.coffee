define ["preloadjs"], (preloadjs) ->

    "use strict"
    
    class LoaderService

        queue: undefined
        geometryCollection: {}

        constructor: () ->
            @queue = new createjs.LoadQueue()
            @queue.on("complete", @onLoadCompleted)
            @queue.on("progress", @onLoadProgress)

        close: =>
            if @queue? then @queue.close()

        load: (manifest, onLoaded, onProgress) =>
            @queue.onLoaded = onLoaded
            @queue.onProgress = onProgress
            @queue.loadManifest manifest

        pushGeometry: (id, g)=>
            @geometryCollection[id] = g

        getImageURL: (id) =>
            result = $(@queue.getResult(id)).attr("src")
            return result

        getVideoSrc: (id) =>
            return $(@queue.getResult(id+"-video")).attr("src")

        getTexture: (id) =>
            img = @getContentById(id)
            texture = new THREE.Texture(img)
            texture.needsUpdate = true
            return texture

        getShader: (id) =>
            return @getContentById(id)

        getSvg: (id) =>
            return @getContentById(id+"-svg")

        getPartial: (id) =>
            return @getContentById(id+"-partial")

        getGeometry: (id) =>
            return @geometryCollection[id].clone()

        getContentById: (id) =>
            return @queue.getResult id

        onLoadCompleted: (e) =>
            q = @queue
            q.onLoaded?(e.currentTarget)

        onLoadProgress: (e) =>
            q = @queue
            q.onProgress?()

    return LoaderService
