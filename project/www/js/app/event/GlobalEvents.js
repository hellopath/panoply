var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define([], function() {
  var GlobalEvents;
  GlobalEvents = (function() {
    "use strict";
    GlobalEvents.prototype.resizeTimeout = void 0;

    function GlobalEvents() {
      this.onResizeHandler = __bind(this.onResizeHandler, this);
      this.init = __bind(this.init, this);
    }

    GlobalEvents.prototype.init = function() {
      $(window).resize(this.onResizeHandler);
      return this.onResizeHandler();
    };

    GlobalEvents.prototype.onResizeHandler = function() {
      Model.windowW = window.innerWidth;
      Model.windowH = window.innerHeight;
      return this.resizeTimeout = setTimeout(Signal.onResize.dispatch, 200);
    };

    return GlobalEvents;

  })();
  return GlobalEvents;
});
