var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["signals"], function(signals) {
  "use strict";
  var SignalContext;
  SignalContext = (function() {
    function SignalContext() {
      this.mapSignalCommand = __bind(this.mapSignalCommand, this);
    }

    SignalContext.prototype.mapSignalCommand = function(signal, command, addOnce) {
      if (addOnce != null) {
        return signal.addOnce(command);
      } else {
        return signal.add(command);
      }
    };

    return SignalContext;

  })();
  return SignalContext;
});
