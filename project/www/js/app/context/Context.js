var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["signals", "SignalContext"], function(signals, SignalContext) {
  "use strict";
  var Context;
  Context = (function(_super) {
    __extends(Context, _super);

    function Context() {
      this.mapSignals = __bind(this.mapSignals, this);
      Context.__super__.constructor.call(this);
      this.mapSignals();
    }

    Context.prototype.mapSignals = function() {
      this.mapSignalCommand(Signal.initViews, Controller.setupViews, true);
      return Signal.initViews.dispatch();
    };

    return Context;

  })(SignalContext);
  return Context;
});
