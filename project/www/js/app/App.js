var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["require", "Context", "Browser", "InitialLoadController"], function(require, Context, Browser, InitialLoadController) {
  "use strict";
  var App;
  App = (function() {
    App.prototype.mainLoader = void 0;

    function App() {
      this.appDataCompleted = __bind(this.appDataCompleted, this);
      this.loadAppData = __bind(this.loadAppData, this);
      this.dataCompleted = __bind(this.dataCompleted, this);
      this.loadInitialData = __bind(this.loadInitialData, this);
      this.init = __bind(this.init, this);
      var b;
      b = new Browser().init();
      Model.isDesktop = b.isDesktop;
      Model.browser = b.browser;
      Model.browserVersion = parseInt(b.version, 10);
      Model.isOldBrowser = Model.browser === "Explorer" && Model.browserVersion <= 9 ? true : false;
      TweenMax.defaultEase = Expo.easeInOut;
      TweenMax.delayedCall(0, this.init);
    }

    App.prototype.init = function() {
      return this.loadInitialData();
    };

    App.prototype.loadInitialData = function() {
      var manifest;
      manifest = [
        {
          id: "data",
          src: "data/data.json",
          type: createjs.LoadQueue.JSON
        }
      ];
      return Loader.load(manifest, this.dataCompleted);
    };

    App.prototype.dataCompleted = function(e) {
      var data;
      data = Loader.queue.getResult("data");
      Model.content = data.content;
      Model.parentEl = $(data.initialData.app_parent);
      Model.routing = data.routing;
      Model.award = data.award;
      return this.loadAppData();
    };

    App.prototype.loadAppData = function() {
      var loadController;
      return loadController = new InitialLoadController(this.appDataCompleted);
    };

    App.prototype.appDataCompleted = function() {
      var context;
      return context = new Context();
    };

    return App;

  })();
  return App;
});
