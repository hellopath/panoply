var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["View", "MouseWheel", "VideoLayer"], function(View, MouseWheel, VideoLayer) {
  var ProjectTopContainer;
  ProjectTopContainer = (function(_super) {
    __extends(ProjectTopContainer, _super);

    ProjectTopContainer.prototype.tl = void 0;

    ProjectTopContainer.prototype.currentNumPart = 0;

    ProjectTopContainer.prototype.activeAnimation = false;

    ProjectTopContainer.prototype.onLastPartBegins = void 0;

    ProjectTopContainer.prototype.videoFrontColor = void 0;

    ProjectTopContainer.prototype.lineTitle = void 0;

    function ProjectTopContainer(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.partCompleted = __bind(this.partCompleted, this);
      this.removeTweensEvents = __bind(this.removeTweensEvents, this);
      this.onWheel = __bind(this.onWheel, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.onVisitOut = __bind(this.onVisitOut, this);
      this.onVisitOver = __bind(this.onVisitOver, this);
      this.setupWheelView = __bind(this.setupWheelView, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      var aw, awardId, awards, awardsSvg, i, path, _i, _len, _ref;
      scope.lineArrowBtn = Loader.getSvg("line-arrow");
      scope.awwwards = Loader.getSvg("awwwards-logo");
      scope.introTxt = Model.content["intro-txt"];
      scope.visitTxt = Model.content["visit-txt"];
      scope.clientTxt = Model.content["client-txt"];
      scope.agenceTxt = Model.content["agence-txt"];
      scope.technoTxt = Model.content["techno-txt"];
      scope.sortieTxt = Model.content["sortie-txt"];
      scope.lineArrowPartial = Loader.getPartial("head-line-arrow");
      scope.headArrow = Loader.getSvg("head-arrow");
      scope.headArrowWide = Loader.getSvg("head-arrow-wide");
      awardsSvg = [];
      awards = Model.award;
      _ref = scope.awards;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        awardId = _ref[i];
        aw = awards[awardId];
        path = awardId + "-logo-svg";
        aw.svg = Loader.getSvg(path);
        awardsSvg[i] = aw;
      }
      scope.awardsSvg = awardsSvg;
      ProjectTopContainer.__super__.constructor.call(this, id, scope);
    }

    ProjectTopContainer.prototype.init = function() {
      this.groupA = this.element.find(".groupA");
      this.groupB = this.element.find(".groupB");
      this.textContainer = this.element.find(".text-container");
      this.awards = this.groupB.find(".awards");
      this.visitBtn = this.groupB.find(".visit-btn");
      return this.ready();
    };

    ProjectTopContainer.prototype.ready = function() {
      var $videoHolder, videoUrl;
      this.videoFrontColor = this.element.find(".video-container .video-front-color");
      this.lineTitle = this.element.find(".groupB .line-title");
      videoUrl = Model.videoBasePath + this.id;
      $videoHolder = this.element.find(".video-container .relative");
      this.videoLayer = new VideoLayer($videoHolder);
      this.videoLayer.videoUrl = videoUrl;
      this.videoLayer.onReadyCallback = this.onVideoCallbackReady;
      this.videoLayer.init();
      return this.resize();
    };

    ProjectTopContainer.prototype.setupWheelView = function() {
      this.addAnimations();
      $(window).on("mousewheel", this.onWheel);
      this.visitBtn.on("mouseover", this.onVisitOver);
      return this.visitBtn.on("mouseout", this.onVisitOut);
    };

    ProjectTopContainer.prototype.onVisitOver = function() {
      return this.visitBtnTl.play();
    };

    ProjectTopContainer.prototype.onVisitOut = function() {
      return this.visitBtnTl.reverse();
    };

    ProjectTopContainer.prototype.addAnimations = function() {
      var $arrow, $groupA_description, $groupA_mainTitle, $groupA_slash, $groupB_awards, $groupB_btn, $groupB_liTitles, $groupB_lineTitle, $groupB_lines, $groupB_mainTitle, $groupB_pTitles, $groupB_paragraph, $navLeft, $navMiddle, $navRight, $topNavMenu, $visitText, scalePos,
        _this = this;
      $visitText = this.visitBtn.find(".visit-text");
      $arrow = this.visitBtn.find(".arrow-holder");
      this.visitBtnTl = new TimelineMax();
      this.visitBtnTl.to($arrow, 0.8, {
        x: 15,
        force3D: true,
        ease: Expo.easeInOut
      }, 0);
      this.visitBtnTl.to($visitText, 0.8, {
        x: 10,
        force3D: true,
        ease: Expo.easeInOut
      }, 0.1);
      this.visitBtnTl.addCallback(function() {
        if (_this.visitBtnTl.reversed()) {
          return $arrow.removeClass("hovered");
        } else {
          return $arrow.addClass("hovered");
        }
      }, 0.1);
      this.visitBtnTl.pause(0);
      this.visitBtnTl.timeScale(2);
      $groupA_description = this.groupA.find(".description");
      $groupA_slash = this.groupA.find(".slash");
      $groupA_mainTitle = this.groupA.find(".main-title");
      $groupB_mainTitle = this.groupB.find(".left-part .main-title");
      $groupB_lineTitle = this.groupB.find(".left-part .line-title");
      $groupB_paragraph = this.groupB.find(".right-part");
      $groupB_btn = this.groupB.find(".visit-btn");
      $groupB_liTitles = this.groupB.find("li .main-title");
      $groupB_pTitles = this.groupB.find("li p");
      $groupB_lines = this.groupB.find(".infos .box-line");
      $groupB_awards = this.groupB.find(".awards span");
      this.tl = new TimelineMax();
      this.animTime = 1.1;
      scalePos = 0.7;
      this.tl.from($groupA_description, this.animTime, {
        y: Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, 0);
      this.tl.from($groupA_slash, this.animTime, {
        y: Model.windowH * scalePos,
        scaleY: 20,
        autoAlpha: 0,
        transformOrigin: "50% 0%",
        ease: Expo.easeInOut,
        force3D: true
      }, 0.02);
      this.tl.from($groupA_mainTitle, this.animTime, {
        y: Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, 0.14);
      this.tl.addLabel("partA");
      this.tl.to($groupA_description, this.animTime, {
        y: -Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, "partA+=0.1");
      this.tl.to($groupA_slash, this.animTime, {
        y: -Model.windowH * scalePos,
        scaleY: 20,
        autoAlpha: 0,
        transformOrigin: "50% 100%",
        ease: Expo.easeInOut,
        force3D: true
      }, "partA+=0.16");
      this.tl.to($groupA_mainTitle, this.animTime, {
        y: -Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, "partA+=0.18");
      this.tl.from($groupB_mainTitle, this.animTime, {
        y: Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, "partA+=1.18");
      this.tl.from($groupB_paragraph, this.animTime, {
        y: Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, "partA+=1.20");
      this.tl.from($groupB_btn, this.animTime, {
        y: Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, "partA+=1.24");
      this.tl.staggerFrom($groupB_liTitles, this.animTime, {
        y: Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, 0.06, "partA+=1.28");
      this.tl.staggerFrom($groupB_pTitles, this.animTime, {
        y: Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, 0.06, "partA+=1.32");
      this.tl.staggerFrom($groupB_lines, this.animTime, {
        y: Model.windowH * scalePos,
        autoAlpha: 0,
        scaleY: 3,
        transformOrigin: "0% 0%",
        ease: Expo.easeInOut,
        force3D: true
      }, 0.06, "partA+=1.4");
      this.tl.staggerFrom($groupB_awards, this.animTime, {
        y: Model.windowH * scalePos,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        force3D: true
      }, 0.06, "partA+=1.5");
      this.tl.from($groupB_lineTitle, this.animTime, {
        scaleX: 0,
        autoAlpha: 0,
        ease: Expo.easeInOut,
        transformOrigin: "0% 50%",
        force3D: true
      }, "partA+=1.7");
      this.tl.addLabel("partB");
      this.tl.pause(0);
      this.tl.timeScale(1.2);
      $topNavMenu = this.element.find("#projects-top-nav-menu-view");
      $navLeft = $topNavMenu.find(".left-btn-part .inside-part");
      $navRight = $topNavMenu.find(".right-btn-part .inside-part");
      $navMiddle = $topNavMenu.find(".bottom-scroll-arrow");
      this.startTl = new TimelineMax();
      this.startTl.from($navLeft, 1, {
        y: -100,
        transformOrigin: "0% 0%",
        ease: Expo.easeInOut
      }, 0);
      this.startTl.from($navRight, 1, {
        y: 100,
        transformOrigin: "0% 0%",
        ease: Expo.easeInOut
      }, 0);
      return this.startTl.from($navMiddle, 1, {
        y: 100,
        transformOrigin: "0% 0%",
        ease: Elastic.easeInOut
      });
    };

    ProjectTopContainer.prototype.onWheel = function(e) {
      var delta;
      delta = e.deltaY;
      if (!this.activeAnimation) {
        this.activeAnimation = true;
        if (delta > 0) {
          this.currentNumPart -= 1;
        } else {
          this.currentNumPart += 1;
        }
        this.currentNumPart = this.currentNumPart < 0 ? 0 : this.currentNumPart;
        this.currentNumPart = this.currentNumPart > 3 ? 2 : this.currentNumPart;
        switch (this.currentNumPart) {
          case 0:
            TweenMax.delayedCall(this.animTime, this.partCompleted);
            this.tl.tweenTo(0);
            this.videoFrontColor.removeClass("open");
            return this.videoFrontColor.removeClass("openDark");
          case 1:
            TweenMax.delayedCall(this.animTime, this.partCompleted);
            this.tl.tweenTo("partA");
            return this.videoFrontColor.addClass("open");
          case 2:
            TweenMax.delayedCall(this.animTime * 1.2, this.partCompleted);
            this.tl.tweenTo("partB");
            return this.videoFrontColor.addClass("openDark");
          case 3:
            this.onLastPartBegins();
            $(window).off("mousewheel", this.onWheel);
            return TweenMax.delayedCall(1, this.removeTweensEvents);
        }
      }
    };

    ProjectTopContainer.prototype.removeTweensEvents = function() {
      return this.tl.clear();
    };

    ProjectTopContainer.prototype.partCompleted = function() {
      this.activeAnimation = false;
      return TweenMax.killDelayedCallsTo(this.partCompleted);
    };

    ProjectTopContainer.prototype.resize = function() {
      var elementCss, resizeParams;
      resizeParams = Util.GetAdjustmentContainerSize(Model.windowW, Model.windowH, Model.mediaW, Model.mediaH);
      elementCss = {
        width: resizeParams.holderW,
        height: resizeParams.holderH
      };
      this.element.css(elementCss);
      this.groupA.css({
        left: (this.textContainer.width() >> 1) - (this.groupA.width() >> 1),
        top: (this.textContainer.height() >> 1) - (this.groupA.height() >> 1)
      });
      return this.groupB.css({
        left: (this.textContainer.width() >> 1) - (this.groupB.width() >> 1),
        top: (this.textContainer.height() >> 1) - ((this.groupB.height() - this.awards.outerHeight(true)) >> 1)
      });
    };

    ProjectTopContainer.prototype.destroy = function() {
      if (this.visitBtnTl != null) {
        this.visitBtnTl.clear();
      }
      if (this.startTl != null) {
        this.startTl.clear();
      }
      if (this.videoLayer != null) {
        this.videoLayer.destroy();
      }
      if (this.tl != null) {
        this.tl.clear();
      }
      $(window).unbind("mousewheel", this.onWheel);
      this.visitBtn.off("mouseover", this.onVisitOver);
      this.visitBtn.off("mouseout", this.onVisitOut);
      return ProjectTopContainer.__super__.destroy.call(this);
    };

    return ProjectTopContainer;

  })(View);
  return ProjectTopContainer;
});
