var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["View", "SplitText"], function(View, SplitText) {
  var ProjectsTopNavMenu;
  ProjectsTopNavMenu = (function(_super) {
    __extends(ProjectsTopNavMenu, _super);

    function ProjectsTopNavMenu(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.onBtnOut = __bind(this.onBtnOut, this);
      this.onBtnOver = __bind(this.onBtnOver, this);
      this.textSplitter = __bind(this.textSplitter, this);
      this.addAnimation = __bind(this.addAnimation, this);
      this.init = __bind(this.init, this);
      scope = Router.getNextPreviousProject();
      scope.lineArrowPartial = Loader.getPartial("head-line-arrow");
      scope.headArrow = Loader.getSvg("head-arrow");
      scope.headArrowWide = Loader.getSvg("head-arrow-wide");
      ProjectsTopNavMenu.__super__.constructor.call(this, id, scope);
    }

    ProjectsTopNavMenu.prototype.init = function() {
      var $arrowLeft, $arrowRight, $leftHolder, $rightHolder;
      this.leftPartBtn = this.element.find(".left-btn-part");
      this.rightPartBtn = this.element.find(".right-btn-part");
      $rightHolder = this.rightPartBtn.find(".middle-holder");
      $leftHolder = this.leftPartBtn.find(".middle-holder");
      this.rightBtn = this.rightPartBtn.find(".inside-part");
      this.leftBtn = this.leftPartBtn.find(".inside-part");
      this.bottomScrollArrow = this.element.find(".bottom-scroll-arrow");
      this.rightBtn.on("mouseover", this.onBtnOver);
      this.rightBtn.on("mouseout", this.onBtnOut);
      this.leftBtn.on("mouseover", this.onBtnOver);
      this.leftBtn.on("mouseout", this.onBtnOut);
      this.leftTl = new TimelineMax();
      this.rightTl = new TimelineMax();
      $arrowLeft = $leftHolder.find(".arrow-holder");
      $arrowRight = $rightHolder.find(".arrow-holder");
      this.leftSplitTexts = this.textSplitter($leftHolder);
      this.rightSplitTexts = this.textSplitter($rightHolder);
      this.addAnimation(this.leftTl, this.leftSplitTexts, $arrowLeft, $leftHolder, "left");
      return this.addAnimation(this.rightTl, this.rightSplitTexts, $arrowRight, $rightHolder, "right");
    };

    ProjectsTopNavMenu.prototype.addAnimation = function(tl, splitter, arrow, btnHolder, type) {
      var $titlesHolder, offsetX,
        _this = this;
      $titlesHolder = btnHolder.find(".titles-holder");
      offsetX = type === "left" ? 20 : -20;
      tl.staggerFromTo(splitter.down.chars, 0.8, {
        y: 0,
        opacity: 1,
        force3D: true,
        ease: Expo.easeInOut
      }, {
        y: 7,
        opacity: 0,
        force3D: true,
        ease: Expo.easeInOut
      }, 0.06, 0);
      tl.staggerFromTo(splitter.up.chars, 0.8, {
        y: -7,
        opacity: 0,
        force3D: true,
        ease: Expo.easeInOut
      }, {
        y: 0,
        opacity: 1,
        force3D: true,
        ease: Expo.easeInOut
      }, 0.06, 0.1);
      tl.to($titlesHolder, 0.8, {
        y: offsetX,
        force3D: true,
        ease: Expo.easeInOut
      }, 0.3);
      tl.addCallback(function() {
        if (tl.reversed()) {
          return arrow.removeClass("hovered");
        } else {
          return arrow.addClass("hovered");
        }
      }, 0.26);
      tl.pause(0);
      return tl.timeScale(2);
    };

    ProjectsTopNavMenu.prototype.textSplitter = function($el) {
      var down, up;
      up = new SplitText($el.find(".title.up"), {
        type: "words, chars"
      });
      down = new SplitText($el.find(".title.down"), {
        type: "words, chars"
      });
      return {
        up: up,
        down: down
      };
    };

    ProjectsTopNavMenu.prototype.onBtnOver = function(e) {
      var $target, id;
      e.preventDefault();
      $target = $(e.currentTarget);
      id = $target.attr("id");
      if (id === "left") {
        return this.leftTl.play();
      } else {
        return this.rightTl.play();
      }
    };

    ProjectsTopNavMenu.prototype.onBtnOut = function(e) {
      var $target, id;
      e.preventDefault();
      $target = $(e.currentTarget);
      id = $target.attr("id");
      if (id === "left") {
        return this.leftTl.reverse();
      } else {
        return this.rightTl.reverse();
      }
    };

    ProjectsTopNavMenu.prototype.resize = function() {
      var bottomScrollCss, leftCss, rightCss;
      leftCss = {
        top: (Model.windowH >> 1) - (this.leftPartBtn.height()) + 20,
        left: -(this.leftPartBtn.width() >> 1) + (this.leftPartBtn.height()) - 12
      };
      rightCss = {
        top: (Model.windowH >> 1) - (this.rightPartBtn.height()) + 20,
        left: Model.windowW - (this.rightPartBtn.width() >> 1) - this.rightPartBtn.height() - 2
      };
      bottomScrollCss = {
        top: Model.windowH - (this.bottomScrollArrow.width() * 3),
        left: (Model.windowW >> 1) - (this.bottomScrollArrow.width() >> 1)
      };
      this.rightPartBtn.css(rightCss);
      this.leftPartBtn.css(leftCss);
      return this.bottomScrollArrow.css(bottomScrollCss);
    };

    ProjectsTopNavMenu.prototype.destroy = function() {
      this.leftTl.clear();
      this.rightTl.clear();
      this.leftSplitTexts = null;
      this.rightSplitTexts = null;
      this.rightBtn.off("mouseover", this.onBtnOver);
      this.rightBtn.off("mouseout", this.onBtnOut);
      this.leftBtn.off("mouseover", this.onBtnOver);
      this.leftBtn.off("mouseout", this.onBtnOut);
      return ProjectsTopNavMenu.__super__.destroy.call(this);
    };

    return ProjectsTopNavMenu;

  })(View);
  return ProjectsTopNavMenu;
});
