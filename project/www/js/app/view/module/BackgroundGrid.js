var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define([], function() {
  var BackgroundGrid;
  BackgroundGrid = (function() {
    BackgroundGrid.prototype.element = void 0;

    function BackgroundGrid() {
      this.onResize = __bind(this.onResize, this);
      this.init = __bind(this.init, this);
    }

    BackgroundGrid.prototype.init = function() {
      var $grid, grid, html, i, x, _i, _j, _len, _ref;
      $(window).on("resize", this.onResize);
      html = "<div class='grid-holder'>";
      for (i = _i = 0; _i <= 16; i = ++_i) {
        html += "<div class='grid-part'></div>";
      }
      html += "</div>";
      this.element.append(html);
      this.gridHolder = this.element.find(".grid-holder");
      this.gridParts = this.element.find(".grid-part");
      this.gridW = this.gridParts.first().outerWidth(true);
      this.gridNumLen = this.gridParts.length;
      _ref = this.gridParts;
      for (i = _j = 0, _len = _ref.length; _j < _len; i = ++_j) {
        grid = _ref[i];
        $grid = $(grid);
        x = this.gridW * i;
        this.totalGridW = x;
        $grid.css("left", x + "px");
      }
      this.totalGridW += this.gridW;
      return this.onResize();
    };

    BackgroundGrid.prototype.onResize = function() {
      return this.gridHolder.css({
        left: (Model.windowW >> 1) - (this.totalGridW >> 1) + (this.gridW >> 1)
      });
    };

    return BackgroundGrid;

  })();
  return BackgroundGrid;
});
