var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["View"], function(View) {
  var Slideshow;
  Slideshow = (function(_super) {
    __extends(Slideshow, _super);

    Slideshow.prototype.parentId = void 0;

    Slideshow.prototype.bitmaps = void 0;

    Slideshow.prototype.slideContainer = void 0;

    Slideshow.prototype.currentContainerSlideNum = void 0;

    Slideshow.prototype.w = 960;

    Slideshow.prototype.h = 577;

    function Slideshow(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.hightlightDots = __bind(this.hightlightDots, this);
      this.animateContainer = __bind(this.animateContainer, this);
      this.updateSlideshow = __bind(this.updateSlideshow, this);
      this.dotClicked = __bind(this.dotClicked, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      var i, img, _i, _len, _ref;
      this.parentId = scope.parentId;
      this.bitmaps = [];
      scope.dot = Loader.getSvg("dot-circle");
      _ref = scope.images;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        img = _ref[i];
        this.bitmaps[i] = new createjs.Bitmap(Loader.getContentById(this.parentId + "-" + img));
        this.bitmaps[i].name = "slide-" + i;
      }
      scope.bitmaps = this.bitmaps;
      Slideshow.__super__.constructor.call(this, id, scope);
    }

    Slideshow.prototype.init = function() {
      return TweenMax.delayedCall(0, this.ready);
    };

    Slideshow.prototype.ready = function() {
      var $dotContainer, bitmap, i, _i, _len, _ref;
      $dotContainer = this.element.find(".dots-container");
      $dotContainer.css({
        width: $dotContainer.children(":first").outerWidth(true) * this.bitmaps.length + "px"
      });
      $dotContainer.find(".dot-holder").on("click", this.dotClicked);
      this.stage = Util.SetupCanvas({
        element: this.element.find("#slideshow"),
        width: this.w,
        height: this.h
      });
      this.slideContainer = new createjs.Container();
      this.stage.addChild(this.slideContainer);
      _ref = this.bitmaps;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        bitmap = _ref[i];
        bitmap.x = this.w * i;
        this.slideContainer.addChild(bitmap);
      }
      this.currentContainerSlideNum = 0;
      this.updateSlideshow(this.currentContainerSlideNum);
      return this.stage.update(event);
    };

    Slideshow.prototype.dotClicked = function(e) {
      var id, num;
      id = $(e.currentTarget).attr("id");
      num = parseInt(id.replace("slide-", ""), 10);
      return this.updateSlideshow(num);
    };

    Slideshow.prototype.updateSlideshow = function(num) {
      this.animateContainer(num);
      this.hightlightDots(num);
      return this.currentContainerSlideNum = num;
    };

    Slideshow.prototype.animateContainer = function(num) {
      var anim, newPos, oldPos,
        _this = this;
      oldPos = this.slideContainer.x;
      newPos = -(num * this.w);
      if (newPos !== oldPos) {
        anim = {
          value: oldPos
        };
        return TweenMax.to(anim, 1, {
          value: newPos,
          ease: Expo.easeInOut,
          onUpdate: function() {
            _this.slideContainer.x = anim.value;
            return _this.stage.update(event);
          }
        });
      }
    };

    Slideshow.prototype.hightlightDots = function(num) {
      var $child, $dots, child, id, _i, _len, _results;
      $dots = this.element.find(".dots-container").children();
      id = "slide-" + num;
      _results = [];
      for (_i = 0, _len = $dots.length; _i < _len; _i++) {
        child = $dots[_i];
        $child = $(child);
        if ($child.attr("id") === id) {
          _results.push($child.addClass("selected"));
        } else {
          _results.push($child.removeClass("selected"));
        }
      }
      return _results;
    };

    Slideshow.prototype.destroy = function() {
      return Slideshow.__super__.destroy.call(this);
    };

    return Slideshow;

  })(View);
  return Slideshow;
});
