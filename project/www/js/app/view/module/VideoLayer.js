var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define([], function() {
  var VideoLayer;
  VideoLayer = (function() {
    VideoLayer.prototype.videoIsReady = false;

    VideoLayer.prototype.videoLoopOnce = false;

    VideoLayer.prototype.videoUrl = void 0;

    VideoLayer.prototype.autoplay = true;

    VideoLayer.prototype.onReadyCallback = void 0;

    VideoLayer.prototype.firstTimeBufferReady = true;

    function VideoLayer(holder) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.pause = __bind(this.pause, this);
      this.restart = __bind(this.restart, this);
      this.play = __bind(this.play, this);
      this.seek = __bind(this.seek, this);
      this.getCurrentTime = __bind(this.getCurrentTime, this);
      this.onEnded = __bind(this.onEnded, this);
      this.onReady = __bind(this.onReady, this);
      this.proceedToOnReady = __bind(this.proceedToOnReady, this);
      this.onProgress = __bind(this.onProgress, this);
      this.init = __bind(this.init, this);
      this.holder = holder;
    }

    VideoLayer.prototype.init = function() {
      var html;
      html = '\
                <video id="project-video" preload="auto" autoplay>\
                    <source id="mp4_src" src="' + this.videoUrl + '.mp4' + '" type="video/mp4">\
                    <source id="ogg_src" src="' + this.videoUrl + '.theora.ogv' + '" type="video/ogg">\
                    <source id="webm_src" src="' + this.videoUrl + '.webm' + '" type="video/webm">\
                </video>     \
            ';
      this.element = $(html);
      this.holder.append(this.element);
      this.videoEl = this.element;
      this.video = this.videoEl.get()[0];
      this.videoEl.css({
        display: "none",
        position: "absolute"
      });
      this.videoEl.on("progress", this.onProgress);
      this.videoEl.on("ended", this.onEnded);
      this.video.load();
      Signal.onResize.add(this.resize);
    };

    VideoLayer.prototype.onProgress = function(e) {
      var i, middleDuration, ranges, _i, _ref;
      ranges = [];
      for (i = _i = 0, _ref = this.video.buffered.length - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        try {
          ranges.push([this.video.buffered.start(i), this.video.buffered.end(i)]);
        } catch (_error) {
          this.proceedToOnReady();
          return;
        }
      }
      middleDuration = this.video.duration * 0.5;
      if (ranges[0][1] > middleDuration) {
        this.proceedToOnReady();
      }
    };

    VideoLayer.prototype.proceedToOnReady = function() {
      this.onReady();
      return this.firstTimeBufferReady = false;
    };

    VideoLayer.prototype.onReady = function() {
      this.videoEl.off("progress", this.onProgress);
      this.videoEl.css("display", "block");
      this.videoIsReady = true;
      if (this.autoplay) {
        this.video.play();
      }
      if (!this.videoLoopOnce) {
        this.resize();
      }
      if (this.onReadyCallback != null) {
        this.onReadyCallback();
        this.onReadyCallback = void 0;
      }
    };

    VideoLayer.prototype.onEnded = function() {
      this.video.currentTime = 0;
      this.video.play();
      this.videoLoopOnce = true;
    };

    VideoLayer.prototype.getCurrentTime = function() {
      return this.video.currentTime;
    };

    VideoLayer.prototype.seek = function(time) {
      this.video.currentTime = time;
    };

    VideoLayer.prototype.play = function() {
      this.video.play();
    };

    VideoLayer.prototype.restart = function() {
      this.pause();
      this.seek(0);
      this.video.play();
    };

    VideoLayer.prototype.pause = function() {
      this.video.pause();
    };

    VideoLayer.prototype.resize = function() {
      var resizeParams, videoElCss, videoH, videoW;
      if (this.videoIsReady) {
        resizeParams = Util.GetAdjustmentContainerSize(Model.windowW, Model.windowH, Model.mediaW, Model.mediaH);
        videoW = resizeParams.type === "landscape" ? resizeParams.holderW : resizeParams.originalW * resizeParams.scaleY;
        videoH = resizeParams.type === "landscape" ? resizeParams.originalH * resizeParams.scaleX : resizeParams.holderH;
        this.videoEl.css({
          width: videoW,
          height: videoH
        });
        videoElCss = {
          left: (Model.windowW >> 1) - (this.videoEl.width() >> 1),
          top: (Model.windowH >> 1) - (this.videoEl.height() >> 1)
        };
        this.videoEl.css(videoElCss);
      }
    };

    VideoLayer.prototype.destroy = function() {
      Signal.onResize.remove(this.resize);
      this.videoEl.off("ended", this.onEnded);
      this.videoEl.off("progress", this.onProgress);
      this.videoEl.off();
      this.video.src = "";
      this.videoEl.children('source').prop('src', '');
      this.videoEl.remove().length = 0;
      this.element.remove();
      this.holder.remove();
    };

    return VideoLayer;

  })();
  return VideoLayer;
});
