var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["View"], function(View) {
  var Magnifier;
  Magnifier = (function(_super) {
    __extends(Magnifier, _super);

    function Magnifier(id, scope, i) {
      this.destroy = __bind(this.destroy, this);
      this.close = __bind(this.close, this);
      this.open = __bind(this.open, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      var uuid;
      uuid = THREE.Math.generateUUID();
      scope.uuid = uuid;
      scope.screenshot = Loader.getImageURL(scope.id + "-screenshot-" + i);
      scope.magnifierShadow = Loader.getImageURL("magnifier-shadow-image");
      Magnifier.__super__.constructor.call(this, id, scope);
    }

    Magnifier.prototype.init = function() {
      this.tl = new TimelineMax();
      return TweenMax.delayedCall(0, this.ready);
    };

    Magnifier.prototype.ready = function() {
      var $foreign, $gCircle, $maskCircle, $shadow, $svg, $svgCircle, baseDiameter, baseY, diameter, origin, radius, shadowY, time;
      diameter = this.element.width();
      radius = diameter >> 1;
      $svg = this.element.find("svg");
      $shadow = this.element.find(".shadow");
      $svgCircle = $svg.find("circle");
      $maskCircle = $svg.find("mask circle");
      $gCircle = $svg.find("g circle");
      $foreign = $svg.find("foreing");
      baseDiameter = 366;
      baseY = 33;
      shadowY = (diameter / baseDiameter) * baseY;
      TweenMax.set($svg, {
        width: diameter,
        height: diameter
      });
      TweenMax.set($foreign, {
        width: diameter,
        height: diameter
      });
      TweenMax.set($shadow, {
        width: diameter,
        height: diameter,
        y: shadowY
      });
      $svgCircle.attr("cx", radius);
      $svgCircle.attr("cy", radius);
      $svgCircle.attr("r", radius);
      time = 1;
      origin = "50% 50%";
      return this.tl.fromTo($maskCircle, time, {
        scaleX: 0,
        scaleY: 0,
        force3D: true,
        transformOrigin: origin
      }, {
        scaleX: 0.95,
        scaleY: 0.95,
        force3D: true,
        transformOrigin: origin,
        ease: Expo.easeOut
      }, 0.4).fromTo($gCircle, time, {
        scaleX: 0,
        scaleY: 0,
        force3D: true,
        transformOrigin: origin
      }, {
        scaleX: 1,
        scaleY: 1,
        force3D: true,
        transformOrigin: origin,
        ease: Expo.easeOut
      }, 0.3).fromTo($svg, time, {
        scaleX: 1.2,
        scaleY: 1.2,
        force3D: true,
        transformOrigin: origin
      }, {
        scaleX: 1,
        scaleY: 1,
        force3D: true,
        transformOrigin: origin,
        ease: Expo.easeOut
      }, 0.5).fromTo($shadow, time, {
        scaleX: 0,
        scaleY: 0,
        force3D: true,
        transformOrigin: origin
      }, {
        scaleX: 1,
        scaleY: 1,
        force3D: true,
        transformOrigin: origin,
        ease: Expo.easeOut
      }, 0.3).pause(0);
    };

    Magnifier.prototype.open = function() {
      this.tl.timeScale(1.3);
      return this.tl.play();
    };

    Magnifier.prototype.close = function() {
      return this.tl.pause(0);
    };

    Magnifier.prototype.destroy = function() {
      this.tl.clear();
      this.tl = null;
      return Magnifier.__super__.destroy.call(this);
    };

    return Magnifier;

  })(View);
  return Magnifier;
});
