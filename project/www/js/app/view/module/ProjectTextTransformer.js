var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["SplitText"], function(SplitText) {
  var ProjectTextTransformer;
  ProjectTextTransformer = (function() {
    ProjectTextTransformer.prototype.texts = void 0;

    function ProjectTextTransformer() {
      this.destroy = __bind(this.destroy, this);
      this.getTextFromId = __bind(this.getTextFromId, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.init = __bind(this.init, this);
      this.texts = [];
      return;
    }

    ProjectTextTransformer.prototype.init = function(textsEl) {
      var $highlightBox, $highlights, $hl, $line, $p, $pAfter, $paragraphHolder, $t, $textContainer, allLines, chapter, chapterName, classArray, fromRectStr, hl, i, j, line, lineH, mySplitText, pWidth, t, tl, toRectStr, w, x, y, _i, _j, _k, _len, _len1, _len2;
      for (j = _i = 0, _len = textsEl.length; _i < _len; j = ++_i) {
        t = textsEl[j];
        chapter = {};
        $t = $(t);
        classArray = $t.attr("class").split(" ");
        chapterName = classArray[classArray.length - 1];
        $textContainer = $t.find(".text-container");
        $paragraphHolder = $textContainer.find(".paragraph-holder");
        $p = $textContainer.find("p");
        $pAfter = $p.clone();
        lineH = parseInt($p.css("line-height").replace(/[^-\d\.]/g, ''), 10);
        pWidth = $paragraphHolder.width();
        fromRectStr = Util.GetRect(0, 0, lineH, 0);
        toRectStr = Util.GetRect(0, pWidth, lineH, 0);
        tl = new TimelineMax();
        chapter.name = chapterName;
        chapter.tl = tl;
        mySplitText = new SplitText($p, {
          type: "lines",
          linesClass: "line++"
        });
        allLines = $p.find("div");
        for (i = _j = 0, _len1 = allLines.length; _j < _len1; i = ++_j) {
          line = allLines[i];
          $line = $(line);
          $line.css({
            position: "absolute",
            top: (lineH * i) + "px",
            height: lineH + "px"
          });
          tl.fromTo($line, 1, {
            clip: fromRectStr
          }, {
            clip: toRectStr,
            force3D: true,
            ease: Expo.easeOut
          }, 0.1 * i);
        }
        $p.addClass("temp");
        $paragraphHolder.append($pAfter);
        $pAfter.addClass("bottom-p");
        $highlights = $pAfter.find("span.highlight");
        for (i = _k = 0, _len2 = $highlights.length; _k < _len2; i = ++_k) {
          hl = $highlights[i];
          $hl = $(hl);
          $highlightBox = $("<div class='highlight-box'>");
          w = $hl.width() + 3;
          x = $hl.position().left - 1;
          y = $hl.position().top - ($hl.height() >> 1);
          $highlightBox.css({
            width: w + "px",
            left: x + "px",
            top: y + "px"
          });
          tl.fromTo($highlightBox, 1, {
            scaleX: 0,
            force3D: true,
            transformOrigin: "20% 50%"
          }, {
            scaleX: 1,
            force3D: true,
            transformOrigin: "0% 50%",
            ease: Expo.easeOut
          }, 1 + (0.1 * i));
          $textContainer.append($highlightBox);
        }
        tl.pause(0);
        this.texts[j] = chapter;
      }
    };

    ProjectTextTransformer.prototype.transitionIn = function(id) {
      var text;
      text = this.getTextFromId(id);
      if (text == null) {
        return;
      }
      text.tl.timeScale(1.4);
      text.tl.play();
    };

    ProjectTextTransformer.prototype.transitionOut = function(id) {
      var text;
      text = this.getTextFromId(id);
      if (text == null) {
        return;
      }
      text.tl.pause(0);
    };

    ProjectTextTransformer.prototype.getTextFromId = function(id) {
      var t, text, _i, _len, _ref;
      _ref = this.texts;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        t = _ref[_i];
        if ("." + t.name === id) {
          text = t;
          break;
        }
      }
      return text;
    };

    ProjectTextTransformer.prototype.destroy = function() {
      var t, _i, _len, _ref;
      _ref = this.texts;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        t = _ref[_i];
        t.tl.clear();
        t.tl = null;
        t = null;
      }
      this.texts.length = 0;
      return this.texts = null;
    };

    return ProjectTextTransformer;

  })();
  return ProjectTextTransformer;
});
