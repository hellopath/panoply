var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["SplineInterpolator"], function(spline) {
  var HomeProjectCanvas;
  HomeProjectCanvas = (function() {
    HomeProjectCanvas.prototype.element = void 0;

    HomeProjectCanvas.prototype.id = void 0;

    HomeProjectCanvas.prototype.canvas = void 0;

    HomeProjectCanvas.prototype.params = void 0;

    function HomeProjectCanvas() {
      this.deleteGlAssets = __bind(this.deleteGlAssets, this);
      this.getFilterParamsById = __bind(this.getFilterParamsById, this);
      this.clearCanvas = __bind(this.clearCanvas, this);
      this.removeCanvas = __bind(this.removeCanvas, this);
      this.appendCanvas = __bind(this.appendCanvas, this);
      this.resize = __bind(this.resize, this);
      this.drawRectangle = __bind(this.drawRectangle, this);
      this.update = __bind(this.update, this);
      this.render = __bind(this.render, this);
      this.setRectangle = __bind(this.setRectangle, this);
      this.clamp = __bind(this.clamp, this);
      this.splineInterpolate = __bind(this.splineInterpolate, this);
      this.init = __bind(this.init, this);
    }

    HomeProjectCanvas.prototype.init = function() {
      var fragmentShaderStr, i, points, red, vertexShaderStr, _i, _ref, _results;
      this.$canvas = $('<canvas id="' + this.id + '"></canvas>');
      this.canvas = this.$canvas.get()[0];
      fragmentShaderStr = Loader.getShader("image-filter-fragment");
      vertexShaderStr = Loader.getShader("image-filter-vertex");
      this.image = Loader.getContentById("home-" + this.id);
      this.gl = getWebGLContext(this.canvas);
      this.program = createProgramFromSources(this.gl, [vertexShaderStr, fragmentShaderStr]);
      this.gl.useProgram(this.program);
      this.params = this.getFilterParamsById();
      points = this.params.curves;
      red = this.splineInterpolate(points);
      this.curveDataArray = [];
      _results = [];
      for (i = _i = 0, _ref = 256 - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        _results.push(this.curveDataArray.splice(this.curveDataArray.length, 0, red[i], red[i], red[i], 255));
      }
      return _results;
    };

    HomeProjectCanvas.prototype.splineInterpolate = function(points) {
      var array, i, interpolator, _i, _ref;
      interpolator = new SplineInterpolator(points);
      array = [];
      for (i = _i = 0, _ref = 256 - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        array.push(this.clamp(0, Math.floor(interpolator.interpolate(i / 255) * 256), 255));
      }
      return array;
    };

    HomeProjectCanvas.prototype.clamp = function(lo, value, hi) {
      return Math.max(lo, Math.min(value, hi));
    };

    HomeProjectCanvas.prototype.setRectangle = function(gl, x, y, width, height) {
      var x1, x2, y1, y2;
      x1 = x;
      x2 = x + width;
      y1 = y;
      y2 = y + height;
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([x1, y1, x2, y1, x1, y2, x1, y2, x2, y1, x2, y2]), gl.STATIC_DRAW);
    };

    HomeProjectCanvas.prototype.render = function() {
      this.update();
      return requestAnimationFrame(this.render);
    };

    HomeProjectCanvas.prototype.update = function() {};

    HomeProjectCanvas.prototype.drawRectangle = function() {
      var brightness, contrast, positionLocation, rectH, rectW, saturation, texCoordLocation, textureHeight, textureLocation0, textureLocation1, textureWidth, timer;
      positionLocation = this.gl.getAttribLocation(this.program, "a_position");
      texCoordLocation = this.gl.getAttribLocation(this.program, "a_texCoord");
      this.texCoordBuffer = this.gl.createBuffer();
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.texCoordBuffer);
      this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array([0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0]), this.gl.STATIC_DRAW);
      this.gl.enableVertexAttribArray(texCoordLocation);
      this.gl.vertexAttribPointer(texCoordLocation, 2, this.gl.FLOAT, false, 0, 0);
      this.texture0 = this.gl.createTexture();
      this.gl.activeTexture(this.gl.TEXTURE0);
      this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture0);
      textureLocation0 = this.gl.getUniformLocation(this.program, "bgl_RenderedTexture0");
      this.gl.uniform1i(textureLocation0, 0);
      this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
      this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
      this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST);
      this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST);
      this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, this.gl.RGBA, this.gl.UNSIGNED_BYTE, this.image);
      this.texture1 = this.gl.createTexture();
      this.gl.activeTexture(this.gl.TEXTURE1);
      this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture1);
      textureLocation1 = this.gl.getUniformLocation(this.program, "bgl_RenderedTexture1");
      this.gl.uniform1i(textureLocation1, 1);
      this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
      this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
      this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.NEAREST);
      this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.NEAREST);
      this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGBA, 256, 1, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, new Uint8Array(this.curveDataArray));
      this.resolutionLocation = this.gl.getUniformLocation(this.program, "u_resolution");
      this.gl.uniform2f(this.resolutionLocation, this.canvas.width, this.canvas.height);
      this.buffer = this.gl.createBuffer();
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer);
      this.gl.enableVertexAttribArray(positionLocation);
      this.gl.vertexAttribPointer(positionLocation, 2, this.gl.FLOAT, false, 0, 0);
      rectW = this.canvas.width;
      rectH = this.canvas.height;
      this.setRectangle(this.gl, 0, 0, rectW, rectH);
      textureWidth = this.gl.getUniformLocation(this.program, "bgl_RenderedTextureWidth");
      this.gl.uniform1f(textureWidth, rectW);
      textureHeight = this.gl.getUniformLocation(this.program, "bgl_RenderedTextureHeight");
      this.gl.uniform1f(textureHeight, rectH);
      timer = this.gl.getUniformLocation(this.program, "timer");
      this.gl.uniform1f(timer, 1.3333);
      saturation = this.gl.getUniformLocation(this.program, "saturation");
      this.gl.uniform1f(saturation, this.params.saturation);
      brightness = this.gl.getUniformLocation(this.program, "brightness");
      this.gl.uniform1f(brightness, this.params.brightness);
      contrast = this.gl.getUniformLocation(this.program, "contrast");
      this.gl.uniform1f(contrast, this.params.contrast);
      return this.gl.drawArrays(this.gl.TRIANGLES, 0, 6);
    };

    HomeProjectCanvas.prototype.resize = function() {
      var displayHeight, displayWidth, realToCSSPixels;
      this.clearCanvas();
      this.$canvas.css({
        width: this.canvasCss.width,
        height: this.canvasCss.height,
        top: this.canvasCss.top,
        left: this.canvasCss.left
      });
      TweenMax.set(this.canvas, {
        scale: this.canvasCss.scale,
        transformOrigin: "50% 50%"
      });
      realToCSSPixels = 1;
      displayWidth = Math.floor(this.gl.canvas.clientWidth * realToCSSPixels);
      displayHeight = Math.floor(this.gl.canvas.clientHeight * realToCSSPixels);
      this.canvas.width = displayWidth;
      this.canvas.height = displayHeight;
      this.gl.canvas.width = displayWidth;
      this.gl.canvas.height = displayHeight;
      this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);
      return this.drawRectangle();
    };

    HomeProjectCanvas.prototype.appendCanvas = function() {
      return this.element.find(".relative").prepend(this.$canvas);
    };

    HomeProjectCanvas.prototype.removeCanvas = function() {
      this.clearCanvas();
      return this.$canvas.remove();
    };

    HomeProjectCanvas.prototype.clearCanvas = function() {
      this.gl.clearColor(1.0, 1.0, 1.0, 0.5);
      this.gl.clear(this.gl.COLOR_BUFFER_BIT);
      return this.deleteGlAssets();
    };

    HomeProjectCanvas.prototype.getFilterParamsById = function() {
      var route, _i, _len, _ref;
      _ref = Model.routing;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        route = _ref[_i];
        if (route.id === this.id) {
          return route.filterParams;
        }
      }
    };

    HomeProjectCanvas.prototype.deleteGlAssets = function() {
      this.gl.deleteTexture(this.texture0);
      this.gl.deleteBuffer(this.buffer);
      this.gl.deleteBuffer(this.texCoordBuffer);
      this.gl.canvas.width = 1;
      return this.gl.canvas.height = 1;
    };

    return HomeProjectCanvas;

  })();
  return HomeProjectCanvas;
});
