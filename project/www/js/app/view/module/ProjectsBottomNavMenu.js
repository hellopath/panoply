var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["View", "SplitText"], function(View, SplitText) {
  var ProjectsBottomNavMenu;
  ProjectsBottomNavMenu = (function(_super) {
    __extends(ProjectsBottomNavMenu, _super);

    function ProjectsBottomNavMenu(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.onBtnOut = __bind(this.onBtnOut, this);
      this.onBtnOver = __bind(this.onBtnOver, this);
      this.textSplitter = __bind(this.textSplitter, this);
      this.addAnimation = __bind(this.addAnimation, this);
      this.init = __bind(this.init, this);
      scope = Router.getNextPreviousProject();
      scope.lineArrowPartial = Loader.getPartial("head-line-arrow");
      scope.headArrow = Loader.getSvg("head-arrow");
      scope.headArrowWide = Loader.getSvg("head-arrow-wide");
      ProjectsBottomNavMenu.__super__.constructor.call(this, id, scope);
    }

    ProjectsBottomNavMenu.prototype.init = function() {
      var $arrowLeft, $arrowRight;
      this.rightBtnPart = this.element.find(".right-btn-part");
      this.leftBtnPart = this.element.find(".left-btn-part");
      this.rightHolder = this.rightBtnPart.find(".middle-holder");
      this.leftHolder = this.leftBtnPart.find(".middle-holder");
      this.rightBtn = this.rightBtnPart.find(".inside-part");
      this.leftBtn = this.leftBtnPart.find(".inside-part");
      this.btnW = this.rightBtn.width();
      this.btnH = this.rightBtn.height();
      this.rightBtn.on("mouseover", this.onBtnOver);
      this.rightBtn.on("mouseout", this.onBtnOut);
      this.leftBtn.on("mouseover", this.onBtnOver);
      this.leftBtn.on("mouseout", this.onBtnOut);
      this.leftTl = new TimelineMax();
      this.rightTl = new TimelineMax();
      $arrowLeft = this.leftHolder.find(".arrow-holder");
      $arrowRight = this.rightHolder.find(".arrow-holder");
      this.leftSplitTexts = this.textSplitter(this.leftHolder);
      this.rightSplitTexts = this.textSplitter(this.rightHolder);
      this.addAnimation(this.leftTl, this.leftSplitTexts, $arrowLeft, this.leftHolder, "left");
      return this.addAnimation(this.rightTl, this.rightSplitTexts, $arrowRight, this.rightHolder, "right");
    };

    ProjectsBottomNavMenu.prototype.addAnimation = function(tl, splitter, arrow, btnHolder, type) {
      var $titlesHolder, offsetX,
        _this = this;
      $titlesHolder = btnHolder.find(".titles-holder");
      offsetX = type === "left" ? 20 : -20;
      tl.staggerFromTo(splitter.down.chars, 0.8, {
        y: 0,
        opacity: 1,
        force3D: true,
        ease: Expo.easeInOut
      }, {
        y: 7,
        opacity: 0,
        force3D: true,
        ease: Expo.easeInOut
      }, 0.06, 0);
      tl.staggerFromTo(splitter.up.chars, 0.8, {
        y: -7,
        opacity: 0,
        force3D: true,
        ease: Expo.easeInOut
      }, {
        y: 0,
        opacity: 1,
        force3D: true,
        ease: Expo.easeInOut
      }, 0.06, 0.1);
      tl.to($titlesHolder, 0.8, {
        x: offsetX,
        force3D: true,
        ease: Expo.easeInOut
      }, 0.3);
      tl.addCallback(function() {
        if (tl.reversed()) {
          return arrow.removeClass("hovered");
        } else {
          return arrow.addClass("hovered");
        }
      }, 0.26);
      tl.pause(0);
      return tl.timeScale(2);
    };

    ProjectsBottomNavMenu.prototype.textSplitter = function($el) {
      var down, up;
      up = new SplitText($el.find(".title.up"), {
        type: "words, chars"
      });
      down = new SplitText($el.find(".title.down"), {
        type: "words, chars"
      });
      return {
        up: up,
        down: down
      };
    };

    ProjectsBottomNavMenu.prototype.onBtnOver = function(e) {
      var $target, id;
      e.preventDefault();
      $target = $(e.currentTarget);
      id = $target.attr("id");
      if (id === "left") {
        return this.leftTl.play();
      } else {
        return this.rightTl.play();
      }
    };

    ProjectsBottomNavMenu.prototype.onBtnOut = function(e) {
      var $target, id;
      e.preventDefault();
      $target = $(e.currentTarget);
      id = $target.attr("id");
      if (id === "left") {
        return this.leftTl.reverse();
      } else {
        return this.rightTl.reverse();
      }
    };

    ProjectsBottomNavMenu.prototype.resize = function() {
      var leftBtnPartCss, leftCss, rightBtnPartCss, rightCss;
      rightBtnPartCss = {
        width: Model.windowW >> 1
      };
      leftBtnPartCss = {
        width: Model.windowW >> 1
      };
      rightCss = {
        left: (this.btnW >> 1) - (this.rightHolder.width() >> 1),
        top: (this.btnH >> 1) - (this.rightHolder.height() >> 1)
      };
      leftCss = {
        left: (this.btnW >> 1) - (this.leftHolder.width() >> 1),
        top: (this.btnH >> 1) - (this.leftHolder.height() >> 1)
      };
      this.rightBtnPart.css(rightBtnPartCss);
      this.leftBtnPart.css(leftBtnPartCss);
      this.rightHolder.css(rightCss);
      return this.leftHolder.css(leftCss);
    };

    ProjectsBottomNavMenu.prototype.destroy = function() {
      this.leftTl.clear();
      this.rightTl.clear();
      this.leftSplitTexts = null;
      this.rightSplitTexts = null;
      this.rightBtn.off("mouseover", this.onBtnOver);
      this.rightBtn.off("mouseout", this.onBtnOut);
      this.leftBtn.off("mouseover", this.onBtnOver);
      this.leftBtn.off("mouseout", this.onBtnOut);
      return ProjectsBottomNavMenu.__super__.destroy.call(this);
    };

    return ProjectsBottomNavMenu;

  })(View);
  return ProjectsBottomNavMenu;
});
