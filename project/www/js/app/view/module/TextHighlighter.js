var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["View"], function(View) {
  var TextHighlighter;
  TextHighlighter = (function() {
    TextHighlighter.prototype.chaptersEl = void 0;

    TextHighlighter.prototype.allChapters = void 0;

    function TextHighlighter() {
      this.destroy = __bind(this.destroy, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.getTimelineByClass = __bind(this.getTimelineByClass, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
    }

    TextHighlighter.prototype.init = function() {
      this.allChapters = [];
      return this.ready();
    };

    TextHighlighter.prototype.ready = function() {
      var $chapter, $highlightBox, $highlightHolder, $highlights, $hl, chapter, cp, hl, i, tl, w, x, y, _i, _j, _len, _len1, _ref, _results;
      _ref = this.chaptersEl;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        chapter = _ref[_i];
        cp = {};
        $chapter = $(chapter);
        cp.name = Util.GetChapterUniqueClass($chapter);
        $highlightHolder = $chapter.find(".text-container");
        $highlights = $chapter.find(".text-container:not(.clone) span.highlight");
        tl = new TimelineMax();
        cp.tl = tl;
        for (i = _j = 0, _len1 = $highlights.length; _j < _len1; i = ++_j) {
          hl = $highlights[i];
          $hl = $(hl);
          $highlightBox = $("<div class='highlight-box'>");
          w = $hl.width() + 3;
          x = $hl.position().left - 1;
          y = $hl.position().top - ($hl.height() >> 1);
          $highlightBox.css({
            width: w + "px",
            left: x + "px",
            top: y + "px"
          });
          tl.fromTo($highlightBox, 1, {
            scaleX: 0,
            transformOrigin: "20% 50%"
          }, {
            scaleX: 1,
            transformOrigin: "0% 50%",
            ease: Expo.easeOut
          }, 0.6 + (0.1 * i));
          $highlightHolder.append($highlightBox);
        }
        tl.pause(0);
        _results.push(this.allChapters.push(cp));
      }
      return _results;
    };

    TextHighlighter.prototype.getTimelineByClass = function(clazz) {
      var cp, tl, _i, _len, _ref;
      _ref = this.allChapters;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        cp = _ref[_i];
        if (cp.name === clazz) {
          tl = cp.tl;
          break;
        }
      }
      return tl;
    };

    TextHighlighter.prototype.transitionIn = function(tl) {
      return tl.play();
    };

    TextHighlighter.prototype.transitionOut = function(tl) {
      return tl.pause(0);
    };

    TextHighlighter.prototype.destroy = function() {
      var chapter, _i, _len, _ref;
      _ref = this.allChapters;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        chapter = _ref[_i];
        chapter.tl.clear();
        chapter.tl = null;
        chapter = null;
      }
      this.allChapters.length = 0;
      return this.allChapters = null;
    };

    return TextHighlighter;

  })();
  return TextHighlighter;
});
