var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["View"], function(View) {
  "use strict";
  var Header;
  Header = (function(_super) {
    __extends(Header, _super);

    function Header(id) {
      this.init = __bind(this.init, this);
      var scope;
      scope = {};
      scope.logo = Loader.getImageURL("logo-image");
      scope.email = Loader.getImageURL("email-image");
      scope.weare = Loader.getImageURL("weare-image");
      Header.__super__.constructor.call(this, id, scope);
      return;
    }

    Header.prototype.init = function() {
      var $logo, $weAre;
      CSSPlugin.defaultTransformPerspective = 1000;
      $logo = this.element.find(".logo");
      $weAre = this.element.find(".weare");
      this.tl = new TimelineMax({
        repeat: -1,
        repeatDelay: 4
      });
      this.tl.to($logo, 2, {
        directionalRotation: {
          rotationX: -180 + "_ccw"
        },
        force3D: true,
        ease: Expo.easeInOut,
        transformOrigin: "50% 50% -100"
      }, 0);
      this.tl.from($weAre, 2, {
        directionalRotation: {
          rotationX: 180 + "_cw"
        },
        force3D: true,
        ease: Expo.easeInOut,
        transformOrigin: "50% 50% -100"
      }, 0);
      this.tl.to($logo, 2, {
        directionalRotation: {
          rotationX: 0 + "_ccw"
        },
        force3D: true,
        ease: Expo.easeInOut,
        transformOrigin: "50% 50% -100"
      }, 4);
      this.tl.to($weAre, 2, {
        directionalRotation: {
          rotationX: 180 + "_ccw"
        },
        force3D: true,
        ease: Expo.easeInOut,
        transformOrigin: "50% 50% -100"
      }, 4);
      this.tl.play(0);
      this.tl.timeScale(0.8);
    };

    return Header;

  })(View);
  return Header;
});
