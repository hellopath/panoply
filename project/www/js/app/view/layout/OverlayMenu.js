var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["View", "BackgroundGrid"], function(View, BackgroundGrid) {
  var OverlayMenu;
  OverlayMenu = (function(_super) {
    __extends(OverlayMenu, _super);

    function OverlayMenu(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.onMenuToggled = __bind(this.onMenuToggled, this);
      this.onReverseCompleted = __bind(this.onReverseCompleted, this);
      this.close = __bind(this.close, this);
      this.open = __bind(this.open, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.onResize = __bind(this.onResize, this);
      this.init = __bind(this.init, this);
      var i, route, routes, _i, _len, _ref;
      scope = {};
      routes = [];
      _ref = Model.routing;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        route = _ref[i];
        if (route.parentRoute === "project") {
          routes.push(route);
        }
      }
      scope.projects = routes;
      OverlayMenu.__super__.constructor.call(this, id, scope);
    }

    OverlayMenu.prototype.init = function() {
      this.projectsColumn = this.element.find(".projects-column");
      this.infosColumn = this.element.find(".infos-column");
      this.backgroundGridHolder = this.element.find(".background-grid");
      this.grid = new BackgroundGrid();
      this.grid.element = this.backgroundGridHolder;
      this.grid.init();
      this.onResize();
      this.addAnimations();
      $(window).on("resize", this.onResize);
      Signal.toggleMenu.add(this.onMenuToggled);
      return this.element.css("display", "none");
    };

    OverlayMenu.prototype.onResize = function() {
      var infosColumnCss, projectsColumnCss;
      this.element.find("");
      projectsColumnCss = {
        top: (Model.windowH >> 1) - (this.projectsColumn.height() >> 1)
      };
      infosColumnCss = {
        top: (Model.windowH >> 1) - (this.infosColumn.height() >> 1) + 10
      };
      TweenMax.set(this.projectsColumn, projectsColumnCss);
      return TweenMax.set(this.infosColumn, infosColumnCss);
    };

    OverlayMenu.prototype.addAnimations = function() {
      var $animationParts, $gridParts, $p, $projectsTop, delay, i, p, _i, _j, _k, _len, _len1, _len2;
      $projectsTop = this.projectsColumn.find(".project-top");
      $animationParts = this.infosColumn.find(".anim-part");
      $gridParts = this.grid.element.find(".grid-part");
      delay = 0.5;
      this.tl = new TimelineMax({
        onReverseComplete: this.onReverseCompleted
      });
      for (i = _i = 0, _len = $gridParts.length; _i < _len; i = ++_i) {
        p = $gridParts[i];
        $p = $(p);
        this.tl.from($p, 1, {
          scaleX: 0,
          force3D: true,
          transformOrigin: "0% 0%",
          ease: Circ.easeInOut
        }, (delay * 0) + (i * 0.05));
      }
      for (i = _j = 0, _len1 = $projectsTop.length; _j < _len1; i = ++_j) {
        p = $projectsTop[i];
        $p = $(p);
        this.tl.from($p, 1, {
          x: -$p.width() - 40,
          force3D: true,
          transformOrigin: "0% 0%",
          ease: Expo.easeInOut
        }, 0.3 + (delay * 1) + (i * 0.07));
      }
      for (i = _k = 0, _len2 = $animationParts.length; _k < _len2; i = ++_k) {
        p = $animationParts[i];
        $p = $(p);
        this.tl.from($p, 1, {
          x: -$p.width() - 40,
          force3D: true,
          transformOrigin: "0% 0%",
          ease: Expo.easeInOut
        }, 0.3 + (delay * 2) + (i * 0.07));
      }
      return this.tl.pause(0);
    };

    OverlayMenu.prototype.open = function() {
      this.element.css("display", "block");
      this.onResize();
      this.tl.timeScale(1.4);
      return this.tl.play();
    };

    OverlayMenu.prototype.close = function() {
      this.tl.timeScale(2.2);
      return this.tl.reverse();
    };

    OverlayMenu.prototype.onReverseCompleted = function() {
      return this.element.css("display", "none");
    };

    OverlayMenu.prototype.onMenuToggled = function(mode) {
      switch (mode) {
        case "open":
          return this.open();
        case "close":
          return this.close();
      }
    };

    OverlayMenu.prototype.destroy = function() {
      return OverlayMenu.__super__.destroy.call(this);
    };

    return OverlayMenu;

  })(View);
  return OverlayMenu;
});
