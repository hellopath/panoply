var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project"], function(Project) {
  var DiorAddict;
  DiorAddict = (function(_super) {
    __extends(DiorAddict, _super);

    function DiorAddict(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutElement = __bind(this.transitionOutElement, this);
      this.transitionInElement = __bind(this.transitionInElement, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      scope.chapterTxt = Model.content["chapter-txt"];
      scope.flowerA = Loader.getImageURL(id + "-flower-a");
      scope.flowerB = Loader.getImageURL(id + "-flower-b");
      scope.girlA = Loader.getImageURL(id + "-girl-a");
      scope.girlWideA = Loader.getImageURL(id + "-girl-wide-a");
      scope.girlWideB = Loader.getImageURL(id + "-girl-wide-b");
      scope.butterfly = Loader.getImageURL(id + "-butterfly");
      scope.packageA = Loader.getImageURL(id + "-package-a");
      scope.packageB = Loader.getImageURL(id + "-package-b");
      scope.mandarine = Loader.getImageURL(id + "-mandarine");
      scope.iPhone = Loader.getImageURL(id + "-i-phone");
      scope.laptopBig = Loader.getImageURL(id + "-laptop");
      scope.vanilia = Loader.getImageURL(id + "-vanilia");
      scope.canelle = Loader.getImageURL(id + "-canelle");
      scope.laptop = Loader.getSvg("laptop-dior");
      scope.dotsConnection = Loader.getSvg("dots-connection");
      scope.mobile = Loader.getSvg("dior-mobile");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      DiorAddict.__super__.constructor.call(this, id, scope);
    }

    DiorAddict.prototype.init = function() {
      this.scrollPositionCheckElements.push({
        className: ".mirroring-sketch",
        animationType: G.ALWAYS
      });
      return DiorAddict.__super__.init.call(this);
    };

    DiorAddict.prototype.ready = function() {
      return DiorAddict.__super__.ready.call(this);
    };

    DiorAddict.prototype.addAnimations = function() {
      var $dots;
      $dots = this.element.find("svg#dots-connection circle");
      this.connectionDotsTl = new TimelineMax({
        repeat: -1
      });
      this.connectionDotsTl.staggerFrom($dots, 0.5, {
        opacity: 0.2,
        ease: Expo.easeOut
      }, 0.06);
      this.connectionDotsTl.staggerTo($dots, 0.5, {
        opacity: 0.2,
        ease: Expo.easeOut
      }, 0.06);
      this.connectionDotsTl.pause(0, false);
      this.connectionDotsTl.timeScale(1.6);
      return DiorAddict.__super__.addAnimations.call(this);
    };

    DiorAddict.prototype.transitionIn = function() {
      return DiorAddict.__super__.transitionIn.call(this);
    };

    DiorAddict.prototype.transitionOut = function() {
      return DiorAddict.__super__.transitionOut.call(this);
    };

    DiorAddict.prototype.transitionInCompleted = function() {
      return DiorAddict.__super__.transitionInCompleted.call(this);
    };

    DiorAddict.prototype.transitionOutCompleted = function() {
      return DiorAddict.__super__.transitionOutCompleted.call(this);
    };

    DiorAddict.prototype.transitionInElement = function(obj) {
      DiorAddict.__super__.transitionInElement.call(this, obj);
      switch (obj.id) {
        case ".mirroring-sketch":
          this.connectionDotsTl.play(0);
      }
    };

    DiorAddict.prototype.transitionOutElement = function(obj) {
      DiorAddict.__super__.transitionOutElement.call(this, obj);
      switch (obj.id) {
        case ".mirroring-sketch":
          this.connectionDotsTl.pause(0, false);
      }
    };

    DiorAddict.prototype.resize = function() {
      return DiorAddict.__super__.resize.call(this);
    };

    DiorAddict.prototype.destroy = function() {
      if (this.connectionDotsTl != null) {
        this.connectionDotsTl.kill();
        this.connectionDotsTl.clear();
      }
      return DiorAddict.__super__.destroy.call(this);
    };

    return DiorAddict;

  })(Project);
  return DiorAddict;
});
