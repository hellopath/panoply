var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project"], function(Project) {
  var OrchestreNationalDeLorraine;
  OrchestreNationalDeLorraine = (function(_super) {
    __extends(OrchestreNationalDeLorraine, _super);

    function OrchestreNationalDeLorraine(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      scope.letterA = Loader.getImageURL(id + "-letter-a");
      scope.letterB = Loader.getImageURL(id + "-letter-b");
      scope.handA = Loader.getImageURL(id + "-hand-a");
      scope.handB = Loader.getImageURL(id + "-hand-b");
      scope.headA = Loader.getImageURL(id + "-head-a");
      scope.headB = Loader.getImageURL(id + "-head-b");
      scope.screenA = Loader.getImageURL(id + "-screen-a");
      scope.stamp = Loader.getImageURL(id + "-stamp");
      scope.laptop = Loader.getImageURL(id + "-laptop");
      scope.ventaglia = Loader.getImageURL(id + "-ventaglia");
      scope.stairs = Loader.getImageURL(id + "-stairs");
      scope.story = Loader.getImageURL(id + "-story");
      scope.chapterTxt = Model.content["chapter-txt"];
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      OrchestreNationalDeLorraine.__super__.constructor.call(this, id, scope);
    }

    OrchestreNationalDeLorraine.prototype.init = function() {
      return OrchestreNationalDeLorraine.__super__.init.call(this);
    };

    OrchestreNationalDeLorraine.prototype.ready = function() {
      return OrchestreNationalDeLorraine.__super__.ready.call(this);
    };

    OrchestreNationalDeLorraine.prototype.addAnimations = function() {
      return OrchestreNationalDeLorraine.__super__.addAnimations.call(this);
    };

    OrchestreNationalDeLorraine.prototype.transitionIn = function() {
      return OrchestreNationalDeLorraine.__super__.transitionIn.call(this);
    };

    OrchestreNationalDeLorraine.prototype.transitionOut = function() {
      return OrchestreNationalDeLorraine.__super__.transitionOut.call(this);
    };

    OrchestreNationalDeLorraine.prototype.transitionInCompleted = function() {
      return OrchestreNationalDeLorraine.__super__.transitionInCompleted.call(this);
    };

    OrchestreNationalDeLorraine.prototype.transitionOutCompleted = function() {
      return OrchestreNationalDeLorraine.__super__.transitionOutCompleted.call(this);
    };

    OrchestreNationalDeLorraine.prototype.resize = function() {
      return OrchestreNationalDeLorraine.__super__.resize.call(this);
    };

    OrchestreNationalDeLorraine.prototype.destroy = function() {
      return OrchestreNationalDeLorraine.__super__.destroy.call(this);
    };

    return OrchestreNationalDeLorraine;

  })(Project);
  return OrchestreNationalDeLorraine;
});
