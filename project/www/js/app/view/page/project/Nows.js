var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project"], function(Project) {
  var Nows;
  Nows = (function(_super) {
    __extends(Nows, _super);

    function Nows(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutElement = __bind(this.transitionOutElement, this);
      this.transitionInElement = __bind(this.transitionInElement, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      scope.gasoilNow = Loader.getImageURL(id + "-gasoil-now");
      scope.lumiaA = Loader.getImageURL(id + "-lumia-a");
      scope.lumiaB = Loader.getImageURL(id + "-lumia-b");
      scope.iPhone = Loader.getImageURL(id + "-i-phone");
      scope.shadows = Loader.getImageURL(id + "-shadows");
      scope.julyLogo = Loader.getImageURL(id + "-july-logo");
      scope.keys = Loader.getImageURL(id + "-keys");
      scope.screenA = Loader.getImageURL(id + "-screen-a");
      scope.screenB = Loader.getImageURL(id + "-screen-b");
      scope.mobileAppA = Loader.getImageURL(id + "-mobile-app-a");
      scope.mobileAppB = Loader.getImageURL(id + "-mobile-app-b");
      scope.goTicket = Loader.getImageURL(id + "-go-ticket");
      scope.lollipop = Loader.getImageURL(id + "-lollipop");
      scope.laptop = Loader.getImageURL(id + "-laptop");
      scope.stamps = Loader.getImageURL(id + "-stamps");
      scope.passports = Loader.getImageURL(id + "-passports");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      scope.chapterTxt = Model.content["chapter-txt"];
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      Nows.__super__.constructor.call(this, id, scope);
    }

    Nows.prototype.init = function() {
      this.scrollPositionCheckElements.push({
        className: ".animated-number-title-a",
        animationType: G.ONCE
      });
      this.scrollPositionCheckElements.push({
        className: ".animated-number-title-b",
        animationType: G.ONCE
      });
      return Nows.__super__.init.call(this);
    };

    Nows.prototype.ready = function() {
      return Nows.__super__.ready.call(this);
    };

    Nows.prototype.addAnimations = function() {
      var $numberTitleLeft, $numberTitleRight, finishValLeft, finishValRight, mySplitTextLeft, mySplitTextRight, numberTweenLeft, numberTweenRight,
        _this = this;
      Nows.__super__.addAnimations.call(this);
      $numberTitleLeft = this.element.find(".animated-number-title-a");
      mySplitTextLeft = new SplitText(this.element.find(".key-number.left .subtitle"), {
        type: "words"
      });
      numberTweenLeft = {
        val: 0
      };
      finishValLeft = this.scope["key-number-left"].split(".").join("");
      finishValLeft = parseFloat(finishValLeft, 10);
      this.numberTitleLeftTl = new TimelineMax();
      this.numberTitleLeftTl.from($numberTitleLeft, 1, {
        opacity: 0,
        ease: Expo.easeOut
      });
      this.numberTitleLeftTl.from($numberTitleLeft, 0.8, {
        scaleX: 1.1,
        scaleY: 1.1,
        force3D: true,
        ease: Elastic.easeOut
      });
      this.numberTitleLeftTl.staggerFrom(mySplitTextLeft.words, 1, {
        y: "+=20",
        force3D: true,
        opacity: 0,
        ease: Expo.easeOut
      }, 0.1);
      this.numberTitleLeftTl.to(numberTweenLeft, 1, {
        val: finishValLeft,
        ease: Expo.easeOut,
        onUpdate: function() {
          var number;
          number = parseInt(numberTweenLeft.val);
          number = Util.CommaSeparateNumber(number);
          return $numberTitleLeft.text(number);
        }
      }, 0);
      this.numberTitleLeftTl.pause(0, false);
      $numberTitleRight = this.element.find(".animated-number-title-b");
      mySplitTextRight = new SplitText(this.element.find(".key-number.right .subtitle"), {
        type: "words"
      });
      numberTweenRight = {
        val: 0
      };
      finishValRight = this.scope["key-number-right"].split(".").join("");
      finishValRight = parseFloat(finishValRight, 10);
      this.numberTitleRightTl = new TimelineMax();
      this.numberTitleRightTl.from($numberTitleRight, 1, {
        opacity: 0,
        ease: Expo.easeOut
      });
      this.numberTitleRightTl.from($numberTitleRight, 0.8, {
        scaleX: 1.1,
        scaleY: 1.1,
        force3D: true,
        ease: Elastic.easeOut
      });
      this.numberTitleRightTl.staggerFrom(mySplitTextRight.words, 1, {
        y: "+=20",
        force3D: true,
        opacity: 0,
        ease: Expo.easeOut
      }, 0.1);
      this.numberTitleRightTl.to(numberTweenRight, 1, {
        val: finishValRight,
        ease: Expo.easeOut,
        onUpdate: function() {
          var number;
          number = parseInt(numberTweenRight.val);
          number = Util.CommaSeparateNumber(number);
          return $numberTitleRight.text(number);
        }
      }, 0);
      return this.numberTitleRightTl.pause(0, false);
    };

    Nows.prototype.transitionIn = function() {
      return Nows.__super__.transitionIn.call(this);
    };

    Nows.prototype.transitionOut = function() {
      return Nows.__super__.transitionOut.call(this);
    };

    Nows.prototype.transitionInCompleted = function() {
      return Nows.__super__.transitionInCompleted.call(this);
    };

    Nows.prototype.transitionOutCompleted = function() {
      return Nows.__super__.transitionOutCompleted.call(this);
    };

    Nows.prototype.transitionInElement = function(obj) {
      Nows.__super__.transitionInElement.call(this, obj);
      switch (obj.id) {
        case ".animated-number-title-a":
          this.numberTitleLeftTl.play(0);
          break;
        case ".animated-number-title-b":
          this.numberTitleRightTl.play(0);
      }
    };

    Nows.prototype.transitionOutElement = function(obj) {
      Nows.__super__.transitionOutElement.call(this, obj);
      switch (obj.id) {
        case ".animated-number-title-a":
          this.numberTitleLeftTl.pause(0, false);
          break;
        case ".animated-number-title-b":
          this.numberTitleRightTl.pause(0, false);
      }
    };

    Nows.prototype.resize = function() {
      return Nows.__super__.resize.call(this);
    };

    Nows.prototype.destroy = function() {
      if (this.numberTitleLeftTl != null) {
        this.numberTitleLeftTl.kill();
        this.numberTitleLeftTl.clear();
        this.numberTitleRightTl.kill();
        this.numberTitleRightTl.clear();
      }
      return Nows.__super__.destroy.call(this);
    };

    return Nows;

  })(Project);
  return Nows;
});
