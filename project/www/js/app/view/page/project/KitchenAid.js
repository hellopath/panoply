var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project"], function(Project) {
  var KitchenAid;
  KitchenAid = (function(_super) {
    __extends(KitchenAid, _super);

    function KitchenAid(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      scope.machineA = Loader.getImageURL(id + "-machine-a");
      scope.machineB = Loader.getImageURL(id + "-machine-b");
      scope.capsuleA = Loader.getImageURL(id + "-capsule-a");
      scope.capsuleB = Loader.getImageURL(id + "-capsule-b");
      scope.capsuleC = Loader.getImageURL(id + "-capsule-c");
      scope.capsuleD = Loader.getImageURL(id + "-capsule-d");
      scope.capsuleE = Loader.getImageURL(id + "-capsule-e");
      scope.capsuleF = Loader.getImageURL(id + "-capsule-f");
      scope.capsuleG = Loader.getImageURL(id + "-capsule-g");
      scope.capsuleH = Loader.getImageURL(id + "-capsule-h");
      scope.capsuleI = Loader.getImageURL(id + "-capsule-i");
      scope.screenA = Loader.getImageURL(id + "-screen-a");
      scope.screenB = Loader.getImageURL(id + "-screen-b");
      scope.screenC = Loader.getImageURL(id + "-screen-c");
      scope.laptop = Loader.getImageURL(id + "-laptop");
      scope.coffee = Loader.getImageURL(id + "-coffee");
      scope.iPhone = Loader.getImageURL(id + "-i-phone");
      scope.chapterTxt = Model.content["chapter-txt"];
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      KitchenAid.__super__.constructor.call(this, id, scope);
    }

    KitchenAid.prototype.init = function() {
      return KitchenAid.__super__.init.call(this);
    };

    KitchenAid.prototype.ready = function() {
      return KitchenAid.__super__.ready.call(this);
    };

    KitchenAid.prototype.addAnimations = function() {
      return KitchenAid.__super__.addAnimations.call(this);
    };

    KitchenAid.prototype.transitionIn = function() {
      return KitchenAid.__super__.transitionIn.call(this);
    };

    KitchenAid.prototype.transitionOut = function() {
      return KitchenAid.__super__.transitionOut.call(this);
    };

    KitchenAid.prototype.transitionInCompleted = function() {
      return KitchenAid.__super__.transitionInCompleted.call(this);
    };

    KitchenAid.prototype.transitionOutCompleted = function() {
      return KitchenAid.__super__.transitionOutCompleted.call(this);
    };

    KitchenAid.prototype.resize = function() {
      return KitchenAid.__super__.resize.call(this);
    };

    KitchenAid.prototype.destroy = function() {
      return KitchenAid.__super__.destroy.call(this);
    };

    return KitchenAid;

  })(Project);
  return KitchenAid;
});
