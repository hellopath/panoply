var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project", "Slideshow"], function(Project, Slideshow) {
  var ErasableWall;
  ErasableWall = (function(_super) {
    __extends(ErasableWall, _super);

    function ErasableWall(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.transitionOutElement = __bind(this.transitionOutElement, this);
      this.transitionInElement = __bind(this.transitionInElement, this);
      this.onStartBottomAnimation = __bind(this.onStartBottomAnimation, this);
      this.tick = __bind(this.tick, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      var dot, i, radius, _i, _len, _ref;
      scope.worldMap = Loader.getSvg("world-map");
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      scope.clockTime = Loader.getSvg("clock-time");
      scope.laGommeA = Loader.getImageURL(id + "-la-gomme1");
      scope.logIn = Loader.getImageURL(id + "-log-in-game");
      scope.share = Loader.getImageURL(id + "-share-game");
      scope.erase = Loader.getImageURL(id + "-erase-game");
      scope.computerAll = Loader.getImageURL(id + "-computer-all");
      scope.ipadScreen = Loader.getImageURL(id + "-ipad-screen");
      scope.ipadScreen = Loader.getImageURL(id + "-ipad-screen");
      scope.gommeEnd = Loader.getImageURL(id + "-gomme-end");
      scope.dotCircle = Loader.getSvg("dot-circle");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      scope.chapterTxt = Model.content["chapter-txt"];
      radius = 8;
      scope.mapDots = [
        {
          x: 100,
          y: 250,
          r: radius
        }, {
          x: 180,
          y: 300,
          r: radius
        }, {
          x: 240,
          y: 220,
          r: radius
        }, {
          x: 246,
          y: 320,
          r: radius
        }, {
          x: 382,
          y: 280,
          r: radius
        }, {
          x: 620,
          y: 304,
          r: radius
        }, {
          x: 700,
          y: 345,
          r: radius
        }, {
          x: 776,
          y: 200,
          r: radius
        }, {
          x: 780,
          y: 264,
          r: radius
        }, {
          x: 750,
          y: 284,
          r: radius
        }, {
          x: 730,
          y: 422,
          r: radius
        }, {
          x: 828,
          y: 450,
          r: radius
        }
      ];
      _ref = scope.mapDots;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        dot = _ref[i];
        dot.index = i;
      }
      ErasableWall.__super__.constructor.call(this, id, scope);
      return;
    }

    ErasableWall.prototype.init = function() {
      this.scrollPositionCheckElements.push({
        className: ".animated-number-title",
        animationType: G.ONCE
      });
      this.scrollPositionCheckElements.push({
        className: ".clock-holder",
        animationType: G.ONCE
      });
      ErasableWall.__super__.init.call(this);
    };

    ErasableWall.prototype.ready = function() {
      var data;
      ErasableWall.__super__.ready.call(this);
      this.mapDots = this.element.find(".map-dot");
      this.introOne = Util.SetupAnimatedSprite({
        element: this.element.find("#intro-one"),
        image: Loader.getContentById(this.id + "-intro-one"),
        width: 42,
        height: 103,
        count: 20,
        framerate: 10,
        paused: false
      });
      this.introTwo = Util.SetupAnimatedSprite({
        element: this.element.find("#intro-two"),
        image: Loader.getContentById(this.id + "-intro-two"),
        width: 73,
        height: 104,
        count: 23,
        framerate: 14,
        paused: false
      });
      this.introThree = Util.SetupAnimatedSprite({
        element: this.element.find("#intro-three"),
        image: Loader.getContentById(this.id + "-intro-three"),
        width: 73,
        height: 103,
        count: 25,
        framerate: 14,
        paused: false
      });
      this.timeSprite = Util.SetupAnimatedSprite({
        element: this.element.find("#clock-canvas"),
        image: Loader.getContentById(this.id + "-time-running"),
        width: 90,
        height: 86,
        count: 11,
        framerate: 5,
        paused: false
      });
      this.laGomme = Util.SetupAnimatedSprite({
        element: this.element.find("#la-gomme-b"),
        image: Loader.getContentById(this.id + "-la-gomme2"),
        width: 162,
        height: 122,
        count: 49,
        framerate: 20,
        paused: false
      });
      this.whyGirls = Util.SetupAnimatedSprite({
        element: this.element.find("#why-girls"),
        image: Loader.getContentById(this.id + "-why-girls"),
        width: 101,
        height: 84,
        count: 21,
        framerate: 16,
        paused: false
      });
      this.key = Util.SetupAnimatedSprite({
        element: this.element.find("#key"),
        image: Loader.getContentById(this.id + "-key"),
        width: 169,
        height: 112,
        count: 50,
        framerate: 30,
        paused: false
      });
      createjs.Ticker.addEventListener("tick", this.tick);
      data = {
        parentId: this.id,
        images: ["screen-a", "screen-b", "screen-c"]
      };
      this.slideshow = new Slideshow("slideshow", data);
      this.slideshow.parentId = this.id;
      this.element.find(".slideshow-container").append(this.slideshow.element);
      this.slideshow.init();
    };

    ErasableWall.prototype.tick = function(event) {
      this.introOne.stage.update(event);
      this.introTwo.stage.update(event);
      this.introThree.stage.update(event);
      this.laGomme.stage.update(event);
      this.whyGirls.stage.update(event);
      this.key.stage.update(event);
    };

    ErasableWall.prototype.onStartBottomAnimation = function() {
      this.timeTween.pause(this.timeTween.totalTime(), false);
      this.timeSprite.stage.update(event);
    };

    ErasableWall.prototype.transitionInElement = function(obj) {
      ErasableWall.__super__.transitionInElement.call(this, obj);
      switch (obj.id) {
        case ".clock-holder":
          this.clockMapTl.play(0);
          break;
        case ".animated-number-title":
          this.numberTitleTl.play(0);
      }
    };

    ErasableWall.prototype.transitionOutElement = function(obj) {
      ErasableWall.__super__.transitionOutElement.call(this, obj);
    };

    ErasableWall.prototype.addAnimations = function() {
      var $clock, $clockCanvas, $numberTitle, finishVal, mySplitText, numberTween, timeAnim,
        _this = this;
      ErasableWall.__super__.addAnimations.call(this);
      timeAnim = {
        value: 0
      };
      this.clockMapTl = new TimelineMax();
      $clockCanvas = this.element.find("#clock-canvas");
      $clock = this.element.find(".clock");
      this.clockMapTl.from($clock, 1, {
        scaleX: 0.8,
        scaleY: 0.8,
        opacity: 0,
        ease: Elastic.easeOut
      }, 0);
      this.clockMapTl.from($clockCanvas, 1, {
        scaleX: 0.8,
        scaleY: 0.8,
        opacity: 0,
        ease: Expo.easeOut
      }, 0.2);
      this.clockMapTl.from(timeAnim, 1, {
        value: 10,
        roundProps: "value",
        ease: Expo.easeOut,
        onUpdate: function() {
          _this.timeSprite.sprite.gotoAndStop(timeAnim.value);
          return _this.timeSprite.stage.update(event);
        }
      }, 0.2);
      this.clockMapTl.staggerFrom(this.mapDots, 1, {
        scaleX: 0,
        scaleY: 0,
        transformOrigin: "50% 50%",
        ease: Elastic.easeOut
      }, 0.1, 0.6);
      this.clockMapTl.pause(0, true);
      $numberTitle = this.element.find(".animated-number-title");
      mySplitText = new SplitText(this.element.find(".key-number .subtitle"), {
        type: "words"
      });
      numberTween = {
        val: 0
      };
      finishVal = this.scope["key-number"].split(".").join("");
      finishVal = parseFloat(finishVal, 10);
      this.numberTitleTl = new TimelineMax();
      this.numberTitleTl.from($numberTitle, 1, {
        opacity: 0,
        ease: Expo.easeOut
      });
      this.numberTitleTl.from($numberTitle, 0.8, {
        scaleX: 1.1,
        scaleY: 1.1,
        force3D: true,
        ease: Elastic.easeOut
      });
      this.numberTitleTl.staggerFrom(mySplitText.words, 1, {
        y: "+=20",
        force3D: true,
        opacity: 0,
        ease: Expo.easeOut
      }, 0.1);
      this.numberTitleTl.to(numberTween, 1, {
        val: finishVal,
        ease: Expo.easeOut,
        onUpdate: function() {
          var number;
          number = parseInt(numberTween.val);
          number = Util.CommaSeparateNumber(number);
          return $numberTitle.text(number);
        }
      }, 0);
      this.numberTitleTl.pause(0, false);
    };

    ErasableWall.prototype.transitionIn = function() {
      ErasableWall.__super__.transitionIn.call(this);
    };

    ErasableWall.prototype.transitionOut = function() {
      ErasableWall.__super__.transitionOut.call(this);
    };

    ErasableWall.prototype.transitionInCompleted = function() {
      ErasableWall.__super__.transitionInCompleted.call(this);
    };

    ErasableWall.prototype.transitionOutCompleted = function() {
      ErasableWall.__super__.transitionOutCompleted.call(this);
    };

    ErasableWall.prototype.resize = function() {
      ErasableWall.__super__.resize.call(this);
    };

    ErasableWall.prototype.destroy = function() {
      ErasableWall.__super__.destroy.call(this);
      createjs.Ticker.removeEventListener("tick", this.tick);
      this.clockMapTl.kill();
      this.clockMapTl.clear();
      this.numberTitleTl.kill();
      this.numberTitleTl.clear();
      this.introOne.stage.clear();
      this.introTwo.stage.clear();
      this.introThree.stage.clear();
      this.timeSprite.stage.clear();
      this.laGomme.stage.clear();
      this.whyGirls.stage.clear();
      this.key.stage.clear();
    };

    return ErasableWall;

  })(Project);
  return ErasableWall;
});
