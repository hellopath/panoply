var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project"], function(Project) {
  var RobertJaso;
  RobertJaso = (function(_super) {
    __extends(RobertJaso, _super);

    function RobertJaso(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      scope.faceA = Loader.getImageURL(id + "-face-a");
      scope.faceB = Loader.getImageURL(id + "-face-b");
      scope.faceC = Loader.getImageURL(id + "-face-c");
      scope.faceD = Loader.getImageURL(id + "-face-d");
      scope.faceE = Loader.getImageURL(id + "-face-e");
      scope.trophy = Loader.getImageURL(id + "-trophy");
      scope.laptop = Loader.getImageURL(id + "-laptop");
      scope.screenA = Loader.getImageURL(id + "-screen-a");
      scope.chapterTxt = Model.content["chapter-txt"];
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      RobertJaso.__super__.constructor.call(this, id, scope);
    }

    RobertJaso.prototype.init = function() {
      return RobertJaso.__super__.init.call(this);
    };

    RobertJaso.prototype.ready = function() {
      return RobertJaso.__super__.ready.call(this);
    };

    RobertJaso.prototype.addAnimations = function() {
      return RobertJaso.__super__.addAnimations.call(this);
    };

    RobertJaso.prototype.transitionIn = function() {
      return RobertJaso.__super__.transitionIn.call(this);
    };

    RobertJaso.prototype.transitionOut = function() {
      return RobertJaso.__super__.transitionOut.call(this);
    };

    RobertJaso.prototype.transitionInCompleted = function() {
      return RobertJaso.__super__.transitionInCompleted.call(this);
    };

    RobertJaso.prototype.transitionOutCompleted = function() {
      return RobertJaso.__super__.transitionOutCompleted.call(this);
    };

    RobertJaso.prototype.resize = function() {
      return RobertJaso.__super__.resize.call(this);
    };

    RobertJaso.prototype.destroy = function() {
      return RobertJaso.__super__.destroy.call(this);
    };

    return RobertJaso;

  })(Project);
  return RobertJaso;
});
