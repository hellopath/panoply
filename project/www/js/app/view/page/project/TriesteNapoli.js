var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project"], function(Project) {
  var TriesteNapoli;
  TriesteNapoli = (function(_super) {
    __extends(TriesteNapoli, _super);

    function TriesteNapoli(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      scope.coffee = Loader.getImageURL(id + "-coffee");
      scope.map = Loader.getImageURL(id + "-map");
      scope.screenA = Loader.getImageURL(id + "-screen-a");
      scope.screenB = Loader.getImageURL(id + "-screen-b");
      scope.caffe = Loader.getImageURL(id + "-caffe");
      scope.laptop = Loader.getImageURL(id + "-laptop");
      scope.birds = Loader.getImageURL(id + "-birds");
      scope.capsules = Loader.getImageURL(id + "-capsules");
      scope.chapterTxt = Model.content["chapter-txt"];
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      TriesteNapoli.__super__.constructor.call(this, id, scope);
    }

    TriesteNapoli.prototype.init = function() {
      return TriesteNapoli.__super__.init.call(this);
    };

    TriesteNapoli.prototype.ready = function() {
      return TriesteNapoli.__super__.ready.call(this);
    };

    TriesteNapoli.prototype.addAnimations = function() {
      return TriesteNapoli.__super__.addAnimations.call(this);
    };

    TriesteNapoli.prototype.transitionIn = function() {
      return TriesteNapoli.__super__.transitionIn.call(this);
    };

    TriesteNapoli.prototype.transitionOut = function() {
      return TriesteNapoli.__super__.transitionOut.call(this);
    };

    TriesteNapoli.prototype.transitionInCompleted = function() {
      return TriesteNapoli.__super__.transitionInCompleted.call(this);
    };

    TriesteNapoli.prototype.transitionOutCompleted = function() {
      return TriesteNapoli.__super__.transitionOutCompleted.call(this);
    };

    TriesteNapoli.prototype.resize = function() {
      return TriesteNapoli.__super__.resize.call(this);
    };

    TriesteNapoli.prototype.destroy = function() {
      return TriesteNapoli.__super__.destroy.call(this);
    };

    return TriesteNapoli;

  })(Project);
  return TriesteNapoli;
});
