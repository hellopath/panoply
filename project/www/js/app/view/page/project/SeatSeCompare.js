var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project"], function(Project) {
  var SeatSeCompare;
  SeatSeCompare = (function(_super) {
    __extends(SeatSeCompare, _super);

    function SeatSeCompare(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionInElement = __bind(this.transitionInElement, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      scope.allCars = Loader.getImageURL(id + "-all-cars");
      scope.chapterTxt = Model.content["chapter-txt"];
      scope.carWideA = Loader.getImageURL(id + "-car-wide-a");
      scope.carWideB = Loader.getImageURL(id + "-car-wide-b");
      scope.engine = Loader.getImageURL(id + "-engine");
      scope.screenA = Loader.getImageURL(id + "-screen-a");
      scope.screenB = Loader.getImageURL(id + "-screen-b");
      scope.screenC = Loader.getImageURL(id + "-screen-c");
      scope.iphone = Loader.getImageURL(id + "-i-phone");
      scope.ipad = Loader.getImageURL(id + "-i-pad");
      scope.carSmall = Loader.getImageURL(id + "-car-small");
      scope.carSmallRed = Loader.getImageURL(id + "-car-small-red");
      scope.styleTable = Loader.getImageURL(id + "-style-table");
      scope.iconsGroup = Loader.getImageURL(id + "-icons-group");
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      scope.carSteer = Loader.getSvg("car-steer");
      scope.carCadran = Loader.getSvg("car-cadran");
      scope.carBattery = Loader.getSvg("car-battery");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      SeatSeCompare.__super__.constructor.call(this, id, scope);
      return;
    }

    SeatSeCompare.prototype.init = function() {
      this.scrollPositionCheckElements.push({
        className: ".animated-number-title",
        animationType: G.ONCE
      });
      SeatSeCompare.__super__.init.call(this);
    };

    SeatSeCompare.prototype.ready = function() {
      SeatSeCompare.__super__.ready.call(this);
    };

    SeatSeCompare.prototype.addAnimations = function() {
      var $numberTitle, finishVal, mySplitText, numberTween,
        _this = this;
      SeatSeCompare.__super__.addAnimations.call(this);
      $numberTitle = this.element.find(".animated-number-title");
      mySplitText = new SplitText(this.element.find(".key-number .subtitle"), {
        type: "words"
      });
      numberTween = {
        val: 0
      };
      finishVal = this.scope["key-number"].split(".").join("");
      finishVal = parseFloat(finishVal, 10);
      this.numberTitleTl = new TimelineMax();
      this.numberTitleTl.from($numberTitle, 1, {
        opacity: 0,
        ease: Expo.easeOut
      });
      this.numberTitleTl.from($numberTitle, 0.8, {
        scaleX: 1.1,
        scaleY: 1.1,
        force3D: true,
        ease: Elastic.easeOut
      });
      this.numberTitleTl.staggerFrom(mySplitText.words, 1, {
        y: "+=20",
        force3D: true,
        opacity: 0,
        ease: Expo.easeOut
      }, 0.1);
      this.numberTitleTl.to(numberTween, 1, {
        val: finishVal,
        ease: Expo.easeOut,
        onUpdate: function() {
          var number;
          number = parseInt(numberTween.val);
          number = Util.CommaSeparateNumber(number);
          return $numberTitle.text(number);
        }
      }, 0);
      this.numberTitleTl.pause(0, false);
    };

    SeatSeCompare.prototype.transitionIn = function() {
      SeatSeCompare.__super__.transitionIn.call(this);
    };

    SeatSeCompare.prototype.transitionOut = function() {
      SeatSeCompare.__super__.transitionOut.call(this);
    };

    SeatSeCompare.prototype.transitionInCompleted = function() {
      SeatSeCompare.__super__.transitionInCompleted.call(this);
    };

    SeatSeCompare.prototype.transitionOutCompleted = function() {
      SeatSeCompare.__super__.transitionOutCompleted.call(this);
    };

    SeatSeCompare.prototype.transitionInElement = function(obj) {
      SeatSeCompare.__super__.transitionInElement.call(this, obj);
      switch (obj.id) {
        case ".animated-number-title":
          this.numberTitleTl.play(0);
      }
    };

    SeatSeCompare.prototype.resize = function() {
      SeatSeCompare.__super__.resize.call(this);
    };

    SeatSeCompare.prototype.destroy = function() {
      if (this.numberTitleTl != null) {
        this.numberTitleTl.kill();
        this.numberTitleTl.clear();
      }
      SeatSeCompare.__super__.destroy.call(this);
    };

    return SeatSeCompare;

  })(Project);
  return SeatSeCompare;
});
