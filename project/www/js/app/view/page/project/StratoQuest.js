var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project", "SplitText"], function(Project, SplitText) {
  var StratoQuest;
  StratoQuest = (function(_super) {
    __extends(StratoQuest, _super);

    function StratoQuest(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutElement = __bind(this.transitionOutElement, this);
      this.transitionInElement = __bind(this.transitionInElement, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      scope.universeSky = Loader.getImageURL(id + "-universe-sky");
      scope.satellite = Loader.getImageURL(id + "-satellite");
      scope.carA = Loader.getImageURL(id + "-car-a");
      scope.headUniform = Loader.getImageURL(id + "-head-uniform");
      scope.tabletteAir = Loader.getImageURL(id + "-tablette-air");
      scope.iPhone = Loader.getImageURL(id + "-i-phone");
      scope.lumia = Loader.getImageURL(id + "-lumia");
      scope.laptop = Loader.getImageURL(id + "-laptop");
      scope.screenA = Loader.getImageURL(id + "-screen-a");
      scope.saturn = Loader.getImageURL(id + "-saturn");
      scope.plane = Loader.getImageURL(id + "-plane");
      scope.chapterTxt = Model.content["chapter-txt"];
      scope.target = Loader.getSvg("target");
      scope.targetGeometry = Loader.getSvg("target-geometry");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      StratoQuest.__super__.constructor.call(this, id, scope);
    }

    StratoQuest.prototype.init = function() {
      this.numberTitleTl = new TimelineMax();
      this.scrollPositionCheckElements.push({
        className: ".animated-number-title",
        animationType: G.ONCE
      });
      return StratoQuest.__super__.init.call(this);
    };

    StratoQuest.prototype.ready = function() {
      return StratoQuest.__super__.ready.call(this);
    };

    StratoQuest.prototype.addAnimations = function() {
      var $numberTitle, finishVal, mySplitText, numberTween,
        _this = this;
      StratoQuest.__super__.addAnimations.call(this);
      $numberTitle = this.element.find(".animated-number-title");
      mySplitText = new SplitText(this.element.find(".key-number .subtitle"), {
        type: "words"
      });
      numberTween = {
        val: 0
      };
      finishVal = this.scope["key-number"].split(".").join("");
      finishVal = parseFloat(finishVal, 10);
      this.numberTitleTl.from($numberTitle, 1, {
        opacity: 0,
        ease: Expo.easeOut
      });
      this.numberTitleTl.from($numberTitle, 0.8, {
        scaleX: 1.1,
        scaleY: 1.1,
        force3D: true,
        ease: Elastic.easeOut
      });
      this.numberTitleTl.staggerFrom(mySplitText.words, 1, {
        y: "+=20",
        force3D: true,
        opacity: 0,
        ease: Expo.easeOut
      }, 0.1);
      this.numberTitleTl.to(numberTween, 1, {
        val: finishVal,
        ease: Expo.easeOut,
        onUpdate: function() {
          var number;
          number = parseInt(numberTween.val);
          number = Util.CommaSeparateNumber(number);
          return $numberTitle.text(number);
        }
      }, 0);
      return this.numberTitleTl.pause(0, false);
    };

    StratoQuest.prototype.transitionIn = function() {
      return StratoQuest.__super__.transitionIn.call(this);
    };

    StratoQuest.prototype.transitionOut = function() {
      return StratoQuest.__super__.transitionOut.call(this);
    };

    StratoQuest.prototype.transitionInCompleted = function() {
      return StratoQuest.__super__.transitionInCompleted.call(this);
    };

    StratoQuest.prototype.transitionOutCompleted = function() {
      return StratoQuest.__super__.transitionOutCompleted.call(this);
    };

    StratoQuest.prototype.transitionInElement = function(obj) {
      StratoQuest.__super__.transitionInElement.call(this, obj);
      switch (obj.id) {
        case ".animated-number-title":
          this.numberTitleTl.play(0);
      }
    };

    StratoQuest.prototype.transitionOutElement = function(obj) {
      StratoQuest.__super__.transitionOutElement.call(this, obj);
      switch (obj.id) {
        case ".animated-number-title":
          this.numberTitleTl.pause(0, false);
      }
    };

    StratoQuest.prototype.resize = function() {
      return StratoQuest.__super__.resize.call(this);
    };

    StratoQuest.prototype.destroy = function() {
      if (this.numberTitleTl != null) {
        this.numberTitleTl.kill();
        this.numberTitleTl.clear();
      }
      return StratoQuest.__super__.destroy.call(this);
    };

    return StratoQuest;

  })(Project);
  return StratoQuest;
});
