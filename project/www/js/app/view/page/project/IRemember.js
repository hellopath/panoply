var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Project"], function(Project) {
  var IRemember;
  IRemember = (function(_super) {
    __extends(IRemember, _super);

    function IRemember(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutElement = __bind(this.transitionOutElement, this);
      this.transitionInElement = __bind(this.transitionInElement, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      scope.horizontalOrnament = Loader.getSvg("horizontal-ornament");
      scope.universeSky = Loader.getImageURL(id + "-sky-universe");
      scope.ballA = Loader.getImageURL(id + "-ball-a");
      scope.ballB = Loader.getImageURL(id + "-ball-b");
      scope.particlesTerrain = Loader.getImageURL(id + "-particles-terrain");
      scope.wireframeTerrain = Loader.getImageURL(id + "-wireframe-terrain");
      scope.laptop = Loader.getImageURL(id + "-laptop");
      scope.particles = Loader.getImageURL(id + "-particles");
      scope.chapterTxt = Model.content["chapter-txt"];
      scope.polaroidA = Loader.getImageURL(id + "-polaroid-a");
      scope.polaroidB = Loader.getImageURL(id + "-polaroid-b");
      scope.polaroidC = Loader.getImageURL(id + "-polaroid-c");
      scope.polaroidD = Loader.getImageURL(id + "-polaroid-d");
      scope.polaroidDown = Loader.getImageURL(id + "-polaroid-down");
      scope.visitSiteTxt = Model.content["visit-site-txt"];
      IRemember.__super__.constructor.call(this, id, scope);
    }

    IRemember.prototype.init = function() {
      this.numberTitleTl = new TimelineMax();
      this.scrollPositionCheckElements.push({
        className: ".animated-number-title",
        animationType: G.ONCE
      });
      return IRemember.__super__.init.call(this);
    };

    IRemember.prototype.ready = function() {
      return IRemember.__super__.ready.call(this);
    };

    IRemember.prototype.addAnimations = function() {
      var $numberTitle, finishVal, mySplitText, numberTween,
        _this = this;
      IRemember.__super__.addAnimations.call(this);
      $numberTitle = this.element.find(".animated-number-title");
      mySplitText = new SplitText(this.element.find(".key-number .subtitle"), {
        type: "words"
      });
      numberTween = {
        val: 0
      };
      finishVal = this.scope["key-number"].split(".").join("");
      finishVal = parseFloat(finishVal, 10);
      this.numberTitleTl.from($numberTitle, 1, {
        opacity: 0,
        ease: Expo.easeOut
      });
      this.numberTitleTl.from($numberTitle, 0.8, {
        scaleX: 1.1,
        scaleY: 1.1,
        force3D: true,
        ease: Elastic.easeOut
      });
      this.numberTitleTl.staggerFrom(mySplitText.words, 1, {
        y: "+=20",
        force3D: true,
        opacity: 0,
        ease: Expo.easeOut
      }, 0.1);
      this.numberTitleTl.to(numberTween, 1, {
        val: finishVal,
        ease: Expo.easeOut,
        onUpdate: function() {
          var number;
          number = parseInt(numberTween.val);
          number = Util.CommaSeparateNumber(number);
          return $numberTitle.text(number);
        }
      }, 0);
      return this.numberTitleTl.pause(0, false);
    };

    IRemember.prototype.transitionIn = function() {
      return IRemember.__super__.transitionIn.call(this);
    };

    IRemember.prototype.transitionOut = function() {
      return IRemember.__super__.transitionOut.call(this);
    };

    IRemember.prototype.transitionInCompleted = function() {
      return IRemember.__super__.transitionInCompleted.call(this);
    };

    IRemember.prototype.transitionOutCompleted = function() {
      return IRemember.__super__.transitionOutCompleted.call(this);
    };

    IRemember.prototype.transitionInElement = function(obj) {
      IRemember.__super__.transitionInElement.call(this, obj);
      switch (obj.id) {
        case ".animated-number-title":
          this.numberTitleTl.play(0);
      }
    };

    IRemember.prototype.transitionOutElement = function(obj) {
      IRemember.__super__.transitionOutElement.call(this, obj);
      switch (obj.id) {
        case ".animated-number-title":
          this.numberTitleTl.pause(0, false);
      }
    };

    IRemember.prototype.resize = function() {
      IRemember.__super__.resize.call(this);
    };

    IRemember.prototype.destroy = function() {
      if (this.numberTitleTl != null) {
        this.numberTitleTl.kill();
        this.numberTitleTl.clear();
      }
      IRemember.__super__.destroy.call(this);
    };

    return IRemember;

  })(Project);
  return IRemember;
});
