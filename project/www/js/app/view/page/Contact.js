var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Page"], function(Page) {
  var Contact;
  Contact = (function(_super) {
    __extends(Contact, _super);

    function Contact(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      Contact.__super__.constructor.call(this, id, scope);
    }

    Contact.prototype.init = function() {
      return Contact.__super__.init.call(this);
    };

    Contact.prototype.ready = function() {
      return Contact.__super__.ready.call(this);
    };

    Contact.prototype.addAnimations = function() {
      return Contact.__super__.addAnimations.call(this);
    };

    Contact.prototype.transitionIn = function() {
      return Contact.__super__.transitionIn.call(this);
    };

    Contact.prototype.transitionOut = function() {
      return Contact.__super__.transitionOut.call(this);
    };

    Contact.prototype.transitionInCompleted = function() {
      return Contact.__super__.transitionInCompleted.call(this);
    };

    Contact.prototype.transitionOutCompleted = function() {
      return Contact.__super__.transitionOutCompleted.call(this);
    };

    Contact.prototype.resize = function() {
      return Contact.__super__.resize.call(this);
    };

    Contact.prototype.destroy = function() {
      return Contact.__super__.destroy.call(this);
    };

    return Contact;

  })(Page);
  return Contact;
});
