var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Page"], function(Page) {
  var Home;
  Home = (function(_super) {
    __extends(Home, _super);

    Home.prototype.currentTargetType = void 0;

    Home.prototype.disableScrolling = false;

    function Home(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.onChangeScrollingState = __bind(this.onChangeScrollingState, this);
      this.resize = __bind(this.resize, this);
      this.onUpdate = __bind(this.onUpdate, this);
      this.onScroll = __bind(this.onScroll, this);
      this.checkLazyImagesPosition = __bind(this.checkLazyImagesPosition, this);
      this.updateBackground = __bind(this.updateBackground, this);
      this.updateScrollingElements = __bind(this.updateScrollingElements, this);
      this.setupTitleLine = __bind(this.setupTitleLine, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.onTitlesMouseOut = __bind(this.onTitlesMouseOut, this);
      this.onTitlesMouseOver = __bind(this.onTitlesMouseOver, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      var i, p, route, routes, _i, _len, _ref;
      this.projects = [];
      this.scrollPositionCheckElements = [];
      routes = [];
      _ref = Model.routing;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        route = _ref[i];
        if (route.parentRoute === "project") {
          p = {};
          p.id = route.id;
          this.projects.push(p);
          routes.push(route);
        }
      }
      scope.projects = routes;
      scope.topW = Loader.getImageURL(id + "-topW");
      scope.lineArrowPartial = Loader.getPartial("head-line-arrow");
      scope.headArrow = Loader.getSvg("head-arrow");
      scope.headArrowWide = Loader.getSvg("head-arrow-wide");
      scope.blankImg = Loader.getImageURL("blank-image");
      Home.__super__.constructor.call(this, id, scope);
      return;
    }

    Home.prototype.init = function() {
      Home.__super__.init.call(this);
      Signal.changeScrollingState.add(this.onChangeScrollingState);
    };

    Home.prototype.ready = function() {
      var $bwImg, $coloredImg, $line, $lines, $projectImageContainer, $screen, $scrollable, $title, allScrollables, i, line, lineW, project, range, scr, scrollable, splitter, title, tl, _i, _j, _k, _l, _len, _len1, _len2, _ref, _ref1;
      this.titlesContainer = this.element.find(".project-titles-container");
      this.titles = this.element.find(".project-titles-container .project-part");
      this.screens = this.element.find(".project-screens-container .project-part");
      this.holderEl = this.element.find(".containers-holder");
      this.topEl = this.holderEl.find(".top-container");
      this.bottomEl = this.holderEl.find(".bottom-container");
      this.coloredBackgroundEl = this.topEl.find(".colored-background");
      this.projectImageContainers = this.bottomEl.find(".project-image-container");
      this.projectImages = this.bottomEl.find(".project-image");
      this.projectImagesInside = this.bottomEl.find(".project-image img");
      this.titlesContainerScrollRange = parseInt(this.titlesContainer.attr("scroll-range"), 10);
      this.projectsLen = this.projectImageContainers.length;
      this.lazyImages = this.element.find("img.lazy");
      this.titles.on("mouseover", this.onTitlesMouseOver);
      this.titles.on("mouseout", this.onTitlesMouseOut);
      this.scrollables = [];
      allScrollables = this.element.find(".scrollable");
      for (i = _i = 0, _len = allScrollables.length; _i < _len; i = ++_i) {
        scrollable = allScrollables[i];
        $scrollable = $(scrollable);
        scr = {};
        scr.el = $scrollable;
        range = parseInt(scrollable.getAttribute("scroll-range"));
        scr.range = range ? range : 0;
        scr.ease = parseFloat(scrollable.getAttribute("scroll-ease"));
        scr.topBound = $scrollable.offset().top + Model.windowH;
        scr.bottomBound = scr.topBound + $scrollable.height();
        scr.y = 0;
        this.scrollables[i] = scr;
      }
      _ref = this.titles;
      for (i = _j = 0, _len1 = _ref.length; _j < _len1; i = ++_j) {
        title = _ref[i];
        $title = $(title);
        $screen = $(this.screens[i]);
        $coloredImg = $screen.find("img.colored");
        $bwImg = $screen.find("img.bw");
        splitter = new SplitText($title.find(".title"), {
          type: "words, lines"
        });
        this.setupTitleLine(splitter);
        this.projects[i].splitter = splitter;
        this.projects[i].screen = $screen;
        this.projects[i].coloredImg = $coloredImg;
        this.projects[i].bwImg = $bwImg;
      }
      for (i = _k = 0, _ref1 = this.projectsLen - 1; 0 <= _ref1 ? _k <= _ref1 : _k >= _ref1; i = 0 <= _ref1 ? ++_k : --_k) {
        $projectImageContainer = $(this.projectImageContainers[i]);
        project = this.projects[i];
        project.tl = new TimelineMax();
        tl = project.tl;
        $lines = project.splitter.lines;
        lineW = 0;
        for (i = _l = 0, _len2 = $lines.length; _l < _len2; i = ++_l) {
          line = $lines[i];
          $line = $(line).find(".title-through-line");
          lineW += $line.width();
          tl.from($line, 1, {
            scaleX: 0,
            transformOrigin: "0% 0%",
            ease: Expo.easeInOut
          }, 0.5 * i);
        }
        tl.pause(0);
      }
      return this.resize();
    };

    Home.prototype.addAnimations = function() {
      return Home.__super__.addAnimations.call(this);
    };

    Home.prototype.onTitlesMouseOver = function(e) {
      var $target, i, id, project, projectId, _i, _ref;
      $target = $(e.currentTarget);
      id = $target.attr("id");
      e.stopPropagation();
      for (i = _i = 0, _ref = this.projectsLen - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        projectId = this.scope.projects[i].id;
        if (projectId === id) {
          project = this.projects[i];
          project.coloredImg.addClass("transition-in");
          TweenMax.set(project.coloredImg, {
            opacity: 1
          });
          break;
        }
      }
      return false;
    };

    Home.prototype.onTitlesMouseOut = function(e) {
      var $target, i, id, project, projectId, _i, _ref;
      $target = $(e.currentTarget);
      id = $target.attr("id");
      e.stopPropagation();
      for (i = _i = 0, _ref = this.projectsLen - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        projectId = this.scope.projects[i].id;
        if (projectId === id) {
          project = this.projects[i];
          project.coloredImg.removeClass("transition-in");
          TweenMax.set(project.coloredImg, {
            opacity: 0
          });
          break;
        }
      }
      return false;
    };

    Home.prototype.transitionIn = function() {
      $(window).on("scroll", this.onScroll);
      $("body").on("touchmove", this.onScroll);
      $("footer#footer").css("display", "block");
      Model.parentEl.css("position", "static");
      this.onScroll();
      this.onUpdate();
      Home.__super__.transitionIn.call(this);
    };

    Home.prototype.transitionOut = function() {
      Home.__super__.transitionOut.call(this);
    };

    Home.prototype.transitionInCompleted = function() {
      Home.__super__.transitionInCompleted.call(this);
    };

    Home.prototype.transitionOutCompleted = function() {
      Home.__super__.transitionOutCompleted.call(this);
    };

    Home.prototype.setupTitleLine = function(splitter) {
      var $line, $titleLine, children, line, lineCss, lineW, padding, xStart, _i, _len, _ref, _results;
      padding = 10;
      _ref = splitter.lines;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        line = _ref[_i];
        $line = $(line);
        $titleLine = $(Loader.getPartial("title-through-line"));
        children = $line.children();
        xStart = children.first().position().left - padding;
        lineW = children.last().position().left + children.last().width() - xStart + padding;
        lineCss = {
          left: xStart,
          width: lineW
        };
        $line.append($titleLine);
        _results.push($titleLine.css(lineCss));
      }
      return _results;
    };

    Home.prototype.updateScrollingElements = function() {
      var i, range, relativeY, scr, _i, _len, _ref;
      relativeY = this.lastScrollY / this.pageHeight;
      _ref = this.scrollables;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        scr = _ref[i];
        range = scr.range * relativeY;
        scr.y += ((this.lastScrollY + range) - scr.y) * scr.ease;
        scr.y = Math.round(scr.y);
        TweenMax.set(scr.el, {
          y: -scr.y,
          force3D: true
        });
      }
    };

    Home.prototype.updateBackground = function() {
      var alphaVal;
      alphaVal = 1 - Math.min((this.lastScrollY / 800) * 1, 1);
      alphaVal = alphaVal.toFixed(2);
      TweenMax.set(this.coloredBackgroundEl, {
        opacity: alphaVal
      });
    };

    Home.prototype.checkLazyImagesPosition = function() {
      var $lazy, i, imgUrl, lazy, _i, _len, _ref;
      _ref = this.lazyImages;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        lazy = _ref[i];
        $lazy = $(lazy);
        if (this.lastScrollY + Model.windowH > $lazy.offset().top) {
          imgUrl = $lazy.attr("data-src");
          $lazy.attr("src", imgUrl);
          this.lazyImages.splice(i, 1);
          break;
        }
      }
    };

    Home.prototype.onScroll = function() {
      Home.__super__.onScroll.call(this);
    };

    Home.prototype.onUpdate = function() {
      if (!this.disableScrolling) {
        this.updateScrollingElements();
        this.updateBackground();
        if (this.lazyImages.length > 0) {
          this.checkLazyImagesPosition();
        }
      }
      Home.__super__.onUpdate.call(this);
    };

    Home.prototype.resize = function() {
      var $projectImage, $projectImageContainer, $projectImageInside, $titleInside, $titlePart, $titlePartTop, $visualPart, bottomHeight, halfMaxGridWidth, halfWindowH, halfWindowW, heightBetweenImgContainer, heightOffsetMid, heightScaleBetweenImgContainer, i, imageRatio, insideRectRatio, marginBetweenProject, maxGridHeight, maxGridWidth, relativeY, resizeParams, scrollRangeValue, titlePartCss, titlePartTopCss, topHeight, videoH, videoW, visualH, visualPartCss, visualYPosition, visualsHeight, _i, _ref;
      resizeParams = Util.GetAdjustmentContainerSize(Model.windowW, Model.windowH, Model.mediaW, Model.mediaH);
      videoW = resizeParams.type === "landscape" ? resizeParams.holderW : resizeParams.originalW * resizeParams.scaleY;
      videoH = resizeParams.type === "landscape" ? resizeParams.originalH * resizeParams.scaleX : resizeParams.holderH;
      heightOffsetMid = Model.gridSizeType === G.BIG_GRID_SIZE ? Math.floor(Model.mediaH / 2) : Math.floor(Model.mediaH / 3);
      visualH = Model.windowH;
      maxGridWidth = Model.maxWindowW;
      maxGridHeight = heightOffsetMid << 1;
      halfMaxGridWidth = Model.gridSizeType === G.BIG_GRID_SIZE ? Model.gridWidth * 3 : Model.gridWidth * 2;
      halfWindowW = Model.windowW >> 1;
      halfWindowH = Model.windowH >> 1;
      heightBetweenImgContainer = Model.windowH - maxGridHeight;
      heightScaleBetweenImgContainer = -(heightBetweenImgContainer / 400) * 150;
      marginBetweenProject = Math.max(Math.round(heightBetweenImgContainer + heightScaleBetweenImgContainer), 0);
      this.topEl.width(Model.windowW);
      this.topEl.css("height", Model.windowH);
      for (i = _i = 0, _ref = this.projectsLen - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        $projectImage = $(this.projectImages[i]);
        $projectImageInside = $(this.projectImagesInside[i]);
        $projectImageContainer = $(this.projectImageContainers[i]);
        $titlePart = $(this.titles[i]);
        $visualPart = $(this.screens[i]);
        $titlePartTop = $titlePart.find(".project-top");
        $titleInside = $titlePart.find(".title");
        visualPartCss = {
          width: Model.windowW,
          height: Model.windowH,
          top: (i * visualH) - (i * marginBetweenProject)
        };
        visualYPosition = Model.windowH + visualPartCss.top;
        relativeY = visualYPosition / this.pageHeight;
        scrollRangeValue = this.titlesContainerScrollRange;
        titlePartCss = {
          width: maxGridWidth,
          height: maxGridHeight,
          top: visualPartCss.top + halfWindowH - heightOffsetMid + (scrollRangeValue * relativeY),
          left: halfWindowW - halfMaxGridWidth
        };
        titlePartTopCss = {
          top: heightOffsetMid - ($titleInside.height() >> 1)
        };
        $projectImageInside.css({
          width: videoW,
          height: videoH
        });
        $projectImageContainer.css({
          width: Model.windowW,
          height: Model.windowH
        });
        this.projectImageCss = {
          width: videoW,
          height: videoH,
          left: halfWindowW - (videoW >> 1),
          top: halfWindowH - (videoH >> 1)
        };
        this.normalStateRectStr = Util.GetRect(halfWindowH - heightOffsetMid, halfWindowW + halfMaxGridWidth, halfWindowH + heightOffsetMid, halfWindowW - halfMaxGridWidth);
        insideRectRatio = maxGridWidth / maxGridHeight;
        imageRatio = Model.mediaW / Model.mediaH;
        if (insideRectRatio < imageRatio) {
          this.imageInsideScale = (maxGridHeight / Model.windowH) * 1;
        } else {
          this.imageInsideScale = (maxGridWidth / Model.windowW) * 1;
        }
        $titlePart.css(titlePartCss);
        $titlePartTop.css(titlePartTopCss);
        $projectImage.css(this.projectImageCss);
        $visualPart.css(visualPartCss);
        $visualPart.attr("data-posy", visualPartCss.top);
        TweenMax.set($projectImageContainer, {
          clip: this.normalStateRectStr
        });
        TweenMax.set($projectImage, {
          scale: this.imageInsideScale,
          transformOrigin: "50% 50%"
        });
      }
      visualsHeight = visualPartCss.top;
      topHeight = Model.windowH;
      bottomHeight = visualsHeight + visualH;
      this.pageHeight = bottomHeight + topHeight;
      Home.__super__.resize.call(this);
    };

    Home.prototype.onChangeScrollingState = function(state) {
      switch (state) {
        case G.ENABLE:
          return this.disableScrolling = false;
        case G.DISABLE:
          return this.disableScrolling = true;
      }
    };

    Home.prototype.destroy = function() {
      $(window).off("scroll", this.onScroll);
      $("body").off("touchmove", this.onScroll);
      this.titles.off("mouseover", this.onTitlesMouseOver);
      this.titles.off("mouseout", this.onTitlesMouseOut);
      this.scrollPositionCheckElements.length = 0;
      this.titles.length = 0;
      this.projects.length = 0;
      this.disableScrolling = false;
      this.scrollPositionCheckElements = null;
      this.titles = null;
      this.projects = null;
      Home.__super__.destroy.call(this);
    };

    return Home;

  })(Page);
  return Home;
});
