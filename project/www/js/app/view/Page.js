var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["View", "signals", "Footer"], function(View, signals, Footer) {
  "use strict";
  var Page;
  Page = (function(_super) {
    __extends(Page, _super);

    Page.prototype.transitionInComplete = void 0;

    Page.prototype.transitionOutComplete = void 0;

    Page.prototype.tl = void 0;

    Page.prototype.allReadyCallback = void 0;

    Page.prototype.pageHeight = void 0;

    Page.prototype.requestAnimationId = void 0;

    Page.prototype.lastScrollY = 0;

    Page.prototype.scrollTarget = void 0;

    function Page(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.onUpdate = __bind(this.onUpdate, this);
      this.updateFooterPosition = __bind(this.updateFooterPosition, this);
      this.onScroll = __bind(this.onScroll, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionInCompleted = __bind(this.transitionInCompleted, this);
      this.transitionOut = __bind(this.transitionOut, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      this.preInit = __bind(this.preInit, this);
      Page.__super__.constructor.call(this, id, scope);
    }

    Page.prototype.preInit = function() {};

    Page.prototype.init = function() {
      this.tl = new TimelineMax({
        onComplete: this.transitionInCompleted,
        onReverseComplete: this.transitionOutCompleted
      });
      this.transitionInComplete = new signals.Signal();
      this.transitionOutComplete = new signals.Signal();
      Signal.onResize.add(this.resize);
      this.footer = new Footer("footer");
      this.element.append(this.footer.element);
      this.footer.init();
      this.footer.y = 0;
      this.footer.ease = 0.16;
      this.footer.height = this.footer.element.height();
      if (this.allReadyCallback != null) {
        TweenMax.delayedCall(0.2, this.allReadyCallback);
      }
    };

    Page.prototype.ready = function() {};

    Page.prototype.addAnimations = function() {
      this.tl.pause(0);
    };

    Page.prototype.transitionIn = function() {
      $(window).on("scroll", this.onScroll);
      $("body").on("touchmove", this.onScroll);
      this.tl.gotoAndPlay(0);
    };

    Page.prototype.transitionOut = function() {
      this.tl.reverse();
    };

    Page.prototype.transitionInCompleted = function() {
      this.transitionInComplete.dispatch();
    };

    Page.prototype.transitionOutCompleted = function() {
      Model.parentEl.css("position", "fixed");
      this.footer.element.css("display", "none");
      this.transitionOutComplete.dispatch();
    };

    Page.prototype.onScroll = function() {
      this.scrollTarget = $(window).scrollTop();
      this.scrollTarget = this.scrollTarget < 0 ? 0 : this.scrollTarget;
      this.scrollTarget = this.scrollTarget + Model.windowH > this.pageHeight ? this.pageHeight - Model.windowH : this.scrollTarget;
    };

    Page.prototype.updateFooterPosition = function() {
      if (this.footer != null) {
        this.footer.y += (this.lastScrollY - this.footer.y) * this.footer.ease;
        this.footer.y = Math.round(this.footer.y);
        TweenMax.set(this.footer.element, {
          y: -this.footer.y,
          force3D: true
        });
      }
    };

    Page.prototype.onUpdate = function() {
      this.updateFooterPosition();
      this.lastScrollY = this.scrollTarget;
      this.requestAnimationId = requestAnimationFrame(this.onUpdate);
    };

    Page.prototype.resize = function() {
      this.pageHeight += this.footer.height;
      Model.parentEl.css("height", this.pageHeight);
      this.footer.element.css("top", this.pageHeight - this.footer.height);
    };

    Page.prototype.destroy = function() {
      Page.__super__.destroy.call(this);
      cancelAnimationFrame(this.requestAnimationId);
      $(window).off("scroll", this.onScroll);
      $("body").off("touchmove", this.onScroll);
      Signal.onResize.remove(this.resize);
      TweenMax.killDelayedCallsTo(this.allReadyCallback);
      if (this.transitionInComplete != null) {
        this.transitionInComplete.removeAll();
      }
      if (this.transitionOutComplete != null) {
        this.transitionOutComplete.removeAll();
      }
      this.transitionInComplete = null;
      this.transitionOutComplete = null;
      if (this.tl != null) {
        this.tl.clear();
      }
      if (this.footer != null) {
        this.footer.destroy();
      }
      this.tl = null;
      this.footer = null;
    };

    return Page;

  })(View);
  return Page;
});
