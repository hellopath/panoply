var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

define(["Page", "ProjectTopContainer", "ProjectTextTransformer", "Magnifier", "ProjectsBottomNavMenu", "ProjectsTopNavMenu", "Mustache"], function(Page, ProjectTopContainer, ProjectTextTransformer, Magnifier, ProjectsBottomNavMenu, ProjectsTopNavMenu, Mustache) {
  "use strict";
  var Project;
  Project = (function(_super) {
    __extends(Project, _super);

    Project.prototype.beginAnimationFinished = void 0;

    function Project(id, scope) {
      this.destroy = __bind(this.destroy, this);
      this.resize = __bind(this.resize, this);
      this.transitionOutCompleted = __bind(this.transitionOutCompleted, this);
      this.transitionIn = __bind(this.transitionIn, this);
      this.onUpdate = __bind(this.onUpdate, this);
      this.onScrollEnded = __bind(this.onScrollEnded, this);
      this.onMouseMove = __bind(this.onMouseMove, this);
      this.onScroll = __bind(this.onScroll, this);
      this.getMagnifierByID = __bind(this.getMagnifierByID, this);
      this.setupScrollPositionChecker = __bind(this.setupScrollPositionChecker, this);
      this.checkHightlightsElements = __bind(this.checkHightlightsElements, this);
      this.updateBottomNavPosition = __bind(this.updateBottomNavPosition, this);
      this.updateScrollingElements = __bind(this.updateScrollingElements, this);
      this.transitionOutElement = __bind(this.transitionOutElement, this);
      this.transitionInElement = __bind(this.transitionInElement, this);
      this.bottomAnimationStart = __bind(this.bottomAnimationStart, this);
      this.addAnimations = __bind(this.addAnimations, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
      this.preInit = __bind(this.preInit, this);
      this.scrollPositionCheckElements = [];
      this.beginAnimationFinished = false;
      this.mouse = {
        x: 0,
        y: 0
      };
      this.id = id;
      this.scope = scope;
      this.scope.blankImg = Loader.getImageURL("blank-image");
      this.partialId = this.id + "-partial";
      this.rawPartial = Loader.getContentById(this.partialId);
      return;
    }

    Project.prototype.preInit = function() {
      var $bottomContainer, $containersHolder, $containersHolderChildren, $element, render;
      while (this.rawPartial.indexOf("{{") >= 0) {
        render = Mustache.render(this.rawPartial, this.scope);
        this.rawPartial = render;
      }
      $element = $(render);
      $containersHolder = $element.find(".containers-holder");
      $containersHolderChildren = $containersHolder.children().not(".down-containers-scrollable-layer").remove();
      $bottomContainer = $containersHolder.find(".bottom-container").remove();
      this.partials = {};
      this.partials.containersHolderChildren = $containersHolderChildren;
      this.partials.bottomContainer = $bottomContainer;
      this.element = $element;
      this.holderEl = this.element.find(".containers-holder");
      this.topEl = this.holderEl.find(".top-container");
      this.projectTop = new ProjectTopContainer("project-top-container", this.scope.intro);
      this.projectTop.id = this.id;
      this.topEl.append(this.projectTop.element);
      this.projectTop.onVideoCallbackReady = this.onProjectVideoReady;
      this.projectTop.init();
      Project.__super__.preInit.call(this);
    };

    Project.prototype.init = function() {
      var $allLazy, $lazy, $magnifier, $magnifiers, i, lazy, m, magnifier, src, uniqueClass, _i, _j, _len, _len1;
      this.partials.containersHolderChildren.css("display", "none");
      this.partials.bottomContainer.css("opacity", 0);
      this.element.prepend(this.partials.containersHolderChildren);
      this.element.find(".down-containers-scrollable-layer").append(this.partials.bottomContainer);
      $allLazy = this.element.find(".lazy");
      for (_i = 0, _len = $allLazy.length; _i < _len; _i++) {
        lazy = $allLazy[_i];
        $lazy = $(lazy);
        src = $lazy.attr("data-src");
        if ($lazy.is("div")) {
          $lazy.css("background-image", "url(" + src + ")");
        } else {
          $lazy.attr("src", src);
        }
      }
      this.scrollAnim = {
        val: 0
      };
      this.holderEl = this.element.find(".containers-holder");
      this.topEl = this.holderEl.find(".top-container");
      this.bottomEl = this.holderEl.find(".bottom-container");
      this.scrollableLayersHolder = this.element.find(".scrollable-layers-holder");
      TweenMax.set(this.bottomEl, {
        autoAlpha: 0
      });
      $magnifiers = this.element.find("[magnifier]");
      this.magnifiers = [];
      for (i = _j = 0, _len1 = $magnifiers.length; _j < _len1; i = ++_j) {
        magnifier = $magnifiers[i];
        $magnifier = $(magnifier);
        m = new Magnifier("magnifier", this.scope, i);
        $magnifier.append(m.element);
        m.element = $magnifier;
        uniqueClass = "magnifier-" + i;
        m.clazzId = "." + uniqueClass;
        m.element.addClass(uniqueClass);
        m.init();
        this.magnifiers[i] = m;
        this.scrollPositionCheckElements.push({
          className: m.clazzId,
          animationType: G.ALWAYS
        });
      }
      this.projectsBottomNavMenu = new ProjectsBottomNavMenu("projects-bottom-nav-menu");
      this.element.append(this.projectsBottomNavMenu.element);
      this.projectsBottomNavMenu.init();
      this.projectsBottomNavMenu.y = 0;
      this.projectsBottomNavMenu.height = this.projectsBottomNavMenu.element.height();
      this.projectsBottomNavMenu.ease = 0.1;
      this.projectsBottomNavMenu.element.css("opacity", 0);
      this.projectsTopNavMenu = new ProjectsTopNavMenu("projects-top-nav-menu");
      this.projectTop.element.append(this.projectsTopNavMenu.element);
      this.projectsTopNavMenu.init();
      this.projectsTopNavMenu.element.css("opacity", 0);
      this.projectTop.onLastPartBegins = this.bottomAnimationStart;
      this.projectTop.setupWheelView();
      Project.__super__.init.call(this);
    };

    Project.prototype.ready = function() {
      var $chapter, $chapters, $scrollable, allScrollables, chapter, className, i, range, scr, scrollable, _i, _j, _len, _len1;
      this.resize();
      $chapters = this.element.find(".numbered-chapitre-holder");
      for (_i = 0, _len = $chapters.length; _i < _len; _i++) {
        chapter = $chapters[_i];
        $chapter = $(chapter);
        className = Util.GetChapterUniqueClass($chapter);
        this.scrollPositionCheckElements.push({
          className: className,
          animationType: G.ALWAYS
        });
      }
      this.projectTextTransformer = new ProjectTextTransformer();
      this.projectTextTransformer.init($chapters);
      this.scrollables = [];
      allScrollables = this.element.find(".scrollable");
      for (i = _j = 0, _len1 = allScrollables.length; _j < _len1; i = ++_j) {
        scrollable = allScrollables[i];
        $scrollable = $(scrollable);
        scr = {};
        scr.el = $scrollable;
        range = parseInt(scrollable.getAttribute("scroll-range"));
        scr.range = range ? range : 0;
        scr.ease = parseFloat(scrollable.getAttribute("scroll-ease"));
        scr.y = 0;
        scr.rotation = 0;
        this.scrollables[i] = scr;
      }
      this.setupScrollPositionChecker();
    };

    Project.prototype.addAnimations = function() {
      this.partials.containersHolderChildren.css("display", "block");
      this.partials.bottomContainer.css("opacity", 1);
      this.tl.add(this.projectTop.startTl, 0.5);
      return Project.__super__.addAnimations.call(this);
    };

    Project.prototype.bottomAnimationStart = function() {
      var _this = this;
      TweenMax.set(this.bottomEl, {
        autoAlpha: 1
      });
      Model.parentEl.css({
        "position": "static"
      });
      this.footer.element.css({
        display: "block"
      });
      TweenMax.to(this.scrollAnim, 1.6, {
        val: Model.windowH,
        ease: Expo.easeInOut,
        onStart: this.onUpdate,
        onComplete: function() {
          return _this.beginAnimationFinished = true;
        },
        onUpdate: function() {
          $(window).scrollTop(_this.scrollAnim.val);
          return _this.onScroll();
        }
      });
    };

    Project.prototype.transitionInElement = function(obj) {
      var e, _i, _len, _ref;
      _ref = obj.el;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        e = _ref[_i];
        e.classList.add("transition-in");
      }
      switch (obj.type) {
        case G.MAGNIFIER:
          obj.clazz.open();
          break;
        case G.CHAPITRE:
          this.projectTextTransformer.transitionIn(obj.id);
      }
    };

    Project.prototype.transitionOutElement = function(obj) {
      var e, _i, _len, _ref;
      _ref = obj.el;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        e = _ref[_i];
        e.classList.remove("transition-in");
      }
      switch (obj.type) {
        case G.MAGNIFIER:
          obj.clazz.close();
          break;
        case G.CHAPITRE:
          this.projectTextTransformer.transitionOut(obj.id);
      }
    };

    Project.prototype.updateScrollingElements = function() {
      var i, range, relativeY, scr, scrlbLen, _i, _len, _ref;
      scrlbLen = this.scrollables.length;
      if (scrlbLen < 1) {
        return;
      }
      relativeY = this.lastScrollY / this.pageHeight;
      _ref = this.scrollables;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        scr = _ref[i];
        range = scr.range * relativeY;
        scr.y += ((this.lastScrollY + range) - scr.y) * scr.ease;
        scr.y = Math.round(scr.y);
        TweenMax.set(scr.el, {
          y: -scr.y,
          transformOrigin: "50% 0% 0",
          force3D: true
        });
      }
    };

    Project.prototype.updateBottomNavPosition = function() {
      this.projectsBottomNavMenu.y += (this.lastScrollY - this.projectsBottomNavMenu.y) * this.projectsBottomNavMenu.ease;
      this.projectsBottomNavMenu.y = Math.round(this.projectsBottomNavMenu.y);
      TweenMax.set(this.projectsBottomNavMenu.element, {
        y: -this.projectsBottomNavMenu.y,
        force3D: true
      });
    };

    Project.prototype.checkHightlightsElements = function() {
      var bottomBound, element, height, i, offsetTop, topBound, windowH, _i, _len, _ref;
      _ref = this.toCheckElements;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        element = _ref[i];
        offsetTop = element.offsetTop;
        height = element.height;
        windowH = Model.windowH;
        topBound = offsetTop - windowH;
        bottomBound = offsetTop + height;
        if (this.scrollTarget > topBound && this.scrollTarget < bottomBound) {
          if (!element.inCallbackSended) {
            this.transitionInElement(element);
            element.inCallbackSended = true;
            element.outCallbackSended = false;
            element.transitionedInOnce = true;
          }
        } else {
          if (element.animationType === G.ONCE && element.transitionedInOnce) {
            this.toCheckElements.splice(i, 1);
            return;
          } else {
            if (!element.outCallbackSended) {
              this.transitionOutElement(element);
              element.inCallbackSended = false;
              element.outCallbackSended = true;
            }
          }
        }
      }
    };

    Project.prototype.setupScrollPositionChecker = function() {
      var $el, className, clazz, height, i, scrollObj, type, _i, _len, _ref;
      this.toCheckElements = [];
      _ref = this.scrollPositionCheckElements;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        scrollObj = _ref[i];
        className = scrollObj.className;
        $el = this.element.find(className);
        type = className.indexOf("magnifier") > 0 ? G.MAGNIFIER : className.indexOf("chapitre") > 0 ? G.CHAPITRE : "other";
        clazz = type === G.MAGNIFIER ? this.getMagnifierByID(className) : void 0;
        height = type === G.CHAPITRE ? $el.find(".bottom-p").height() : $el.height();
        this.toCheckElements[i] = {
          el: $el,
          id: className,
          clazz: clazz,
          type: type,
          transitionedInOnce: false,
          animationType: scrollObj.animationType,
          height: height,
          offsetTop: $el.offset().top,
          inCallbackSended: false,
          outCallbackSended: false
        };
      }
    };

    Project.prototype.getMagnifierByID = function(id) {
      var m, magnifier, _i, _len, _ref;
      _ref = this.magnifiers;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        m = _ref[_i];
        if (m.clazzId === id) {
          magnifier = m;
        }
      }
      return magnifier;
    };

    Project.prototype.onScroll = function() {
      Project.__super__.onScroll.call(this);
      clearTimeout(this.scrollTimeout);
      this.scrollTimeout = setTimeout(this.onScrollEnded, 300);
    };

    Project.prototype.onMouseMove = function(e) {
      e.preventDefault();
      this.mouse.x = e.pageX;
      this.mouse.y = e.pageY;
    };

    Project.prototype.onScrollEnded = function() {
      if (this.beginAnimationFinished) {
        return this.checkHightlightsElements();
      }
    };

    Project.prototype.onUpdate = function() {
      this.updateBottomNavPosition();
      this.updateScrollingElements();
      Project.__super__.onUpdate.call(this);
    };

    Project.prototype.transitionIn = function() {
      this.projectsBottomNavMenu.element.css("opacity", 1);
      this.projectsTopNavMenu.element.css("opacity", 1);
      return Project.__super__.transitionIn.call(this);
    };

    Project.prototype.transitionOutCompleted = function() {
      TweenMax.set(this.bottomEl, {
        autoAlpha: 0
      });
      return Project.__super__.transitionOutCompleted.call(this);
    };

    Project.prototype.resize = function() {
      this.onScroll();
      this.projectTop.resize();
      if (this.projectsBottomNavMenu != null) {
        this.projectsBottomNavMenu.resize();
      }
      this.projectsTopNavMenu.resize();
      this.pageHeight = Model.windowH + this.bottomEl.height() + this.projectsBottomNavMenu.height;
      this.topEl.width(Model.windowW);
      this.topEl.css("height", Model.windowH);
      this.scrollableLayersHolder.css("top", Model.windowH);
      Project.__super__.resize.call(this);
      this.projectsBottomNavMenu.element.css("top", this.pageHeight - this.footer.height - this.projectsBottomNavMenu.height);
    };

    Project.prototype.destroy = function() {
      var m, _i, _len, _ref;
      if (this.magnifiers != null) {
        _ref = this.magnifiers;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          m = _ref[_i];
          m.destroy();
          m = null;
        }
        this.magnifiers.length = 0;
      }
      if (this.projectTop != null) {
        this.projectTop.destroy();
      }
      if (this.projectTextTransformer != null) {
        this.projectTextTransformer.destroy();
      }
      if (this.projectsBottomNavMenu != null) {
        this.projectsBottomNavMenu.destroy();
      }
      if (this.projectsTopNavMenu != null) {
        this.projectsTopNavMenu.destroy();
      }
      if (this.scrollPositionCheckElements != null) {
        this.scrollPositionCheckElements.length = 0;
      }
      if (this.toCheckElements != null) {
        this.toCheckElements.length = 0;
      }
      if (this.scrollables != null) {
        this.scrollables.length = 0;
      }
      this.magnifiers = null;
      this.scrollPositionCheckElements = null;
      this.toCheckElements = null;
      this.scrollables = null;
      Project.__super__.destroy.call(this);
    };

    return Project;

  })(Page);
  return Project;
});
