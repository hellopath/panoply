var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define([], function() {
  var LogoA;
  LogoA = (function() {
    function LogoA() {
      this.onUpdate = __bind(this.onUpdate, this);
      this.init = __bind(this.init, this);
      this.container = new THREE.Group();
    }

    LogoA.prototype.init = function() {
      var tex;
      this.geom = Loader.getGeometry("werkstatt");
      tex = Loader.getTexture("dior-full-test-image");
      this.mat = new THREE.MeshBasicMaterial({
        color: 0xc4c4c4,
        map: tex
      });
      this.geom = new THREE.BoxGeometry(10, 10, 10);
      this.logo = new THREE.Mesh(this.geom, this.mat);
      this.container.add(this.logo);
      return this.logo.scale.set(1, 1, 1);
    };

    LogoA.prototype.onUpdate = function() {
      var sc, time;
      time = Date.now() * 0.0001;
      this.logo.rotation.z = Math.sin(time) * 0.5;
      this.logo.rotation.y = Math.sin(time) * 0.5;
      sc = 1.4 + Math.cos(time) * 0.2;
      return this.logo.scale.set(sc, sc, sc);
    };

    return LogoA;

  })();
  return LogoA;
});
