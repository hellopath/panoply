var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define([], function() {
  var glDetector;
  glDetector = (function() {
    glDetector.prototype.canvas = void 0;

    glDetector.prototype.webgl = void 0;

    function glDetector() {
      this.displayMessage = __bind(this.displayMessage, this);
      this.webgl = __bind(this.webgl, this);
      this.canvas = !!window.CanvasRenderingContext2D;
    }

    glDetector.prototype.webgl = function() {
      var context, isEnable;
      isEnable = false;
      this.canvas = document.createElement("canvas");
      context = !!window.WebGLRenderingContext && (this.canvas.getContext("webgl") || this.canvas.getContext("experimental-webgl"));
      if (context != null) {
        isEnable = true;
      } else {
        isEnable = false;
      }
      return isEnable;
    };

    glDetector.prototype.displayMessage = function(el) {
      var mes;
      mes = "One day, just maybe...<br>			Your browser will support WebGL.<br>			<br>			Fortunately you can enjoy our wishes on <a href='https://www.google.com/intl/en/chrome/browser/'>Chrome</a> or <a href='http://www.mozilla.org/en-US/firefox/new/'>Firefox</a>.";
      return el.append("<p>" + mes + "</p>");
    };

    return glDetector;

  })();
  return glDetector;
});
