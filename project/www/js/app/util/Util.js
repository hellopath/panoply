var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define([], function() {
  "use strict";
  var Util;
  Util = (function() {
    function Util() {
      this.GetSupportedVideoExtension = __bind(this.GetSupportedVideoExtension, this);
      this.GetChapterUniqueClass = __bind(this.GetChapterUniqueClass, this);
      this.GetRect = __bind(this.GetRect, this);
      this.RemoveHTMLTags = __bind(this.RemoveHTMLTags, this);
      this.SetupAnimatedSprite = __bind(this.SetupAnimatedSprite, this);
      this.GetRoutingIndexById = __bind(this.GetRoutingIndexById, this);
      this.SetupCanvas = __bind(this.SetupCanvas, this);
      this.CommaSeparateNumber = __bind(this.CommaSeparateNumber, this);
      this.ReplaceAll = __bind(this.ReplaceAll, this);
      this.GetCookie = __bind(this.GetCookie, this);
      this.SetCookie = __bind(this.SetCookie, this);
      this.GetAdjustmentContainerSize = __bind(this.GetAdjustmentContainerSize, this);
      this.GetUrlVars = __bind(this.GetUrlVars, this);
      this.IsEven = __bind(this.IsEven, this);
      this.Rand = __bind(this.Rand, this);
      this.GetImgNativeSize = __bind(this.GetImgNativeSize, this);
      this.ToFixed = __bind(this.ToFixed, this);
      this.IsInt = __bind(this.IsInt, this);
      this.RandomInt = __bind(this.RandomInt, this);
      this.ConvertToClassName = __bind(this.ConvertToClassName, this);
      this.ConvertToSlug = __bind(this.ConvertToSlug, this);
      this.Limit = __bind(this.Limit, this);
      this.RadiansToDegrees = __bind(this.RadiansToDegrees, this);
      this.DegreesToRadians = __bind(this.DegreesToRadians, this);
      this.JqueryObjToString = __bind(this.JqueryObjToString, this);
      this.CapitalizeFirstLetter = __bind(this.CapitalizeFirstLetter, this);
    }

    Util.prototype.CapitalizeFirstLetter = function(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    };

    Util.prototype.JqueryObjToString = function(jobj) {
      return $('<div>').append($(jobj).clone()).html();
    };

    Util.prototype.DegreesToRadians = function(degrees) {
      return degrees * (Math.PI / 180);
    };

    Util.prototype.RadiansToDegrees = function(radians) {
      return radians * (180 / Math.PI);
    };

    Util.prototype.Limit = function(min, max, value) {
      return Math.max(min, Math.min(max, value));
    };

    Util.prototype.ConvertToSlug = function(text, separator) {
      separator = separator != null ? separator : "-";
      return text.toLowerCase().replace(/[^\w ]+/g, "").replace(RegExp(" +", "g"), separator);
    };

    Util.prototype.ConvertToClassName = function(text) {
      var t, tArray, txt, _i, _len;
      tArray = text.split("-");
      txt = "";
      if (tArray.length > 1) {
        for (_i = 0, _len = tArray.length; _i < _len; _i++) {
          t = tArray[_i];
          txt += this.CapitalizeFirstLetter(t);
        }
      } else {
        txt = this.CapitalizeFirstLetter(tArray[0]);
      }
      return txt;
    };

    Util.prototype.RandomInt = function(multiplier) {
      if (multiplier == null) {
        multiplier = 10000000;
      }
      if (multiplier != null) {
        multiplier;
      } else {
        10000000;
      }
      return Math.round(Math.random() * multiplier) + 1;
    };

    Util.prototype.IsInt = function(n) {
      return n % 1 === 0;
    };

    Util.prototype.ToFixed = function(number, factor) {
      return Math.round(number * factor) / factor;
    };

    Util.prototype.GetImgNativeSize = function(url) {
      var img;
      img = new Image();
      img.src = url;
      return {
        width: img.width,
        height: img.height
      };
    };

    Util.prototype.Rand = function(max, min, decimals) {
      var d, randomNum;
      if (min > max) {
        void 0;
      }
      randomNum = Math.random() * (max - min) + min;
      d = Math.pow(10, decimals);
      return ~~((d * randomNum) + 0.5) / d;
    };

    Util.prototype.IsEven = function(value) {
      if ((value % 2) === 0) {
        return true;
      } else {
        return false;
      }
    };

    Util.prototype.GetUrlVars = function(key) {
      var parts, vars;
      vars = {};
      parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/g, function(m, key, value) {
        var cleanVal, indexPos;
        cleanVal = value;
        if (value.indexOf("#") > 0) {
          indexPos = value.indexOf("#");
          cleanVal = value.slice(0, indexPos);
        }
        vars[key] = cleanVal;
      });
      return vars;
    };

    Util.prototype.GetAdjustmentContainerSize = function(viewportWidth, viewportHeight, originalW, originalH) {
      var holderH, holderW, originalAspectRatio, scaleX, scaleY, type, viewportAspectRatio;
      viewportWidth = viewportWidth;
      viewportHeight = viewportHeight;
      viewportAspectRatio = viewportWidth / viewportHeight;
      originalAspectRatio = originalW / originalH;
      scaleX = (viewportWidth / originalW) * 1;
      scaleY = (viewportHeight / originalH) * 1;
      if (viewportAspectRatio > originalAspectRatio) {
        type = "landscape";
        holderW = originalW * scaleX;
        holderH = viewportHeight;
      } else {
        type = "portrait";
        holderW = viewportWidth;
        holderH = originalH * scaleY;
      }
      return {
        type: type,
        holderW: holderW,
        holderH: holderH,
        originalW: originalW,
        originalH: originalH,
        scaleX: scaleX,
        scaleY: scaleY
      };
    };

    Util.prototype.SetCookie = function(cname, cvalue) {
      return document.cookie = cname + "=" + cvalue;
    };

    Util.prototype.GetCookie = function(cname) {
      var c, ca, cookie, i, name;
      name = cname + "=";
      ca = document.cookie.split(';');
      i = 0;
      cookie = "";
      while (i < ca.length) {
        c = ca[i].trim();
        if (c.indexOf(name) === 0) {
          cookie = c.substring(name.length, c.length);
        }
        i++;
      }
      return cookie;
    };

    Util.prototype.ReplaceAll = function(find, replace, str) {
      return str.replace(new RegExp(find, 'g'), replace);
    };

    Util.prototype.CommaSeparateNumber = function(val) {
      while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
      }
      return val;
    };

    Util.prototype.SetupCanvas = function(params) {
      var stage;
      params.element.css({
        width: params.width + "px",
        height: params.height + "px"
      });
      stage = new createjs.Stage(params.element[0]);
      stage.canvas.width = params.width;
      stage.canvas.height = params.height;
      return stage;
    };

    Util.prototype.GetRoutingIndexById = function(id) {
      var i, route, _i, _len, _ref;
      _ref = Model.routing;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        route = _ref[i];
        if (route.id === id) {
          return i;
        }
      }
    };

    Util.prototype.SetupAnimatedSprite = function(params) {
      var data, sprite, spriteSheet, stage;
      params.framerate = params.framerate || 30;
      params.regX = params.regX || 0;
      params.regY = params.regY || 0;
      stage = this.SetupCanvas(params);
      data = {
        images: [params.image],
        frames: {
          width: params.width,
          height: params.height,
          regX: params.regX,
          regY: params.regX,
          count: params.count
        },
        animations: {
          run: [0, params.count - 1, "run"]
        }
      };
      spriteSheet = new createjs.SpriteSheet(data);
      sprite = new createjs.Sprite(spriteSheet, "run");
      sprite.framerate = params.framerate;
      if (params.paused) {
        sprite.gotoAndStop(0);
      } else {
        sprite.gotoAndPlay(0);
      }
      stage.addChild(sprite);
      createjs.Ticker.timingMode = createjs.Ticker.RAF;
      return {
        stage: stage,
        sprite: sprite,
        tick: params.tick
      };
    };

    Util.prototype.RemoveHTMLTags = function(str) {
      return str.replace(/<\/?[^>]+(>|$)/g, "");
    };

    Util.prototype.GetRect = function(top, right, bottom, left) {
      return "rect(" + top + "px " + right + "px " + bottom + "px " + left + "px" + ")";
    };

    Util.prototype.GetChapterUniqueClass = function($chapter) {
      var splitter, uniqueClass;
      splitter = $chapter.attr("class").split(" ");
      uniqueClass = "." + splitter[splitter.length - 1];
      return uniqueClass;
    };

    Util.prototype.GetSupportedVideoExtension = function() {
      var support;
      support = Modernizr.video;
      if (support["h264"] !== "") {
        return "mp4";
      } else if (support["ogg"] !== "") {
        return "ogg";
      } else {
        return "webm";
      }
    };

    return Util;

  })();
  return Util;
});
