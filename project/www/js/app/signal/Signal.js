define(["signals"], function(signals) {
  "use strict";
  var Signal;
  Signal = (function() {
    Signal.prototype.onResize = new signals.Signal();

    Signal.prototype.onUpdate = new signals.Signal();

    Signal.prototype.onWheel = new signals.Signal();

    Signal.prototype.onScroll = new signals.Signal();

    Signal.prototype.onRouteChanged = new signals.Signal();

    Signal.prototype.onKeyPressed = new signals.Signal();

    Signal.prototype.initViews = new signals.Signal();

    Signal.prototype.startRouting = new signals.Signal();

    function Signal() {}

    return Signal;

  })();
  return Signal;
});
