var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define([], function() {
  var PagesTransitionController;
  PagesTransitionController = (function() {
    PagesTransitionController.prototype.type = void 0;

    function PagesTransitionController() {
      this.getTransitionType = __bind(this.getTransitionType, this);
      this.getProjectIndexById = __bind(this.getProjectIndexById, this);
      this.cleanBetweenState = __bind(this.cleanBetweenState, this);
      this.newViewFromProjectToProjectReady = __bind(this.newViewFromProjectToProjectReady, this);
      this.onProjectToProjectTransitionStarted = __bind(this.onProjectToProjectTransitionStarted, this);
      this.newViewFromProjectToHomeReady = __bind(this.newViewFromProjectToHomeReady, this);
      this.onProjectToHomeTransitionStarted = __bind(this.onProjectToHomeTransitionStarted, this);
      this.onVideoTransitionCompleted = __bind(this.onVideoTransitionCompleted, this);
      this.onVideoTransitionStarted = __bind(this.onVideoTransitionStarted, this);
      this.videoPageTransitionStart = __bind(this.videoPageTransitionStart, this);
      this.onPageTransition = __bind(this.onPageTransition, this);
      Signal.pagesTransition.add(this.onPageTransition);
    }

    PagesTransitionController.prototype.onPageTransition = function() {
      switch (this.type) {
        case G.NORMAL_TRANSITION:
          Switcher.continueNewViewTransition();
          break;
        case G.VIDEO_TRANSITION:
          this.onVideoTransitionStarted();
          break;
      }
    };

    PagesTransitionController.prototype.videoPageTransitionStart = function(callback) {
      var $visualPart, currentView, newPage, projectIndex, visualPosY,
        _this = this;
      currentView = Switcher.newView;
      newPage = Switcher.newPage;
      projectIndex = this.getProjectIndexById(newPage.id, currentView);
      $visualPart = $(currentView.screens[projectIndex]);
      visualPosY = parseInt($visualPart.attr("data-posy"), 10) + Model.windowH;
      $visualPart.css("z-index", 9999);
      Model.parentEl.css({
        "position": "static"
      });
      Signal.windowTo.dispatch(visualPosY, 1.6);
      Signal.windowToCompleted.addOnce(function() {
        return TweenMax.delayedCall(0.3, function() {
          $(window).scrollTop(visualPosY);
          Signal.changeScrollingState.dispatch(G.DISABLE);
          return callback();
        });
      });
    };

    PagesTransitionController.prototype.onVideoTransitionStarted = function() {
      var $projectImage, $projectImageContainer, $titleInside, $titlePart, ease, homeView, newPage, projectIndex,
        _this = this;
      homeView = Switcher.oldView;
      newPage = Switcher.newPage;
      projectIndex = this.getProjectIndexById(newPage.id, homeView);
      $projectImageContainer = $(homeView.projectImageContainers[projectIndex]);
      $projectImage = $(homeView.projectImages[projectIndex]);
      $titlePart = $(homeView.titles[projectIndex]);
      $titleInside = $titlePart.find(".title");
      ease = Power3.easeInOut;
      Signal.videoPageLoaded.addOnce(function() {
        var totalDuration;
        totalDuration = _this.currentLoaderTimeline.totalDuration();
        _this.currentLoaderTimeline.timeScale(1.2);
        _this.currentLoaderTimeline.eventCallback("onComplete", function() {
          var $projectVideo, $projectVideoContainer, newView, toRectStr;
          _this.currentLoaderTimeline.eventCallback("onComplete", null);
          _this.currentLoaderTimeline = void 0;
          newView = Switcher.newView;
          newView.projectTop.videoLayer.seek(0);
          newView.projectTop.videoLayer.pause();
          TweenMax.to($titleInside, 1, {
            scale: 6,
            transformOrigin: "50% 50%",
            opacity: 0,
            ease: ease
          });
          newView.element.find(".text-container").css({
            display: "none"
          });
          newView.element.css({
            height: Model.windowH,
            opacity: 0,
            position: "fixed"
          });
          TweenMax.to($projectImage, 0.3, {
            opacity: 0,
            ease: Circ.easeOut
          });
          $projectVideo = newView.element.find("#project-video");
          $projectVideoContainer = newView.element.find(".video-container");
          toRectStr = Util.GetRect(0, Model.windowW, Model.windowH, 0);
          $projectVideoContainer.css({
            width: Model.windowW,
            height: Model.windowH
          });
          $projectVideo.css(homeView.projectImageCss);
          TweenMax.set($projectVideoContainer, {
            clip: homeView.normalStateRectStr
          });
          TweenMax.set($projectVideo, {
            scale: homeView.imageInsideScale,
            force3D: true,
            transformOrigin: "50% 50%"
          });
          TweenMax.to(newView.element, 0.2, {
            opacity: 1,
            ease: Circ.easeOut
          });
          return TweenMax.delayedCall(0.1, function() {
            newView.projectTop.videoLayer.play();
            TweenMax.to($projectVideoContainer, 1, {
              clip: toRectStr,
              ease: ease
            });
            return TweenMax.to($projectVideo, 1, {
              scale: 1,
              force3D: true,
              transformOrigin: "50% 50%",
              ease: ease,
              onComplete: function() {
                homeView.transitionOutCompleted();
                return Signal.videoTransitionCompleted.dispatch();
              }
            });
          });
        });
        return _this.currentLoaderTimeline.tweenTo(totalDuration);
      });
    };

    PagesTransitionController.prototype.onVideoTransitionCompleted = function() {
      var i, newView, oldView, project, projectId, randomTime, totalDuration, _i, _ref,
        _this = this;
      Switcher.prepareView();
      Signal.pagesTransition.dispatch();
      newView = Switcher.newView;
      oldView = Switcher.oldView;
      newView.onProjectVideoReady = Signal.videoPageLoaded.dispatch;
      for (i = _i = 0, _ref = oldView.projectsLen - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        projectId = oldView.scope.projects[i].id;
        if (projectId === newView.id) {
          project = oldView.projects[i];
          this.currentLoaderTimeline = project.tl;
          TweenMax.set(project.coloredImg, {
            opacity: 1
          });
          break;
        }
      }
      totalDuration = this.currentLoaderTimeline.totalDuration();
      randomTime = Util.Rand(totalDuration * 0.7, totalDuration * 0.1, 2);
      this.currentLoaderTimeline.timeScale(0.4);
      this.currentLoaderTimeline.tweenTo(randomTime);
      newView.preInit();
      Model.pageEl.prepend(newView.element);
      Model.parentEl.css("position", "fixed");
      Signal.videoTransitionCompleted.addOnce(function() {
        var manifest;
        Switcher.destroyViews();
        Signal.logoStateChanged.dispatch(G.LOGO_LOAD_STARTED_STATE);
        manifest = [];
        manifest = manifest.concat(Switcher.getFilesManifest());
        Loader.load(manifest, function() {
          var $projectVideoContainer;
          $projectVideoContainer = newView.element.find(".top-container #project-top-container-view .video-container");
          $projectVideoContainer.css({
            clip: "auto",
            width: "100%",
            height: "100%"
          });
          newView.element.find(".text-container").css({
            display: "block"
          });
          _this.type = G.NORMAL_TRANSITION;
          newView.allReadyCallback = Switcher.onAllReady;
          newView.init();
          return Signal.logoStateChanged.dispatch(G.LOGO_LOAD_COMPLETED_STATE);
        });
      });
    };

    PagesTransitionController.prototype.onProjectToHomeTransitionStarted = function() {
      var newView, oldView;
      Switcher.prepareView();
      newView = Switcher.newView;
      oldView = Switcher.oldView;
      newView.element.css({
        "position": "fixed"
      });
      oldView.element.css({
        "position": "fixed"
      });
      newView.allReadyCallback = this.newViewFromProjectToHomeReady;
      newView.preInit();
      Model.pageEl.prepend(newView.element);
      newView.init();
    };

    PagesTransitionController.prototype.newViewFromProjectToHomeReady = function() {
      var $projectImage, $projectImageContainer, $projectVideo, $projectVideoContainer, $visualPart, fromRectStr, newView, oldView, projectIndex, toRectStr, visualPosY,
        _this = this;
      newView = Switcher.newView;
      oldView = Switcher.oldView;
      newView.ready();
      newView.resize();
      newView.addAnimations();
      newView.transitionIn();
      projectIndex = this.getProjectIndexById(oldView.id, newView);
      $visualPart = $(newView.screens[projectIndex]);
      visualPosY = parseInt($visualPart.attr("data-posy"), 10) + Model.windowH;
      $projectImageContainer = $(newView.projectImageContainers[projectIndex]);
      $projectImage = $(newView.projectImages[projectIndex]);
      $projectVideo = oldView.element.find("#project-video");
      $projectVideoContainer = oldView.element.find(".video-container");
      Model.parentEl.css({
        "position": "static"
      });
      $(window).scrollTop(visualPosY);
      toRectStr = newView.normalStateRectStr;
      fromRectStr = Util.GetRect(0, Model.windowW, Model.windowH, 0);
      TweenMax.set($projectVideoContainer, {
        clip: fromRectStr
      });
      TweenMax.set($projectImageContainer, {
        clip: fromRectStr
      });
      TweenMax.delayedCall(0.1, function() {
        var ease;
        ease = Expo.easeInOut;
        TweenMax.to($projectVideoContainer, 1, {
          clip: toRectStr,
          ease: ease
        });
        TweenMax.to($projectImageContainer, 1, {
          clip: toRectStr,
          ease: ease
        });
        TweenMax.to($projectVideo, 1, {
          scale: newView.imageInsideScale,
          force3D: true,
          transformOrigin: "50% 50%",
          ease: ease
        });
        TweenMax.to($projectImage, 1, {
          scale: newView.imageInsideScale,
          force3D: true,
          transformOrigin: "50% 50%",
          ease: ease
        });
        TweenMax.to($projectVideoContainer, 1, {
          delay: 0.8,
          opacity: 0,
          ease: ease,
          onComplete: function() {
            return Switcher.destroyViews();
          }
        });
      });
    };

    PagesTransitionController.prototype.onProjectToProjectTransitionStarted = function() {
      var newView, oldView,
        _this = this;
      Switcher.prepareView();
      newView = Switcher.newView;
      oldView = Switcher.oldView;
      newView.onProjectVideoReady = Signal.videoPageLoaded.dispatch;
      newView.preInit();
      Model.pageEl.prepend(newView.element);
      newView.projectTop.videoLayer.seek(0);
      newView.projectTop.videoLayer.pause();
      return Signal.videoPageLoaded.addOnce(function() {
        var fromLeft, newViewIndex, oldStartPosX, oldViewIndex;
        newView.element.find(".text-container").css({
          display: "none"
        });
        newView.element.css({
          "z-index": 5,
          height: Model.windowH,
          opacity: 1,
          position: "fixed"
        });
        newView.projectTop.videoLayer.play();
        newViewIndex = Util.GetRoutingIndexById(newView.id);
        oldViewIndex = Util.GetRoutingIndexById(oldView.id);
        if (newViewIndex > oldViewIndex) {
          fromLeft = false;
        } else {
          fromLeft = true;
        }
        if (fromLeft) {
          TweenMax.set(newView.element, {
            x: -Model.windowW,
            force3D: true,
            transformOrigin: "0% 50%"
          });
          oldStartPosX = Model.windowW;
        } else {
          TweenMax.set(newView.element, {
            x: Model.windowW,
            force3D: true,
            transformOrigin: "0% 50%"
          });
          oldStartPosX = -Model.windowW;
        }
        TweenMax.to(newView.element, 1, {
          x: 0,
          force3D: true,
          transformOrigin: "0% 50%",
          ease: Expo.easeInOut
        });
        return TweenMax.to(oldView.element, 1.1, {
          x: oldStartPosX,
          force3D: true,
          transformOrigin: "0% 50%",
          ease: Expo.easeInOut,
          onComplete: _this.newViewFromProjectToProjectReady
        });
      });
    };

    PagesTransitionController.prototype.newViewFromProjectToProjectReady = function() {
      var manifest, newView, oldView,
        _this = this;
      newView = Switcher.newView;
      oldView = Switcher.oldView;
      Model.parentEl.css("position", "fixed");
      Switcher.destroyViews();
      Signal.logoStateChanged.dispatch(G.LOGO_LOAD_STARTED_STATE);
      manifest = [];
      manifest = manifest.concat(Switcher.getFilesManifest());
      Loader.load(manifest, function() {
        newView.element.find(".text-container").css({
          display: "block"
        });
        _this.type = G.NORMAL_TRANSITION;
        newView.allReadyCallback = Switcher.onAllReady;
        newView.init();
        return Signal.logoStateChanged.dispatch(G.LOGO_LOAD_COMPLETED_STATE);
      });
    };

    PagesTransitionController.prototype.cleanBetweenState = function() {
      return Loader.close();
    };

    PagesTransitionController.prototype.getProjectIndexById = function(id, view) {
      var i, newId, _i, _ref;
      newId = id;
      for (i = _i = 0, _ref = view.projectsLen - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        if (view.projects[i].id === newId) {
          return i;
          break;
        }
      }
    };

    PagesTransitionController.prototype.getTransitionType = function() {
      var newPage, oldPage;
      newPage = Switcher.newPage;
      oldPage = Switcher.oldPage;
      if (oldPage == null) {
        this.type = G.NORMAL_TRANSITION;
      } else if (newPage.route !== "home" && oldPage.route !== "home") {
        this.type = G.SLIDE_TRANSITION_TO_PROJECT;
      } else if (newPage.route === "home" && oldPage.route !== "home") {
        this.type = G.SLIDE_TRANSITION_TO_HOME;
      } else {
        this.type = G.VIDEO_TRANSITION;
      }
    };

    return PagesTransitionController;

  })();
  return PagesTransitionController;
});
