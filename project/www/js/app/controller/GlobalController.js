var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["Header", "Footer"], function(Header, Footer) {
  "use strict";
  var GlobalController;
  GlobalController = (function() {
    function GlobalController() {
      this.setupViews = __bind(this.setupViews, this);
    }

    GlobalController.prototype.setupViews = function() {
      var footer, header;
      header = new Header("header");
      Model.parentEl.append(header.element);
      header.init();
      footer = new Footer("footer");
      Model.parentEl.append(footer.element);
      footer.init();
    };

    return GlobalController;

  })();
  return GlobalController;
});
