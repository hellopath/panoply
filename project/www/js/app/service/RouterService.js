var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["hasher"], function(hasher) {
  "use strict";
  var RouterService;
  RouterService = (function() {
    function RouterService() {
      this.getNextPreviousProject = __bind(this.getNextPreviousProject, this);
      this.getNewViewFromRoute = __bind(this.getNewViewFromRoute, this);
      this.sendToDefault = __bind(this.sendToDefault, this);
      this.pageChanged = __bind(this.pageChanged, this);
      this.onRouteChanged = __bind(this.onRouteChanged, this);
      this.configHasher = __bind(this.configHasher, this);
      this.createRoute = __bind(this.createRoute, this);
      this.setupRouting = __bind(this.setupRouting, this);
    }

    RouterService.prototype.setupRouting = function() {
      var children, project, route, routes, _i, _j, _len, _len1, _ref;
      routes = [];
      _ref = Model.routing;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        route = _ref[_i];
        if (route.children != null) {
          children = route.children;
          for (_j = 0, _len1 = children.length; _j < _len1; _j++) {
            project = children[_j];
            routes.push(this.createRoute(project.id, route.id + "/" + project.id, project.scope, project.files, project.filterParams, project.name));
          }
        } else {
          routes.push(this.createRoute(route.id, route.id, route.scope, route.files, route.name));
        }
      }
      Model.routing = routes;
    };

    RouterService.prototype.createRoute = function(id, route, scope, files, filterParams, name) {
      var r;
      r = {};
      r.id = id;
      r.clazz = Util.ConvertToClassName(id);
      r.route = route;
      r.parentRoute = route.split("/")[0];
      r.scope = scope;
      r.files = files;
      r.filterParams = filterParams;
      r.name = name;
      r.scope.id = id;
      return r;
    };

    RouterService.prototype.configHasher = function() {
      hasher.prependHash = '/';
      hasher.changed.add(this.onRouteChanged);
      hasher.initialized.add(this.onRouteChanged);
      hasher.init();
    };

    RouterService.prototype.onRouteChanged = function(newHash) {
      var newHashFounded, r, _i, _len, _ref;
      newHashFounded = false;
      _ref = Model.routing;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        r = _ref[_i];
        if (newHash === r.route) {
          Model.newHash = r.route;
          newHashFounded = true;
          this.pageChanged();
        }
      }
      if (!newHashFounded) {
        this.sendToDefault();
      }
    };

    RouterService.prototype.pageChanged = function() {
      Signal.onRouteChanged.dispatch();
    };

    RouterService.prototype.sendToDefault = function() {
      hasher.setHash("home");
    };

    RouterService.prototype.getNewViewFromRoute = function(newHash) {
      var r, view, _i, _len, _ref;
      view = void 0;
      _ref = Model.routing;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        r = _ref[_i];
        if (r.route === newHash) {
          view = r;
          return view;
          break;
        }
      }
    };

    RouterService.prototype.getNextPreviousProject = function() {
      var i, next, nextId, previous, previousId, r, totalLen, _i, _len, _ref;
      totalLen = Model.routing.length;
      _ref = Model.routing;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        r = _ref[i];
        if (r.route === Model.newHash) {
          previousId = i === 1 ? totalLen - 1 : i - 1;
          nextId = i === totalLen - 1 ? 1 : i + 1;
          previous = Model.routing[previousId];
          next = Model.routing[nextId];
        }
      }
      return {
        previous: previous,
        next: next
      };
    };

    return RouterService;

  })();
  return RouterService;
});
