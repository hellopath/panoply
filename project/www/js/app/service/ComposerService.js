var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["ShaderPass", "EffectComposer", "RenderPass", "MaskPass", "CopyShader", "KaleidoShader"], function(ShaderPass, EffectComposer, RenderPass, MaskPass, CopyShader, KaleidoShader) {
  "use strict";
  var ComposerService;
  ComposerService = (function() {
    function ComposerService() {
      this.onUpdate = __bind(this.onUpdate, this);
      this.addDebugInfo = __bind(this.addDebugInfo, this);
      this.ready = __bind(this.ready, this);
      this.init = __bind(this.init, this);
    }

    ComposerService.prototype.init = function() {
      this.ready();
      return this.addDebugInfo();
    };

    ComposerService.prototype.ready = function() {
      this.composer = new THREE.EffectComposer(Renderer);
      this.composer.addPass(new THREE.RenderPass(Scene, Camera));
      this.kaleidoscope = new THREE.ShaderPass(THREE.KaleidoShader);
      this.kaleidoscope.uniforms['sides'].value = 20;
      this.kaleidoscope.uniforms['angle'].value = 10;
      this.composer.addPass(this.kaleidoscope);
      return this.kaleidoscope.renderToScreen = true;
    };

    ComposerService.prototype.addDebugInfo = function() {};

    ComposerService.prototype.onUpdate = function() {};

    return ComposerService;

  })();
  return ComposerService;
});
