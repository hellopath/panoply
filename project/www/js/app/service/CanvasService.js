var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["THREE", "LogoA"], function(THREE, LogoA) {
  "use strict";
  var CanvasService;
  CanvasService = (function() {
    CanvasService.prototype.mainContainer = void 0;

    CanvasService.prototype.FIX_HEIGHT = 0.529;

    function CanvasService() {
      this.onUpdate = __bind(this.onUpdate, this);
      this.onResize = __bind(this.onResize, this);
      this.resizeBackground = __bind(this.resizeBackground, this);
      this.applyBackground = __bind(this.applyBackground, this);
      this.onRender = __bind(this.onRender, this);
      this.startRender = __bind(this.startRender, this);
      this.remove = __bind(this.remove, this);
      this.add = __bind(this.add, this);
      this.init = __bind(this.init, this);
    }

    CanvasService.prototype.init = function() {
      this.clearColor = new THREE.Color(0xd6d6d6);
      this.scene = new THREE.Scene();
      this.camera = new THREE.PerspectiveCamera(30, Model.windowW / Model.windowH, 1, 1000);
      this.camera.position.set(0, 0, 1);
      this.renderer = new THREE.WebGLRenderer({
        antialias: true,
        clearAlpha: 1,
        premultipliedAlpha: false
      });
      window.Renderer = this.renderer;
      window.Camera = this.camera;
      window.Scene = this.scene;
      Composer.init();
      this.renderer.setSize(Model.windowW, Model.windowH);
      Model.backEl.append(this.renderer.domElement);
      this.renderer.setClearColor(this.clearColor, 1);
      this.renderer.gammaInput = true;
      this.renderer.gammaOutput = true;
      this.renderer.autoClear = false;
      this.backgroundContainer = new THREE.Group();
      this.scene.add(this.backgroundContainer);
      this.mainContainer = new THREE.Group();
      this.scene.add(this.mainContainer);
      this.tanFOV = Math.tan((Math.PI / 180) * this.camera.fov / 2);
      this.applyBackground();
      Signal.onResize.add(this.onResize);
      return this.onResize();
    };

    CanvasService.prototype.add = function(object) {
      return this.mainContainer.add(object);
    };

    CanvasService.prototype.remove = function(object) {
      return this.mainContainer.remove(object);
    };

    CanvasService.prototype.startRender = function() {
      return this.onUpdate();
    };

    CanvasService.prototype.onRender = function() {};

    CanvasService.prototype.applyBackground = function() {
      var c, cubeNum, cubeWidth, evenCounter, geometry, i, material, oddCounter, planeWidth, posX, _i, _ref, _results;
      planeWidth = 243;
      geometry = new THREE.PlaneBufferGeometry(planeWidth, 5.5);
      material = new THREE.MeshBasicMaterial({
        color: 0xf1f1f1
      });
      cubeNum = Math.round(Model.windowW / 240) + 10;
      if (!Util.IsEven(cubeNum)) {
        cubeNum += 1;
      }
      cubeWidth = planeWidth * 1.004;
      evenCounter = 0;
      oddCounter = 0;
      _results = [];
      for (i = _i = 0, _ref = cubeNum - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        c = new THREE.Mesh(geometry, material);
        this.backgroundContainer.add(c);
        if (Util.IsEven(i)) {
          posX = (cubeWidth / 2) + (evenCounter * cubeWidth);
          posX = posX;
          evenCounter += 1;
        } else {
          posX = (cubeWidth / 2) + (oddCounter * cubeWidth);
          posX = -posX;
          oddCounter += 1;
        }
        _results.push(c.position.x = posX);
      }
      return _results;
    };

    CanvasService.prototype.resizeBackground = function() {
      var scaleY;
      scaleY = (Model.windowH / this.FIX_HEIGHT) * 1;
      return this.backgroundContainer.scale.y = scaleY;
    };

    CanvasService.prototype.onResize = function() {
      this.camera.aspect = Model.windowW / Model.windowH;
      this.camera.fov = (360 / Math.PI) * Math.atan(this.tanFOV * (Model.windowH / this.FIX_HEIGHT));
      this.camera.updateProjectionMatrix();
      this.camera.lookAt(this.scene.position);
      this.renderer.setSize(Model.windowW, Model.windowH);
      this.resizeBackground();
      return this.onUpdate();
    };

    CanvasService.prototype.onUpdate = function() {
      this.onRender();
      this.renderer.clear(this.clearColor, 1);
      this.renderer.render(this.scene, this.camera);
      return this.requestAnimationId = requestAnimationFrame(this.onUpdate);
    };

    return CanvasService;

  })();
  return CanvasService;
});
