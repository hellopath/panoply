var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["preloadjs"], function(preloadjs) {
  "use strict";
  var LoaderService;
  LoaderService = (function() {
    LoaderService.prototype.queue = void 0;

    LoaderService.prototype.geometryCollection = {};

    function LoaderService() {
      this.onLoadProgress = __bind(this.onLoadProgress, this);
      this.onLoadCompleted = __bind(this.onLoadCompleted, this);
      this.getContentById = __bind(this.getContentById, this);
      this.getGeometry = __bind(this.getGeometry, this);
      this.getPartial = __bind(this.getPartial, this);
      this.getSvg = __bind(this.getSvg, this);
      this.getShader = __bind(this.getShader, this);
      this.getTexture = __bind(this.getTexture, this);
      this.getVideoSrc = __bind(this.getVideoSrc, this);
      this.getImageURL = __bind(this.getImageURL, this);
      this.pushGeometry = __bind(this.pushGeometry, this);
      this.load = __bind(this.load, this);
      this.close = __bind(this.close, this);
      this.queue = new createjs.LoadQueue();
      this.queue.on("complete", this.onLoadCompleted);
      this.queue.on("progress", this.onLoadProgress);
    }

    LoaderService.prototype.close = function() {
      if (this.queue != null) {
        return this.queue.close();
      }
    };

    LoaderService.prototype.load = function(manifest, onLoaded, onProgress) {
      this.queue.onLoaded = onLoaded;
      this.queue.onProgress = onProgress;
      return this.queue.loadManifest(manifest);
    };

    LoaderService.prototype.pushGeometry = function(id, g) {
      return this.geometryCollection[id] = g;
    };

    LoaderService.prototype.getImageURL = function(id) {
      var result;
      result = $(this.queue.getResult(id)).attr("src");
      return result;
    };

    LoaderService.prototype.getVideoSrc = function(id) {
      return $(this.queue.getResult(id + "-video")).attr("src");
    };

    LoaderService.prototype.getTexture = function(id) {
      var img, texture;
      img = this.getContentById(id);
      texture = new THREE.Texture(img);
      texture.needsUpdate = true;
      return texture;
    };

    LoaderService.prototype.getShader = function(id) {
      return this.getContentById(id);
    };

    LoaderService.prototype.getSvg = function(id) {
      return this.getContentById(id + "-svg");
    };

    LoaderService.prototype.getPartial = function(id) {
      return this.getContentById(id + "-partial");
    };

    LoaderService.prototype.getGeometry = function(id) {
      return this.geometryCollection[id].clone();
    };

    LoaderService.prototype.getContentById = function(id) {
      return this.queue.getResult(id);
    };

    LoaderService.prototype.onLoadCompleted = function(e) {
      var q;
      q = this.queue;
      return typeof q.onLoaded === "function" ? q.onLoaded(e.currentTarget) : void 0;
    };

    LoaderService.prototype.onLoadProgress = function(e) {
      var q;
      q = this.queue;
      return typeof q.onProgress === "function" ? q.onProgress() : void 0;
    };

    return LoaderService;

  })();
  return LoaderService;
});
