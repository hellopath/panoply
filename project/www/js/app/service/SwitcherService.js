var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

define(["Home", "DiorAddict", "ErasableWall", "IRemember", "KitchenAid", "Nows", "OrchestreNationalDeLorraine", "RobertJaso", "SeatSeCompare", "StratoQuest", "TriesteNapoli"], function(Home, DiorAddict, ErasableWall, IRemember, KitchenAid, Nows, OrchestreNationalDeLorraine, RobertJaso, SeatSeCompare, StratoQuest, TriesteNapoli) {
  var SwitcherService;
  SwitcherService = (function() {
    function SwitcherService() {
      this.getProjectsVideoManifest = __bind(this.getProjectsVideoManifest, this);
      this.getPartialManifest = __bind(this.getPartialManifest, this);
      this.getFilesManifest = __bind(this.getFilesManifest, this);
      this.destroyViews = __bind(this.destroyViews, this);
      this.createView = __bind(this.createView, this);
      this.continueNewViewTransition = __bind(this.continueNewViewTransition, this);
      this.startTransition = __bind(this.startTransition, this);
      this.onAllReady = __bind(this.onAllReady, this);
      this.initView = __bind(this.initView, this);
      this.updateAndCreateView = __bind(this.updateAndCreateView, this);
      this.prepareView = __bind(this.prepareView, this);
      this.manifestNormalLoadCompleted = __bind(this.manifestNormalLoadCompleted, this);
      this.manifestProjectVideoPartLoadCompleted = __bind(this.manifestProjectVideoPartLoadCompleted, this);
      this.manifestToProjectLoadCompleted = __bind(this.manifestToProjectLoadCompleted, this);
      this.manifestToHomeLoadCompleted = __bind(this.manifestToHomeLoadCompleted, this);
      this.prepareManifestAndLoaded = __bind(this.prepareManifestAndLoaded, this);
      this.onRouteChanged = __bind(this.onRouteChanged, this);
      this.init = __bind(this.init, this);
      ({
        newPage: void 0,
        oldPage: void 0,
        newView: void 0,
        oldView: void 0,
        hist: void 0
      });
    }

    SwitcherService.prototype.init = function() {
      this.hist = [];
      Signal.onRouteChanged.add(this.onRouteChanged);
    };

    SwitcherService.prototype.onRouteChanged = function() {
      Transition.cleanBetweenState();
      this.oldPage = this.newPage;
      this.newPage = Router.getNewViewFromRoute(Model.newHash);
      Transition.getTransitionType();
      if (Model.menuIsOpened) {
        Signal.menuStateChanged.dispatch(G.CLOSE_MENU);
      }
      this.prepareManifestAndLoaded();
    };

    SwitcherService.prototype.prepareManifestAndLoaded = function() {
      var manifest,
        _this = this;
      manifest = [];
      switch (Transition.type) {
        case G.NORMAL_TRANSITION:
          manifest = manifest.concat(this.getFilesManifest());
          manifest = manifest.concat(this.getPartialManifest());
          Loader.load(manifest, this.manifestNormalLoadCompleted);
          break;
        case G.VIDEO_TRANSITION:
          Transition.videoPageTransitionStart(function() {
            manifest = manifest.concat(_this.getPartialManifest());
            return Loader.load(manifest, _this.manifestProjectVideoPartLoadCompleted);
          });
          break;
        case G.SLIDE_TRANSITION_TO_HOME:
          manifest = manifest.concat(this.getFilesManifest());
          manifest = manifest.concat(this.getPartialManifest());
          Loader.load(manifest, this.manifestToHomeLoadCompleted);
          break;
        case G.SLIDE_TRANSITION_TO_PROJECT:
          manifest = manifest.concat(this.getPartialManifest());
          Loader.load(manifest, this.manifestToProjectLoadCompleted);
          break;
      }
    };

    SwitcherService.prototype.manifestToHomeLoadCompleted = function() {
      Transition.onProjectToHomeTransitionStarted();
    };

    SwitcherService.prototype.manifestToProjectLoadCompleted = function() {
      Transition.onProjectToProjectTransitionStarted();
    };

    SwitcherService.prototype.manifestProjectVideoPartLoadCompleted = function() {
      Transition.onVideoTransitionCompleted();
    };

    SwitcherService.prototype.manifestNormalLoadCompleted = function() {
      this.prepareView();
      this.initView();
    };

    SwitcherService.prototype.prepareView = function() {
      var Class;
      Class = require(this.newPage.clazz);
      this.updateAndCreateView(Class);
    };

    SwitcherService.prototype.updateAndCreateView = function(Class) {
      if (this.newView != null) {
        this.oldView = this.newView;
      }
      this.newView = this.createView(this.newPage.id, Class, this.newPage.scope);
    };

    SwitcherService.prototype.initView = function() {
      this.newView.allReadyCallback = this.onAllReady;
      this.newView.preInit();
      Model.pageEl.prepend(this.newView.element);
      this.newView.init();
    };

    SwitcherService.prototype.onAllReady = function() {
      TweenMax.delayedCall(0, this.newView.ready);
      TweenMax.delayedCall(0, this.newView.resize);
      TweenMax.delayedCall(0, this.newView.addAnimations);
      this.newView.element.css("opacity", 1);
      TweenMax.delayedCall(0, this.startTransition);
    };

    SwitcherService.prototype.startTransition = function() {
      Signal.pagesTransition.dispatch();
    };

    SwitcherService.prototype.continueNewViewTransition = function() {
      this.newView.transitionIn();
      this.destroyViews();
    };

    SwitcherService.prototype.createView = function(id, Class, scope) {
      var view;
      view = new Class(id, scope);
      this.hist.push(view);
      return view;
    };

    SwitcherService.prototype.destroyViews = function() {
      var destroyArray, i, view, _i, _ref;
      if (this.hist.length <= 1) {
        return;
      }
      destroyArray = this.hist.splice(0, this.hist.length - 1);
      for (i = _i = 0, _ref = destroyArray.length - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
        view = destroyArray[i];
        if (view != null) {
          view.destroy();
          view.element.remove();
        }
      }
    };

    SwitcherService.prototype.getFilesManifest = function() {
      var file, id, manifest, node, splitArray, type, _i, _len, _ref;
      manifest = [];
      _ref = this.newPage.files;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];
        splitArray = file.split(".");
        id = splitArray[0];
        type = splitArray[1];
        node = {
          id: this.newPage.id + "-" + id,
          src: "image/page/" + this.newPage.route + "/" + id + "." + type
        };
        manifest.push(node);
      }
      return manifest;
    };

    SwitcherService.prototype.getPartialManifest = function() {
      return {
        id: this.newPage.id + "-partial",
        src: "partial/page/" + this.newPage.route + ".html"
      };
    };

    SwitcherService.prototype.getProjectsVideoManifest = function() {
      var extension;
      extension = Util.GetSupportedVideoExtension();
      return {
        id: this.newPage.id + "-video",
        src: "video/page/" + this.newPage.route + "/top." + extension
      };
    };

    return SwitcherService;

  })();
  return SwitcherService;
});
