var _this = this;

require.config({
  waitSeconds: 30,
  paths: {
    jquery: "../component/jquery/dist/jquery.min",
    signals: "../component/js-signals/dist/signals.min",
    hasher: "../component/hasher/dist/js/hasher.min",
    preloadjs: "../component/createjs-preloadjs/lib/preloadjs-0.6.0.min",
    Mustache: "../component/mustache.js/mustache",
    TweenMax: "../component/greensock/src/minified/TweenMax.min",
    TweenLite: "../component/greensock/src/minified/TweenLite.min",
    SplitText: "../component/greensock/SplitText",
    TimelineMax: "../component/greensock/src/minified/TimelineMax.min",
    TimelineLite: "../component/greensock/src/minified/TimelineLite.min",
    MouseWheel: "../component/jquery-mousewheel/jquery.mousewheel.min",
    Modernizr: "../lib/modernizr.custom",
    Main: "Main",
    App: "app/App",
    Context: "app/context/Context",
    SignalContext: "app/context/SignalContext",
    GlobalController: "app/controller/GlobalController",
    InitialLoadController: "app/controller/InitialLoadController",
    GlobalEvents: "app/event/GlobalEvents",
    GlobalModel: "app/model/GlobalModel",
    LoaderService: "app/service/LoaderService",
    Signal: "app/signal/Signal",
    Browser: "app/util/Browser",
    Util: "app/util/Util",
    View: "app/view/View",
    Footer: "app/view/layout/Footer",
    Header: "app/view/layout/Header"
  },
  shim: {
    TimelineMax: {
      deps: ["TweenMax"],
      exports: "TimelineMax"
    },
    TweenMax: {
      exports: "TweenMax"
    },
    SplitText: {
      deps: ["TweenMax"],
      exports: "SplitText"
    },
    preloadjs: {
      exports: "preloadjs"
    },
    signals: {
      exports: "signals"
    },
    hasher: {
      exports: "hasher"
    },
    mustache: {
      exports: "mustache"
    }
  }
});

require(["jquery", "App", "LoaderService", "GlobalModel", "GlobalController", "Signal", "Util", "GlobalEvents", "TweenMax", "Modernizr"], function(jquery, App, LoaderService, GlobalModel, GlobalController, Signal, Util, GlobalEvents, TweenMax, Modernizr) {
  var events;
  window.jQuery = window.$ = jquery;
  (function() {
    var browserRaf, canceled, targetTime, vendor, w, _i, _len, _ref;
    w = window;
    _ref = ['ms', 'moz', 'webkit', 'o'];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      vendor = _ref[_i];
      if (w.requestAnimationFrame) {
        break;
      }
      w.requestAnimationFrame = w["" + vendor + "RequestAnimationFrame"];
      w.cancelAnimationFrame = w["" + vendor + "CancelAnimationFrame"] || w["" + vendor + "CancelRequestAnimationFrame"];
    }
    if (w.requestAnimationFrame) {
      if (w.cancelAnimationFrame) {
        return;
      }
      browserRaf = w.requestAnimationFrame;
      canceled = {};
      w.requestAnimationFrame = function(callback) {
        var id;
        return id = browserRaf(function(time) {
          if (id in canceled) {
            return delete canceled[id];
          } else {
            return callback(time);
          }
        });
      };
      return w.cancelAnimationFrame = function(id) {
        return canceled[id] = true;
      };
    } else {
      targetTime = 0;
      w.requestAnimationFrame = function(callback) {
        var currentTime;
        targetTime = Math.max(targetTime + 16, currentTime = +(new Date));
        return w.setTimeout((function() {
          return callback(+(new Date));
        }), targetTime - currentTime);
      };
      return w.cancelAnimationFrame = function(id) {
        return clearTimeout(id);
      };
    }
  })();
  window.G = {};
  window.Loader = new LoaderService();
  window.Model = new GlobalModel();
  window.Controller = new GlobalController();
  window.Signal = new Signal();
  window.Util = new Util();
  events = new GlobalEvents().init();
  return $(function() {
    return new App();
  });
});
